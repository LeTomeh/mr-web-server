# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.auth.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='MRUser',
            fields=[
                ('user_ptr', models.OneToOneField(primary_key=True, serialize=False, auto_created=True, parent_link=True, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'MRUser',
                'verbose_name_plural': 'MRUsers',
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='MRDeveloper',
            fields=[
                ('mruser_ptr', models.OneToOneField(primary_key=True, serialize=False, auto_created=True, parent_link=True, to='User.MRUser')),
                ('_staff', models.BooleanField(verbose_name='Is from MR inside Staff')),
            ],
            options={
                'verbose_name': 'MRDeveloper',
                'verbose_name_plural': 'MRDevelopers',
            },
            bases=('User.mruser',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
    ]

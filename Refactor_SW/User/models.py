from django.contrib.auth.models import User
from django.db import models


class MRUser(User):
    pass

    class Meta:
        verbose_name = "MRUser"
        verbose_name_plural = "MRUsers"


class MRDeveloper(MRUser):
    _staff = models.BooleanField("Is from MR inside Staff")

    class Meta:
        verbose_name = "MRDeveloper"
        verbose_name_plural = "MRDevelopers"


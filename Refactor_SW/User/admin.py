from django.contrib import admin

# Register your models here.
from .models import MRDeveloper, MRUser

admin.site.register(MRUser)
admin.site.register(MRDeveloper)

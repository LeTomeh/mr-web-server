"""untitled URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib.auth import views as auth_views

from WebServer.views import userinterface, register, error, list_all, list_id, list_off, list_on, list_ar, \
    list_vr, test_interface, launch_mod, view_login, view_logout

app_name = 'User'

urlpatterns = [
    url(r'^login/', view_login, name='login'),
    url(r'^logout/', view_logout, name='logout'),
    url(r'^register/', register, name='register'),
    url(r'^index/', userinterface, name='userinterface'),
    url(r'^error/', error, name='error'),
    url(r'^list_all/', list_all, name='list_all'),
    url(r'^list_vr/', list_vr, name='list_vr'),
    url(r'^list_ar/', list_ar, name='list_ar'),
    url(r'^list_on/', list_on, name='list_on'),
    url(r'^list_off/', list_off, name='list_off'),
    url(r'^list_id/', list_id, name='list_id'),
    url(r'^launch_mod', launch_mod, name='launch_mod'),
    url(r'^test_interface', test_interface, name='test_interface')
]
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import user_passes_test, login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.template.context_processors import csrf

from User.forms import RegistrationUserForm
from WebServer.snippets import rfc
from .models import MRDeveloper


# Create your views here.


def home(request):
    return render(request, "home.html")


def error(request):
    return render(request, 'error.html')

def userinterface(request):
    """rfc.connect_to_server()
    rfc.start_connection("127.0.0.1")"""
    return render(request, "userinterface.html")


@user_passes_test(MRDeveloper)
def devinterface(request):
    return render(request, 'devinterface.html')


@login_required()
def test_interface(request):
    return render(request, 'userinterface.html')


def view_logout(request):
    logout(request)
    return render(request, "logout.html")


def view_login(request):
    if request.method == 'POST':
        username = request.POST.get('username', False)
        password = request.POST.get('password', False)
        user = authenticate(username=username, password=password)
        print(user)
        if user is not None:
            login(request, user)
            return redirect('/user/index/')
        else:
            return render(request, "login.html")

    return render(request, 'login.html')


def register(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/user/index/')
    else:
        if request.method == 'POST':
            form = RegistrationUserForm(request.POST)
            """form.clean()"""
            print(form.errors)
            if form.is_valid():
                print("yeah")
                form.save()
                return HttpResponseRedirect('/user/login/')

        context = {}
        context.update(csrf(request))
        context['form'] = RegistrationUserForm()

        return render(request, 'register.html', context)


@login_required()
def modulepage(request):
    return render(request, 'modulepage.html')

@login_required()
def moduledevpage(request):
    return render(request, 'moduledevpage.html')


def list_all(request):
    if request.method == "POST":
        try:
            return render(request, "userinterface.html", rfc.list_all_modules())
        except Exception as e:
            return render(request, "userinterface.html", repr(e))
    return redirect('/user/test_interface')


def list_vr(request):
    if request.method == "POST":
        try:
            return render(request, "userinterface.html", rfc.list_vr_modules())
        except Exception as e:
            return render(request, "userinterface.html", repr(e))
    return redirect('/user/test_interface')


def list_ar(request):
    if request.method == "POST":
        try:
            return render(request, "userinterface.html", rfc.list_ar_modules())
        except Exception as e:
            return render(request, "userinterface.html", repr(e))
    return redirect('/user/test_interface')


def list_off(request):
    if request.method == "POST":
        try:
            return render(request, "userinterface.html", rfc.list_off_modules())
        except Exception as e:
            return render(request, "userinterface.html", repr(e))
    return redirect('/user/test_interface')


def list_id(request):
    if request.method == "POST":
        try:
            return render(request, "userinterface.html", rfc.list_id_modules(request.POST.get('module_id', "1")))
        except Exception as e:
            return render(request, "userinterface.html", repr(e))
    return redirect('/user/test_interface')


def list_on(request):
    if request.method == "POST":
        try:
            return render(request, "userinterface.html", rfc.list_on_modules())
        except Exception as e:
            return render(request, "userinterface.html", repr(e))
    return redirect('/user/test_interface')


def launch_mod(request):
    if request.method == "POST":
        try:
            mid = request.POST.get('module_id', "1")
            print(rfc.launch_module(mid, "MRinside"))
            return render(request, "userinterface.html")
        except Exception as e:
            return render(request, "userinterface.html", repr(e))
    return redirect('/user/test_interface')


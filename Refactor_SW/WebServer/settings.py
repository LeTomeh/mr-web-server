LOGIN_REDIRECT_URL = 'userinterface'
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)
AUTH_USER_MODEL = 'models.MRUser'

LOGIN_URL = '/user/login/'

from django.contrib import admin
from .models import Logs, Module

# Register your models here.

admin.site.register(Logs)
admin.site.register(Module)

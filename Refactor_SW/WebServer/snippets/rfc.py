### @package snippets
# This module has been created to implement the RFC, which is needed to communicate with the SP.
#
# @author Victor Graffard
#
import base64
import json
import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
BUFF_SIZE = 4096

"""
Add pictures to the JSON Object -> base 64???
"""


def parse_list_response(response):
    result = {}
    commands_array = response.split("\r\n")
    vr = False
    ar = False
    turned_on = False
    for command in commands_array:
        if command[:3] in "210":
            name = command[4:]
        if command[:3] in "211":
            id = command[4:]
        if command[:3] in "212":
            desc = command[4:]
        if command[:3] in "213":
            vr = True
        if command[:3] in "214":
            ar = True
        if command[:3] in "215":
            players = command[4:]
        if command[:3] in "201":
            vr = True
        if command[:3] in "202":
            ar = True
        if command[:3] in "203":
            turned_on = False
        if command[:3] in "204":
            turned_on = True
        if command[:3] in "260":
            result = {
                "id": id,
                "name": name,
                "desc": desc,
                "vr": vr,
                "ar": ar,
                "players": players,
                "turned_on": turned_on}
        if command[:3] in "270":
            print(result)
            return result
    return result


def send_message(to_send):
    s.send(to_send.encode())


def connect_to_server():
    tcp_ip = '192.168.81.1'
    tcp_port = 4242

    try:
        s.connect((tcp_ip, tcp_port))
    except:
        return


def close_connection():
    s.close()


def start_connection(ip):
    message = 'SW ' + ip + ' TATA\r\n'
    send_message(message)
    bytes_response = s.recv(BUFF_SIZE)
    response = bytes_response.decode('UTF-8')
    print(bytes_response)
    if response[:3] != "100":
        raise Exception("Error code: " + response[:3] + " Can not connect to the Server.")
    return response


def list_all_modules():
    message = 'LIST ALL\r\n'
    send_message(message)
    bytes_response = s.recv(BUFF_SIZE)
    response = bytes_response.decode('UTF-8')
    print(bytes_response)
    if response[:3] != "200":
        raise Exception("Error code: " + response[:3] + " Could not get the listing of the modules.")
    return parse_list_response(response)


def list_vr_modules():
    message = 'LIST VR\r\n'
    send_message(message)
    bytes_response = s.recv(BUFF_SIZE)
    response = bytes_response.decode('UTF-8')
    print(bytes_response)
    if response[:3] != "201":
        raise Exception("Error code: " + response[:3] + " Could not get the listing of the VR modules.")
    return parse_list_response(response)


def list_ar_modules():
    message = 'LIST AR\r\n'
    send_message(message)
    bytes_response = s.recv(BUFF_SIZE)
    response = bytes_response.decode('UTF-8')
    print(bytes_response)
    if response[:3] != "202":
        raise Exception("Error code: " + response[:3] + " Could not get the listing of the AR modules.")
    return parse_list_response(response)


def list_off_modules():
    message = 'LIST OFF\r\n'
    send_message(message)
    bytes_response = s.recv(BUFF_SIZE)
    response = bytes_response.decode('UTF-8')
    print(bytes_response)
    if response[:3] != "203":
        raise Exception("Error code: " + response[:3] + " Could not get the listing of the OFF modules.")
    return parse_list_response(response)


def list_on_modules():
    message = 'LIST ON\r\n'
    send_message(message)
    bytes_response = s.recv(BUFF_SIZE)
    response = bytes_response.decode('UTF-8')
    print(bytes_response)
    if response[:3] != "204":
        raise Exception("Error code: " + response[:3] + " Could not get the listing of the ON modules.")
    return parse_list_response(response)


def list_id_modules(id):
    message = 'LIST ' + id + '\r\n'
    send_message(message)
    bytes_response = s.recv(BUFF_SIZE)
    response = bytes_response.decode('UTF-8')
    print(bytes_response)
    if response[:3] != "205":
        raise Exception("Error code: " + response[:3] + " Could not get the listing of the ID modules.")
    return parse_list_response(response)


def launch_module(mid, uid):
    message = 'LAUNCH ' + mid + ' ' + uid + '\r\n'
    send_message(message)
    bytes_response = s.recv(BUFF_SIZE)
    response = bytes_response.decode('UTF-8')
    print(bytes_response)
    if response[:3] != "301":
        raise Exception("Error code: " + response[:3] + " Could not launch the module.")
    return list_all_modules()


def add_user_to_module(mid, uid):
    message = 'USER ' + mid + ' ' + uid + '\r\n'
    send_message(message)
    bytes_response = s.recv(BUFF_SIZE)
    response = bytes_response.decode('UTF-8')
    print(bytes_response)
    if response[:3] != "302":
        raise Exception("Error code: " + response[:3] + " Could not add user to the module.")
    return list_all_modules()


def pause_module(mid):
    message = 'PAUSE ' + mid + '\r\n'
    send_message(message)
    bytes_response = s.recv(BUFF_SIZE)
    response = bytes_response.decode('UTF-8')
    print(bytes_response)
    if response[:3] != "303":
        raise Exception("Error code: " + response[:3] + " Could not pause the module.")
    return list_all_modules()


def leave_module(uid):
    message = 'LOGOUT ' + uid + '\r\n'
    send_message(message)
    bytes_response = s.recv(BUFF_SIZE)
    response = bytes_response.decode('UTF-8')
    print(bytes_response)
    if response[:3] != "304":
        raise Exception("Error code: " + response[:3] + " Could not leave the module.")
    return list_all_modules()


def stop_module(mid):
    message = 'STOP ' + mid + '\r\n'
    send_message(message)
    bytes_response = s.recv(BUFF_SIZE)
    response = bytes_response.decode('UTF-8')
    print(bytes_response)
    if response[:3] != "305":
        raise Exception("Error code: " + response[:3] + " Could not stop the module.")
    return list_all_modules()


def switch_connection_type(connection_type):
    message = 'SWITCHING ' + connection_type + '\r\n'
    send_message(message)
    bytes_response = s.recv(BUFF_SIZE)
    response = bytes_response.decode('UTF-8')
    print(bytes_response)
    if response[:3] != "401":
        raise Exception("Error code: " + response[:3] + " Could not host the connexion.")
    return

"""
def upload_module(file_type, file, file_name, id):
    with open(file, "rb"), as image_file:
        encoded_file = base64.b64encode(image_file.read())
    message = "UPLOAD " + file_type + " " + file_name + " " + id + "\r\n"
    send_message(message)
    bytes_response = s.recv(BUFF_SIZE)
    response = bytes_response.decode('UTF-8')
    print(bytes_response)
    if response[:3] != "510":
        raise Exception("Error code: " + response[:3] + " Could not upload the module.")
"""

"""
def HOSTING

def UPLOAD

def DOWNLOAD

"""
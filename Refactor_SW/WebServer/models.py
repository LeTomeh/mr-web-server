from django.db import models


class Module(models.Model):
    turned_on = models.BooleanField("Is the module turned on")
    players = models.CharField("Number of players in the game actually", max_length=254)
    name = models.CharField("Name of the module", max_length=254)
    ar = models.BooleanField("Is the module compatible with AR")
    desc = models.CharField("Description of the module", max_length=254)
    id = models.PositiveIntegerField("Id of the module", primary_key=True)
    vr = models.BooleanField("Is the module compatible with VR")


class Logs(models.Model):
    value = models.CharField(max_length=254)
    provenance = models.CharField(max_length=254)
    iserror = models.BooleanField()

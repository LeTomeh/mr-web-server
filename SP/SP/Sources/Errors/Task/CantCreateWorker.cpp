/**
* \file CantCreateWorker.cpp
* \brief CantCreateWorker implementation.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CantCreateWorker class implementation.
*
*/
#include "Errors\Task\CantCreateWorker.hh"

/**
* \fn CantCreateWorker(const std::string& message)
* \brief Constructor of a specific CantCreateWorker.
*
* \param message, use to get the message of the error.
* \return CantCreateWorker&
*/
Task::CantCreateWorker::CantCreateWorker(const std::string & message) : _message(message)
{
}

/**
* \fn ~CantCreateWorker()
* \brief Destructor of a specific CantCreateWorker
*/
Task::CantCreateWorker::~CantCreateWorker()
{
}

/**
* \fn const char* what() const
* \brief Getter returning message of a specific error.
*
* \return const char*
*/
const char * Task::CantCreateWorker::what() const
{
	return _message.c_str();
}

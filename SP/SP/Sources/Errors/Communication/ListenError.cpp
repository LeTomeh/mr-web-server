/**
* \file ListenError.cpp
* \brief ListenError implementation.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* ListenError class implementation.
*
*/

#include "Errors\Communication\ListenError.hh"

/**
* \fn ListenError(const std::string& message)
* \brief Constructor of a specific ListenError.
*
* \param message, use to get the message of the error.
* \return ListenError&
*/
Communication::ListenError::ListenError(const std::string & message) : _message(message)
{
}

/**
* \fn ~ListenError()
* \brief Destructor of a specific ListenError
*/
Communication::ListenError::~ListenError()
{
}

/**
* \fn const char* what() const
* \brief Getter returning message of a specific error.
*
* \return const char*
*/
const char * Communication::ListenError::what() const
{
	return _message.c_str();
}

#include "Errors\Communication\BindError.hh"

Communication::BindError::BindError(const std::string & message) : _message(message)
{
}

Communication::BindError::~BindError()
{
}

const char * Communication::BindError::what() const
{
	return _message.c_str();
}

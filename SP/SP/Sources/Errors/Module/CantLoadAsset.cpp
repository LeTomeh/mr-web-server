/**
* \file CantLoadAsset.cpp
* \brief CantLoadAsset implementation.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CantLoadAsset class implementation.
*
*/

#include "Errors\Module\CantLoadAsset.hh"

/**
* \fn CantLoadAsset(const std::string& message)
* \brief Constructor of a specific CantLoadAsset.
*
* \param message, use to get the message of the error.
* \return CantLoadAsset&
*/
Module::CantLoadAsset::CantLoadAsset(const std::string & message) : _message(message)
{
}

/**
* \fn ~CantLoadAsset()
* \brief Destructor of a specific CantLoadAsset
*/
Module::CantLoadAsset::~CantLoadAsset()
{
}

/**
* \fn const char* what() const
* \brief Getter returning message of a specific error.
*
* \return const char*
*/
const char * Module::CantLoadAsset::what() const
{
	return _message.c_str();
}

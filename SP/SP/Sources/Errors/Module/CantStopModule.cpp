#include "Errors\Module\CantStopModule.hh"

Module::CantStopModule::CantStopModule(const std::string & message) : _message(message)
{
}

Module::CantStopModule::~CantStopModule()
{
}

const char * Module::CantStopModule::what() const
{
	return _message.c_str();
}

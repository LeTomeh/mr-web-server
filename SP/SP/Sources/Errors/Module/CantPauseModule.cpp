/**
* \file CantPauseModule.cpp
* \brief CantPauseModule implementation.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CantPauseModule class implementation.
*
*/

#include "Errors\Module\CantPauseModule.hh"

/**
* \fn CantPauseModule(const std::string& message)
* \brief Constructor of a specific CantPauseModule.
*
* \param message, use to get the message of the error.
* \return CantPauseModule&
*/
Module::CantPauseModule::CantPauseModule(const std::string & message) : _message(message)
{
}

/**
* \fn ~CantPauseModule()
* \brief Destructor of a specific CantPauseModule
*/
Module::CantPauseModule::~CantPauseModule()
{
}

/**
* \fn const char* what() const
* \brief Getter returning message of a specific error.
*
* \return const char*
*/
const char * Module::CantPauseModule::what() const
{
	return _message.c_str();
}

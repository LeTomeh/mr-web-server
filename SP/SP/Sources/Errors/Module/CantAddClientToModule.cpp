/**
* \file CantAddClientToModule.cpp
* \brief CantAddClientToModule implementation.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CantAddClientToModule class implementation.
*
*/

#include "Errors\Module\CantAddClientToModule.hh"

/**
* \fn CantAddClientToModule(const std::string& message)
* \brief Constructor of a specific CantAddClientToModule.
*
* \param message, use to get the message of the error.
* \return CantAddClientToModule&
*/
Module::CantAddClientToModule::CantAddClientToModule(const std::string & message) : _message(message)
{
}

/**
* \fn ~CantAddClientToModule()
* \brief Destructor of a specific CantAddClientToModule
*/
Module::CantAddClientToModule::~CantAddClientToModule()
{
}

/**
* \fn const char* what() const
* \brief Getter returning message of a specific error.
*
* \return const char*
*/
const char * Module::CantAddClientToModule::what() const
{
	return _message.c_str();
}

/**
* \file CantLoadDescription.cpp
* \brief CantLoadDescription implementation.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CantLoadDescription class implementation.
*
*/

#include "Errors\Module\CantLoadDescription.hh"

/**
* \fn CantLoadDescription(const std::string& message)
* \brief Constructor of a specific CantLoadDescription.
*
* \param message, use to get the message of the error.
* \return CantLoadDescription&
*/
Module::CantLoadDescription::CantLoadDescription(const std::string & message) : _message(message)
{
}

/**
* \fn ~CantLoadDescription()
* \brief Destructor of a specific CantLoadDescription
*/
Module::CantLoadDescription::~CantLoadDescription()
{
}

/**
* \fn const char* what() const
* \brief Getter returning message of a specific error.
*
* \return const char*
*/
const char * Module::CantLoadDescription::what() const
{
	return _message.c_str();
}

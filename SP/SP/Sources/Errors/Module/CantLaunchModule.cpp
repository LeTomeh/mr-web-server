/**
* \file CantLaunchModule.cpp
* \brief CantLaunchModule implementation.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CantLaunchModule class implementation.
*
*/

#include "Errors\Module\CantLaunchModule.hh"

/**
* \fn CantLaunchModule(const std::string& message)
* \brief Constructor of a specific CantLaunchModule.
*
* \param message, use to get the message of the error.
* \return CantLaunchModule&
*/
Module::CantLaunchModule::CantLaunchModule(const std::string & message) : _message(message)
{
}

/**
* \fn ~CantLaunchModule()
* \brief Destructor of a specific CantLaunchModule
*/
Module::CantLaunchModule::~CantLaunchModule()
{
}

/**
* \fn const char* what() const
* \brief Getter returning message of a specific error.
*
* \return const char*
*/
const char * Module::CantLaunchModule::what() const
{
	return _message.c_str();
}

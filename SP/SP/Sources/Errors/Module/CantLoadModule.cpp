/**
* \file CantLoadModule.cpp
* \brief CantLoadModule implementation.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CantLoadModule class implementation.
*
*/

#include "Errors\Module\CantLoadModule.hh"

/**
* \fn CantLoadModule(const std::string& message)
* \brief Constructor of a specific CantLoadModule.
*
* \param message, use to get the message of the error.
* \return CantLoadModule&
*/
Module::CantLoadModule::CantLoadModule(const std::string & message) : _message(message)
{
}

/**
* \fn ~CantLoadModule()
* \brief Destructor of a specific CantLoadModule
*/
Module::CantLoadModule::~CantLoadModule()
{
}

/**
* \fn const char* what() const
* \brief Getter returning message of a specific error.
*
* \return const char*
*/
const char * Module::CantLoadModule::what() const
{
	return _message.c_str();
}

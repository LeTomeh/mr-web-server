/**
* \file CantRemoveClientToModule.cpp
* \brief CantRemoveClientToModule implementation.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CantRemoveClientToModule class implementation.
*
*/

#include "Errors\Module\CantRemoveClientToModule.hh"

/**
* \fn CantRemoveClientToModule(const std::string& message)
* \brief Constructor of a specific CantRemoveClientToModule.
*
* \param message, use to get the message of the error.
* \return CantRemoveClientToModule&
*/
Module::CantRemoveClientToModule::CantRemoveClientToModule(const std::string & message) : _message(message)
{
}

/**
* \fn ~CantRemoveClientToModule()
* \brief Destructor of a specific CaCantRemoveClientToModulentLaunchModule
*/
Module::CantRemoveClientToModule::~CantRemoveClientToModule()
{
}

/**
* \fn const char* what() const
* \brief Getter returning message of a specific error.
*
* \return const char*
*/
const char * Module::CantRemoveClientToModule::what() const
{
	return _message.c_str();
}

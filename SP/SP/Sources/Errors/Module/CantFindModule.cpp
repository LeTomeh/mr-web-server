/**
* \file CantFindModule.cpp
* \brief CantFindModule implementation.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CantFindModule class implementation.
*
*/

#include "Errors\Module\CantFindModule.hh"

/**
* \fn CantFindModule(const std::string& message)
* \brief Constructor of a specific CantFindModule.
*
* \param message, use to get the message of the error.
* \return CantFindModule&
*/
Module::CantFindModule::CantFindModule(const std::string & message) : _message(message)
{
}

/**
* \fn ~CantFindModule()
* \brief Destructor of a specific CantFindModule
*/
Module::CantFindModule::~CantFindModule()
{
}

/**
* \fn const char* what() const
* \brief Getter returning message of a specific error.
*
* \return const char*
*/
const char * Module::CantFindModule::what() const
{
	return _message.c_str();
}

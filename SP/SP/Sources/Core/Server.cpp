/**
* \file Server.cpp
* \brief Server Class.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* Server class implementation.
*
*/

#include "Core/Server.hh"

/**
* \fn Server(short)
* \brief Constructor of a specific Server.
*
* \param port, use to setup the communication.
* \return Server&
*/
Core::Server::Server(short port)
{
	_port = port;
	_service = new boost::asio::io_service;
	_logger = new Logger::Logger();
	_clientManager = new Client::ClientManager(_logger);
	_moduleManager = new Module::ModuleManager(_clientManager, _logger);
	_taskManager = new Task::TaskManager(dynamic_cast<Core::IServer*>(this), _logger);
	_communicationManager = new Communication::CommunicationManager(_clientManager, _taskManager, _logger, *_service, port);
	_isRunning = true;
}

/**
* \fn ~Server()
* \brief Destructor of a specific Server
*/
Core::Server::~Server()
{
	delete _logger;
	delete _clientManager;
	delete _moduleManager;
	delete _taskManager;
	delete _communicationManager;
}

/**
* \fn void run()
* \brief Function for handling the main loop of the server.
*
* \return void
*/
void Core::Server::run()
{
	try {
		_logger->newLog(new Logger::Log("Server start", "Server"));
		_service->run();
	}
	catch (const Communication::CommunicationError& e)
	{
		_logger->newLog(new Logger::Log(e.what(), "Communication"));
		_logger->newLog(new Logger::Log("Server stop", "Server"));
	}
}

/**
* \fn void close()
* \brief Function for handling the shutdown of the server.
*
* \return void
*/
void Core::Server::close()
{
	_communicationManager->clean();
	_moduleManager->clean();
	_clientManager->clean();
	_isRunning = false;
}

/**
* \fn void init(short)
* \brief Function for handling the initialisation of the server.
*
* \param port, use to setup the communication of the server.
* \return void
*/
void Core::Server::init(short port)
{
	if (_port != port)
		_port = port;
	_communicationManager->init(_port);
	_moduleManager->init();
}

/**
* \fn Module::IModuleManager* getModuleManager() const
* \brief Getter for the moduleManager of the server.
*
* \return Module::IModuleManager*
*/
Module::IModuleManager * Core::Server::getModuleManager() const
{
	return _moduleManager;
}

/**
* \fn Client::IClientManager* getClientManager() const
* \brief Getter for the clientManager of the server.
*
* \return Client::IClientManager*
*/
Client::IClientManager * Core::Server::getClientManager() const
{
	return _clientManager;
}

/**
* \fn Communication::ICommunicationManager* getCommunicationManager() const
* \brief Getter for the communicationManager of the server.
*
* \return Communication::ICommunicationManager*
*/
Communication::ICommunicationManager * Core::Server::getCommunicationManager() const
{
	return _communicationManager;
}

/**
* \fn Logger::ILogger* getLogger() const
* \brief Getter for the logger of the server.
*
* \return Logger::ILogger*
*/
Logger::ILogger* Core::Server::getLogger() const
{
	return _logger;
}
/**
* \file ModuleFactory.cpp
* \brief ModuleFactory Class.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* ModuleFactory class implementation.
*
*/

#include "Module/ModuleFactory.hh"

/**
* \fn ModuleFactory(std::string)
* \brief Constructor of a specific ModuleFactory.
*
* \param rootModulePath, use to get the root path of every modules.
* \return ModuleFactory&
*/
Module::ModuleFactory::ModuleFactory(std::string rootModulePath)
	: _rootModulePath(rootModulePath)
{
}

/**
* \fn ~ModuleFactory()
* \brief Destructor of a specific ModuleFactory
*/
Module::ModuleFactory::~ModuleFactory()
{
}

/**
* \fn Module::IModule* createModule(const unsigned long) const
* \brief Function returning a new module based on the module id.
*
* \param id, use to get the id of the module.
* \return Module::IModule*
*/
Module::IModule * Module::ModuleFactory::createModule(const unsigned long id) const
{
	struct stat		info;
	std::string		moduleDirectory;
	std::string		descriptionPath;
	IModule*		module = nullptr;

	moduleDirectory = _rootModulePath;
	moduleDirectory += std::to_string(id);
	LIBXML_TEST_VERSION
	descriptionPath = moduleDirectory + "/description.xml";
	xmlDocPtr doc = xmlReadFile((boost::filesystem::current_path().generic_string() + "/" + descriptionPath).c_str(), NULL, 0);
	if (doc != NULL)
	{
		ModuleDescription* description = nullptr;
		std::string name;
		bool allowAR = false;
		bool allowVR = false;
		for (xmlNode* cur_node = xmlDocGetRootElement(doc)->children; cur_node; cur_node = cur_node->next)
		{
			if (std::string((char*)cur_node->name).compare("description") == 0)
				description = new ModuleDescription((char*)cur_node->children->content);
			if (std::string((char*)cur_node->name).compare("name") == 0)
				name = (char*)cur_node->children->content;
			if (std::string((char*)cur_node->name).compare("allowVR") == 0)
				allowVR = (xmlStrcmp(cur_node->children->content, (const xmlChar*)"true") == 0);
			if (std::string((char*)cur_node->name).compare("allowAR") == 0)
				allowAR = (xmlStrcmp(cur_node->children->content, (const xmlChar*)"true") == 0);
		}
		module = new Module(id, moduleDirectory, name, description, allowAR, allowVR);
		xmlFreeDoc(doc);
	}
	xmlCleanupParser();
	xmlMemoryDump();
	return module;
}

/**
* \file Module.cpp
* \brief Module Class.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* Module class implementation.
*
*/

#include "Module/Module.hh"

/**
* \fn Module(const unsigned long, const std::string, const std::string, const IModuleDescription*, const bool)
* \brief Constructor of a specific Module.
*
* \param id, use to set the id of the module.
* \param path, use to set the path of the module.
* \param name, use to set the name of the module.
* \param description, use to set the description of the module.
* \param allowAR, use to set the AR compatibiilty of the module.
* \return Module&
*/
Module::Module::Module(const unsigned long id, const std::string path, const std::string name, const IModuleDescription * description, const bool allowAR, const bool allowVR)
	: _id(id), _allowAR(allowAR), _allowVR(allowVR), _isAvailable(true), _path(std::string(path)), _name(std::string(name))
{
	_executablePath = "";
	_description = description;
	_isLaunched = false;
}

/**
* \fn ~Module()
* \brief Destructor of a specific Module
*/
Module::Module::~Module()
{
}

/**
* \fn bool isLaunched() const
* \brief Function use to know if the module is launched.
*
* \return bool
*/
bool Module::Module::isLaunched() const
{
	return _isLaunched;
}

/**
* \fn unsigned long getID() const
* \brief Getter returning the id of a specific module.
*
* \return unsigned long
*/
unsigned long Module::Module::getID() const
{
	return _id;
}

/**
* \fn std::string getPath() const
* \brief Getter returning path of a specific module.
*
* \return std::string
*/
std::string Module::Module::getPath() const
{
	return _path;
}

/**
* \fn Module::e_ModuleDisponibility getModuleDisponibility() const
* \brief Getter returning the disponibility of a specific module.
*
* \return Module::e_ModuleDisponibility
*/
Module::e_ModuleDisponibility Module::Module::getModuleDisponibility() const
{
	return _disponibility;
}

/**
* \fn std::string getName() const
* \brief Getter returning name of a specific module.
*
* \return std::string
*/
std::string Module::Module::getName() const
{
	return _name;
}

/**
* \fn const Module::IModuleDescription* getDescription() const
* \brief Getter returning description of a specific module.
*
* \return const Module::IModuleDescription*
*/
const Module::IModuleDescription * Module::Module::getDescription() const
{
	return _description;
}

/**
* \fn bool allowAR() const
* \brief Function returning the AR compatibility of a specific module.
*
* \return bool
*/
bool Module::Module::allowAR() const
{
	return _allowAR;
}

/**
* \fn bool allowVR() const
* \brief Function returning the VR compatibility of a specific module.
*
* \return bool
*/
bool Module::Module::allowVR() const
{
	return _allowVR;
}

/**
* \fn Module::IUserList* getUserList() const
* \brief Getter returning the uset list of a specific module.
*
* \return Module::IUserList*
*/
Module::IUserList * Module::Module::getUserList() const
{
	return _userList;
}

/**
* \fn void connectNewClient(unsigned long)
* \brief Function adding a client to the client list of a specific module.
*
* \param clientID, use to get the client id.
* \return void
*/
void Module::Module::connectNewClient(unsigned long clientID)
{
	_userList->addUser(clientID);
}

/**
* \fn void pauseClient(unsigned long)
* \brief Function pausing a specific client from a specific module.
*
* \param clientID, use to get the client id.
* \return void
*/
void Module::Module::pauseClient(unsigned long clientID)
{
}

/**
* \fn void disconnectClient(unsigned long)
* \brief Function removing a specific client from a specific module.
*
* \param clientID, use to get the client id.
* \return void
*/
void Module::Module::disconnectClient(unsigned long clientID)
{
	_userList->removeUser(clientID);
}

/**
* \fn void launch(unsigned long clientID, Core::IServer* server)
* \brief Function starting a specific module.
*
* \param clientID, use to get the id of the client who launch the module
* \param server, use to get the server
* \return void
*/
void Module::Module::launch(unsigned long clientID, Core::IServer* server)
{
	if (_executablePath == "") {
		std::string execPath = boost::filesystem::current_path().generic_string() + "/" + _path + "/" + _name + ".exe";
		_executablePath = execPath.c_str();
	}
	WinExec(_executablePath, SW_SHOW);
	_launchedBy = clientID;
	_isLaunched = true;
	_serverConnexion = server->getCommunicationManager()->createConnexion();
	if (_serverConnexion)
		_serverConnexion->doRead();
}

/**
* \fn void pauseModule()
* \brief Function pausing a specific module.
*
* \return void
*/
void Module::Module::pauseModule()
{
	if (_serverConnexion)
	{
		_serverConnexion->addToResponseStream("PAUSE\r\n");
		_serverConnexion->doWrite();
		_serverConnexion->doRead();
	}
	// "210\r\n" -> SUCCESS
	// "219\r\n"  -> failed
}

/**
* \fn void unpauseModule()
* \brief Function pausing a specific module.
*
* \return void
*/
void Module::Module::unpauseModule()
{
	if (_serverConnexion)
	{
		_serverConnexion->addToResponseStream("UNPAUSE\r\n");
		_serverConnexion->doWrite();
		_serverConnexion->doRead();
	}
	// "221\r\n" -> success
	// "229\r\n" -> failed
}

/**
* \fn void close()
* \brief Function closing a specific module.
*
* \return void
*/
void Module::Module::close()
{
	_launchedBy = 0;
	_isLaunched = false;
	if (_serverConnexion)
	{
		_serverConnexion->addToResponseStream("QUIT\r\n");
		_serverConnexion->doWrite();
		_serverConnexion->socket().close();
		delete _serverConnexion;
	}
	// "230\r\n" -> sucess
	// "239\r\n" -> failed
}

/**
* \fn bool isAvailable() const
* \brief Getter to know if module is available.
*
* \return bool
*/
bool Module::Module::isAvailable() const
{
	return _isAvailable;
}

/**
* \fn void setAvailable(bool available)
* \brief Setter to know if module is available.
*
* \param available, use to set the availability of the module.
* \return void
*/
void Module::Module::setAvailable(bool available)
{
	_isAvailable = available;
}

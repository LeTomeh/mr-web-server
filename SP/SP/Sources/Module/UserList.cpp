/**
* \file UserList.cpp
* \brief UserList Class.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* UserList class implementation.
*
*/

#include "Module/UserList.hh"

/**
* \fn UserList()
* \brief Constructor of a specific UserList.
*
* \return UserList&
*/
Module::UserList::UserList()
{
}

/**
* \fn ~UserList()
* \brief Destructor of a specific UserList
*/
Module::UserList::~UserList()
{
}

/**
* \fn unsigned long getUserListSize() const
* \brief Getter returning the size of a specific UserList.
*
* \return unsigned long
*/
unsigned long Module::UserList::getUserListSize() const
{
	return _userList.size();
}

/**
* \fn std::list<unsigned long> getUserList() const
* \brief Getter returning the list of id from a specific UserList.
*
* \return std::list<unsigned long>
*/
std::list<unsigned long> Module::UserList::getUserList() const
{
	return _userList;
}

/**
* \fn void addUser(unsigned long)
* \brief Function adding a new user to the user list.
*
* \return void
*/
void Module::UserList::addUser(unsigned long userId)
{
	_userList.push_back(userId);
}

/**
* \fn removeUser(unsigned long clientId)
* \brief Function removing a user to the user list
*
* \param clientId, used to remove id of the user from the userlist
* \return void
*/
void Module::UserList::removeUser(unsigned long userId)
{
	_userList.remove(userId);
}
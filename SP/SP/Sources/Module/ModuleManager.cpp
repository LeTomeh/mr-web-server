/**
* \file ModuleManager.cpp
* \brief ModuleManager Class.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* ModuleManager class implementation.
*
*/

#include "Module/ModuleManager.hh"

/**
* \fn ModuleManager(const std::string, const std::string)
* \brief Constructor of a specific ModuleManager.
*
* \param name, use to get the name of the asset.
* \param path, use to get the path of the asset.
* \return ModuleManager&
*/
Module::ModuleManager::ModuleManager(Client::IClientManager * clientManager, Logger::ILogger * logger)
{
	_clientManager = clientManager;
	_logger = logger;
	_moduleFactory = new ModuleFactory("Modules/");
	_assetManager = new Asset::AssetsManager("Modules/", logger);
}

/**
* \fn ~ModuleManager()
* \brief Destructor of a specific ModuleManager
*/
Module::ModuleManager::~ModuleManager()
{
}

/**
* \fn void init()
* \brief Function handling the initialisation of a specific ModuleManager.
*
* \return void
*/
void Module::ModuleManager::init()
{
	unsigned long moduleID = 1;
	IModule* module = nullptr;
	
	module = _moduleFactory->createModule(moduleID);
	while (module)
	{
		_moduleList.push_back(module);
		if (module->allowAR())
			_moduleARList.push_back(module->getID());
		if (module->allowVR())
			_moduleVRList.push_back(module->getID());
		_moduleOFFList.push_back(module->getID());
		_logger->newLog(new Logger::Log("Adding module named " + module->getName() + " as module " + std::to_string(module->getID()), "ModuleManager"));
		moduleID++;
		module = _moduleFactory->createModule(moduleID);
	}
}

/**
* \fn void update()
* \brief Function handling the updating of every module for a specific ModuleManager.
*
* \return void
*/
void Module::ModuleManager::update()
{
	unsigned long moduleID = 1;
	IModule* module = nullptr;

	module = _moduleFactory->createModule(moduleID);
	while (module || moduleID < _moduleList.size())
	{
		if (!module)
			_logger->newLog(new Logger::Log("Adding module named " + _moduleList[moduleID]->getName() + " as unavailable", "ModuleManager", true));
		else if (module->getID() >= _moduleList.size())
			_logger->newLog(new Logger::Log("Adding module named " + module->getName() + " as module " + std::to_string(module->getID()), "ModuleManager"));
		_moduleList[moduleID]->setAvailable( (module != NULL) );
		if (module->allowAR())
			_moduleARList.push_back(module->getID());
		if (module->allowVR())
			_moduleVRList.push_back(module->getID());
		moduleID++;
		module = _moduleFactory->createModule(moduleID);
	}
}

/**
* \fn void clean()
* \brief Function cleaning a specific ModuleManager.
*
* \return void
*/
void Module::ModuleManager::clean()
{
	while (_moduleList.size())
	{		
		delete _moduleList[0];
		_moduleList.erase(_moduleList.begin());
	}
	_moduleVRList.clear();
	_moduleARList.clear();
	_moduleONList.clear();
	_moduleOFFList.clear();
	_modulePausedList.clear();
}

/**
* \fn void createNewModule()
* \brief Function handling the creation of a new module for specific ModuleManager.
*
* \return void
*/
void Module::ModuleManager::createNewModule()
{
	_moduleList.push_back(_moduleFactory->createModule(_moduleList.size() + 1));
}

/**
* \fn void addClientToModule(unsigned long, unsigned long)
* \brief Function adding a specific client to a specific module.
*
* \param clientID, use to get the client id.
* \param moduleID, use to get the module id.
* \return void
*/
void Module::ModuleManager::addClientToModule(unsigned long clientID, unsigned long moduleID)
{
	for (unsigned long it = 0; it < _moduleList.size(); it++)
	{
		if (_moduleList[it]->getID() == moduleID)
		{
			if (_moduleList[it]->isLaunched())
			{
				_logger->newLog(new Logger::Log("Adding user " + std::to_string(clientID) + " to module " + std::to_string(moduleID), "ModuleManager"));
				_moduleList[it]->connectNewClient(clientID);
			}
			else
			{
				_logger->newLog(new Logger::Log("Can't add user: module " + std::to_string(moduleID) + " is not launched", "ModuleManager", true));
			}
			return;
		}
	}
	_logger->newLog(new Logger::Log("Can't find module " + std::to_string(moduleID), "ModuleManager", true));
	throw (CantFindModule("Can't find module"));
}

/**
* \fn void pauseClientToModule(unsigned long, unsigned long)
* \brief Function pausing a specific client to a specific module.
*
* \param clientID, use to get the client id.
* \param moduleID, use to get the module id.
* \return void
*/
void Module::ModuleManager::pauseClientToModule(unsigned long clientID, unsigned long moduleID)
{
}

/**
* \fn void removeClientToModule(unsigned long, unsigned long)
* \brief Function removing a specific client to a specific module.
*
* \param clientID, use to get the client id.
* \param moduleID, use to get the module id.
* \return void
*/
void Module::ModuleManager::removeClientToModule(unsigned long clientID, unsigned long moduleID)
{
	for (unsigned long it = 0; it < _moduleList.size(); it++)
	{
		if (_moduleList[it]->getID() == moduleID && _moduleList[it]->isLaunched())
		{
			_logger->newLog(new Logger::Log("Removing user " + std::to_string(clientID) + " to module " + std::to_string(moduleID), "ModuleManager"));
			_moduleList[it]->disconnectClient(clientID);
			return;
		}
		else
		{
			_logger->newLog(new Logger::Log("Can't remove user " + std::to_string(clientID) + " from module " + std::to_string(moduleID), "ModuleManager", true));
			return;
		}
	}
	_logger->newLog(new Logger::Log("Can't find module " + std::to_string(moduleID), "ModuleManager", true));
	throw (CantFindModule("Can't find module"));
}

/**
* \fn void launchModule(unsigned long, Core::IServer* server)
* \brief Function launching a specific module.
*
* \param clientID, use to get the client id.
* \param moduleID, use to get the module id.
* \param server, use to get the server.
* \return void
*/
void Module::ModuleManager::launchModule(unsigned long clientID, unsigned long moduleID, Core::IServer* server)
{
	for (unsigned long it = 0; it < _moduleList.size(); it++)
	{
		if (_moduleList[it]->getID() == moduleID && !_moduleList[it]->isLaunched())
		{
			_logger->newLog(new Logger::Log("User " + std::to_string(clientID) + " launch module " + std::to_string(moduleID), "ModuleManager"));
			_moduleList[it]->launch(clientID, server);
			return;
		}
		else
		{
			_logger->newLog(new Logger::Log("Can't launch module " + std::to_string(moduleID), "ModuleManager", true));
			return;
		}
	}
	_logger->newLog(new Logger::Log("Can't find module " + std::to_string(moduleID), "ModuleManager", true));
	throw (CantLaunchModule("Can't launch module"));
}

/**
* \fn void pauseModule(unsigned long)
* \brief Function pausing a specific module.
*
* \param moduleID, use to get the module id.
* \return void
*/
void Module::ModuleManager::pauseModule(unsigned long moduleID)
{
}

/**
* \fn void stopModule(unsigned long)
* \brief Function stoping a specific module.
*
* \param moduleID, use to get the module id.
* \return void
*/
void Module::ModuleManager::stopModule(unsigned long moduleID)
{
	for (unsigned long it = 0; it < _moduleList.size(); it++)
	{
		if (_moduleList[it]->getID() == moduleID && _moduleList[it]->isLaunched())
		{
			_logger->newLog(new Logger::Log("Closing module " + std::to_string(moduleID), "ModuleManager"));
			_moduleList[it]->close();
			return;
		}
		else
		{
			_logger->newLog(new Logger::Log("Can't close module " + std::to_string(moduleID), "ModuleManager", true));
			return;
		}
	}
	_logger->newLog(new Logger::Log("Can't find module " + std::to_string(moduleID), "ModuleManager", true));
	throw (CantStopModule("Can't stop module"));
}

/**
* \fn unsigned long getModuleListSize() const
* \brief Getter returning the size of the module list.
*
* \return unsigned long
*/
Module::IModule * Module::ModuleManager::getModule(unsigned long moduleID) const
{
	for (unsigned long it = 0; it < _moduleList.size(); it++)
	{
		if (_moduleList[it]->getID() == moduleID)
			return _moduleList[it];
	}
	return nullptr;
}

/**
* \fn std::vector<IModule*> getModuleList() const
* \brief Function returning a specific module.
*
* \param moduleID, use to get the module id.
* \return std::vector<IModule*>
*/
std::vector<Module::IModule*> Module::ModuleManager::getModuleList() const
{
	return _moduleList;
}

/**
* \fn unsigned long getModuleListSize() const
* \brief Getter returning the size of the module list.
*
* \return unsigned long
*/
unsigned long Module::ModuleManager::getModuleListSize() const
{
	return _moduleList.size();
}

/**
* \fn std::list<unsigned long> getModuleVRList() const
* \brief Getter returning the VR module list.
*
* \return std::list<unsigned long>
*/
std::list<unsigned long> Module::ModuleManager::getModuleVRList() const
{
	return _moduleVRList;
}

/**
* \fn std::list<unsigned long> getModuleARList() const
* \brief Getter returning the AR module list.
*
* \return std::list<unsigned long>
*/
std::list<unsigned long> Module::ModuleManager::getModuleARList() const
{
	return _moduleARList;
}

/**
* \fn std::list<unsigned long> getModuleLaunched() const
* \brief Getter returning the launched module list.
*
* \return std::list<unsigned long>
*/
std::list<unsigned long> Module::ModuleManager::getModuleLaunched() const
{
	return _moduleONList;
}

/**
* \fn std::list<unsigned long> getModuleClosed() const
* \brief Getter returning the closed module list.
*
* \return std::list<unsigned long>
*/
std::list<unsigned long> Module::ModuleManager::getModuleClosed() const
{
	return _moduleOFFList;
}

/**
* \fn void addNewAssets(unsigned long, std::string)
* \brief Function adding a new asset to a specific module.
*
* \param moduleID, use to get the id of the module.
* \param path, use to get the path of the asset.
* \return void
*/
void Module::ModuleManager::addNewAssets(unsigned long moduleID, std::string path)
{
}

/**
* \fn unsigned long getModuleAssetsListSize(unsigned long) const
* \brief Getter returning the size of the asset list for a specific module.
*
* \param moduleID, use to get the id of the module.
* \return unsigned long
*/
unsigned long Module::ModuleManager::getModuleAssetsListSize(unsigned long moduleID) const
{
	return _assetManager->getModuleAssetsListSize(moduleID);
}

/**
* \fn std::list<Asset::IAsset*> getModuleAssetsList(unsigned long) const
* \brief Getter returning the asset list for a specific module.
*
* \param moduleID, use to get the id of the module.
* \return std::list<Asset::IAsset*>
*/
Asset::IAssetsList* Module::ModuleManager::getModuleAssetsList(unsigned long moduleID) const
{
	return _assetManager->getModuleAssetsList(moduleID);
}

/**
* \fn std::list<std::string> getModuleAssetsListName(unsigned long) const
* \brief Getter returning the list of name of the asset list for a specific module.
*
* \param moduleID, use to get the id of the module.
* \return std::list<std::string>
*/
std::list<std::string> Module::ModuleManager::getModuleAssetsListName(unsigned long moduleID) const
{
	return _assetManager->getModuleAssetsListName(moduleID);
}

/**
* \file ModuleDescription.cpp
* \brief ModuleDescription Class.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* ModuleDescription class implementation.
*
*/

#include "Module/ModuleDescription.hh"

/**
* \fn AssModuleDescriptionet(const std::string)
* \brief Constructor of a specific ModuleDescription.
*
* \param description, use to get the description of the ModuleDescription.
* \return ModuleDescription&
*/
Module::ModuleDescription::ModuleDescription(const std::string description) : _description(std::string(description))
{
}

/**
* \fn ~ModuleDescription()
* \brief Destructor of a specific ModuleDescription
*/
Module::ModuleDescription::~ModuleDescription()
{
}

/**
* \fn unsigned long getDescriptionSize() const
* \brief Getter returning size of the description of a specific ModuleDescription.
*
* \return unsigned long
*/
unsigned long Module::ModuleDescription::getDescriptionSize() const
{
	return _description.size();
}

/**
* \fn std::string getDescription() const
* \brief Getter returning the description of a specific ModuleDescription.
*
* \return std::string
*/
std::string Module::ModuleDescription::getDescription() const
{
	return _description;
}

#include "Utils\TaskGenerator.hh"

std::map < std::string, Task::TaskGenerator::tGeneratorPtr> Task::TaskGenerator::createTaskCreator()
{
	std::map<std::string, tGeneratorPtr> map;

	map["LIST"] = &TaskGenerator::list;
	map["LAUNCH"] = &TaskGenerator::launchModule;
	map["PAUSE"] = &TaskGenerator::pauseModule;
	map["USER"] = &TaskGenerator::addUserToModule;
	map["LOGOUT"] = &TaskGenerator::logoutUser;
	map["STOP"] = &TaskGenerator::stopModule;
	map["LOAD"] = &TaskGenerator::loadModule;
	map["SWITCH"] = &TaskGenerator::switchConnectionType;
	map["HOSTING"] = &TaskGenerator::hostingNewConnection;
	map["UPLOAD"] = &TaskGenerator::uploadPackage;
	map["DOWNLOAD"] = &TaskGenerator::downloadPackage;
	return map;
}

std::map < std::string, Task::TaskGenerator::tGeneratorPtr> Task::TaskGenerator::createListCreator()
{
	std::map<std::string, tGeneratorPtr> map;

	map["ALL"] = &TaskGenerator::listAllModule;
	map["MODULE"] = &TaskGenerator::listModule;
	map["ON"] = &TaskGenerator::listModuleOn;
	map["OFF"] = &TaskGenerator::listModuleOff;
	map["VR"] = &TaskGenerator::listModuleVR;
	map["AR"] = &TaskGenerator::listModuleAR;
	return map;
}
std::map<std::string, Task::TaskGenerator::tGeneratorPtr> Task::TaskGenerator::_taskCreator = createTaskCreator();
std::map<std::string, Task::TaskGenerator::tGeneratorPtr> Task::TaskGenerator::_listCreator = createListCreator();


Task::ITask * Task::TaskGenerator::generateTask(std::string clientRequest, unsigned long id)
{
	TaskGenerator* tasker = new TaskGenerator();
	ITask*	task = nullptr;
	std::istringstream iss(clientRequest);
	std::string sub;

	getline(iss, sub, ' ');
	if (_taskCreator.find(sub) != _taskCreator.end())
	{
		task = (tasker->*(_taskCreator[sub]))(iss.str(), id);
	}
	delete tasker;
	return task;
}

Task::ITask * Task::TaskGenerator::list(std::string clientRequest, unsigned long id)
{
	std::istringstream	iss(clientRequest);
	std::string sub;

	getline(iss, sub, ' ');
	sub = "";
	getline(iss, sub, '\r');
	if (std::atoi(sub.c_str()) != 0)
		return (this->*(_listCreator["MODULE"]))(iss.str(), id);
	if (sub != "" && _listCreator.find(sub) != _listCreator.end())
		return (this->*(_listCreator[sub]))(iss.str(), id);
	return nullptr;
}

Task::ITask * Task::TaskGenerator::listModule(std::string clientRequest, unsigned long id)
{
	std::map<std::string, void*> map;
	std::istringstream	iss(clientRequest);
	std::string sub;

	getline(iss, sub, ' ');
	getline(iss, sub, '\r');
	map["ID_MODULE"] = static_cast<void*>(new std::string(sub));
	map["SENDER"] = static_cast<void*>(new std::string(std::to_string(id)));
	return (new Task(LIST_MODULE, map));
}

Task::ITask * Task::TaskGenerator::listAllModule(std::string clientRequest, unsigned long id)
{
	std::map<std::string, void*> map;

	map["SENDER"] = static_cast<void*>(new std::string(std::to_string(id)));
	return (new Task(LIST_ALLMODULE, map));
}

Task::ITask * Task::TaskGenerator::listModuleOn(std::string clientRequest, unsigned long id)
{
	std::map<std::string, void*> map;

	map["SENDER"] = static_cast<void*>(new std::string(std::to_string(id)));
	return (new Task(LIST_MODULE_ON, map));
}

Task::ITask * Task::TaskGenerator::listModuleOff(std::string clientRequest, unsigned long id)
{
	std::map<std::string, void*> map;

	map["SENDER"] = static_cast<void*>(new std::string(std::to_string(id)));
	return (new Task(LIST_MODULE_OFF, map));
}

Task::ITask * Task::TaskGenerator::listModuleVR(std::string clientRequest, unsigned long id)
{
	std::map<std::string, void*> map;

	map["SENDER"] = static_cast<void*>(new std::string(std::to_string(id)));
	return (new Task(LIST_MODULE_VR, map));
}

Task::ITask * Task::TaskGenerator::listModuleAR(std::string clientRequest, unsigned long id)
{
	std::map<std::string, void*> map;

	map["SENDER"] = static_cast<void*>(new std::string(std::to_string(id)));
	return (new Task(LIST_MODULE_AR, map));
}

Task::ITask * Task::TaskGenerator::launchModule(std::string clientRequest, unsigned long id)
{
	std::map<std::string, void*> map;
	std::istringstream iss(clientRequest);
	std::string sub;

	getline(iss, sub, ' ');
	getline(iss, sub, ' ');
	map["ID_MODULE"] = static_cast<void*>(new std::string(sub));
	getline(iss, sub, '\r');
	map["ID_USER"] = static_cast<void*>(new std::string(sub));
	map["SENDER"] = static_cast<void*>(new std::string(std::to_string(id)));
	return (new Task(LAUNCH_MODULE, map));
}

Task::ITask * Task::TaskGenerator::pauseModule(std::string clientRequest, unsigned long id)
{
	std::map<std::string, void*> map;
	std::istringstream iss(clientRequest);
	std::string sub;

	getline(iss, sub, ' ');
	getline(iss, sub, '\r');
	map["ID_MODULE"] = static_cast<void*>(new std::string(sub));
	map["SENDER"] = static_cast<void*>(new std::string(std::to_string(id)));
	return (new Task(PAUSE_MODULE, map));
}

Task::ITask * Task::TaskGenerator::addUserToModule(std::string clientRequest, unsigned long id)
{
	std::map<std::string, void*> map;
	std::istringstream iss(clientRequest);
	std::string sub;

	getline(iss, sub, ' ');
	getline(iss, sub, ' ');
	map["ID_MODULE"] = static_cast<void*>(new std::string(sub));
	getline(iss, sub, '\r');
	map["ID_USER"] = static_cast<void*>(new std::string(sub));
	map["SENDER"] = static_cast<void*>(new std::string(std::to_string(id)));
	return (new Task(ADD_CLIENT, map));
}

Task::ITask * Task::TaskGenerator::logoutUser(std::string clientRequest, unsigned long id)
{
	std::map<std::string, void*> map;

	map["SENDER"] = static_cast<void*>(new std::string(std::to_string(id)));
	return (new Task(CLOSE_CLIENT));
}

Task::ITask * Task::TaskGenerator::stopModule(std::string clientRequest, unsigned long id)
{
	std::map<std::string, void*> map;
	std::istringstream iss(clientRequest);
	std::string sub;

	getline(iss, sub, ' ');
	getline(iss, sub, '\r');
	map["ID_MODULE"] = static_cast<void*>(new std::string(sub));
	map["SENDER"] = static_cast<void*>(new std::string(std::to_string(id)));
	return (new Task(STOP_MODULE, map));
}

Task::ITask * Task::TaskGenerator::loadModule(std::string clientRequest, unsigned long id)
{
	return nullptr;
}

Task::ITask * Task::TaskGenerator::switchConnectionType(std::string clientRequest, unsigned long id)
{
	std::map<std::string, void*> map;
	std::istringstream iss(clientRequest);
	std::string sub;

	getline(iss, sub, ' ');
	getline(iss, sub, '\r');
	map["CONNECTIONTYPE"] = static_cast<void*>(new std::string(sub));
	map["SENDER"] = static_cast<void*>(new std::string(std::to_string(id)));
	return (new Task(SWITCHING_CONNECTION, map));
}

Task::ITask * Task::TaskGenerator::hostingNewConnection(std::string clientRequest, unsigned long id)
{
	std::map<std::string, void*> map;
	std::istringstream iss(clientRequest);
	std::string sub;

	getline(iss, sub, ' ');
	getline(iss, sub, ' ');
	map["CONNECTIONTYPE"] = static_cast<void*>(new std::string(sub));
	getline(iss, sub, ':');
	map["SWIP"] = static_cast<void*>(new std::string(sub));
	getline(iss, sub);
	map["SWPORT"] = static_cast<void*>(new std::string(sub));
	map["SENDER"] = static_cast<void*>(new std::string(std::to_string(id)));
	return (new Task(HOSTING_CONNECTION, map));
}

Task::ITask * Task::TaskGenerator::uploadPackage(std::string clientRequest, unsigned long id)
{
	std::map<std::string, void*> map;
	std::istringstream iss(clientRequest);
	std::string sub;

	getline(iss, sub, ' ');
	map["DATATYPE"] = static_cast<void*>(new std::string(sub));
	getline(iss, sub, '\r');
	map["ID_MODULE"] = static_cast<void*>(new std::string(sub));
	map["SENDER"] = static_cast<void*>(new std::string(std::to_string(id)));
	return (new Task(UPLOAD_FILE, map));
}

Task::ITask * Task::TaskGenerator::downloadPackage(std::string clientRequest, unsigned long id)
{
	std::map<std::string, void*> map;
	std::istringstream iss(clientRequest);
	std::string sub;

	getline(iss, sub, ' ');
	iss >> sub;
	map["DATATYPE"] = static_cast<void*>(new std::string(sub));
	iss >> sub;
	map["ID_MODULE"] = static_cast<void*>(new std::string(sub));
	map["SENDER"] = static_cast<void*>(new std::string(std::to_string(id)));
	return (new Task(DOWNLOAD_FILE, map));
}

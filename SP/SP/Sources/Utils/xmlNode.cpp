#include "Utils\xmlNode.hh"

/**
* \fn xmlNode(std::string name, std::string value)
* \brief Constructor of a specific xmlNode.
*
* \param name , use to get the name of the node.
* \param value, use to get the value of the node.
* \return xmlNode&
*/
Utils::Parser::xmlNode::xmlNode(std::string name, std::string value)
	: _name(name), _value(value)
{
}

/**
* \fn ~xmlNode()
* \brief Destructor of a specific xmlNode
*/
Utils::Parser::xmlNode::~xmlNode()
{
	while (_childrens.size())
	{
		delete _childrens.back();
		_childrens.pop_back();
	}
}

/**
* \fn xmlNode* getNextChild() const
* \brief Getter returning the next child xmlNode.
*
* \return xmlNode*
*/
Utils::Parser::xmlNode * Utils::Parser::xmlNode::getNextChild() const
{
	return nullptr;
}

/**
* \fn void addChild(xmlNode* node)
* \brief Function adding a new child to the current childs list.
*
* \param node, use to add the new node.
* \return void
*/
void Utils::Parser::xmlNode::addChild(xmlNode * node)
{
}

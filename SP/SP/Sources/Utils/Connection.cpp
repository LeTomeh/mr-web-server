#include "Utils\Connection.hh"

/**
* \fn bool checkWebServerHeader(std::string& header)
* \brief Function checking if the header is a web server header
*
* \param header, use to get the client header
* \return bool
*/
bool Utils::Connection::checkWebServerHeader(std::string& header)
{
	std::istringstream iss(header);
	
	while (iss)
	{
		std::string sub;
		
		iss >> sub;
		if (sub == WEBSERVER_TOKEN)
		{
			iss >> sub;
			boost::system::error_code ec;
			boost::asio::ip::address::from_string(sub, ec);
			if (!ec)
				return true;
		}
	}
	return false;
}

bool Utils::Connection::checkGameServerHeader(std::string& header)
{
	std::istringstream iss(header);

	while (iss)
	{
		std::string sub;

		iss >> sub;
		if (sub == GAMESERVER_TOKEN)
		{
			iss >> sub;
			// gamename
			// IP server:portServer

/*
			boost::system::error_code ec;
			boost::asio::ip::address::from_string(sub, ec);
			if (!ec)
				return true;
*/
		}
	}
	return false;
}
/**
 * \file AssetsList.cpp
 * \brief AssetsList Class.
 * \Author Gilles Geunis
 * \version 0.1
 * \date 01/12/2016
 *
 * AssetsList class implementation.
 *
 */

#include "Asset/AssetsList.hh"

/**
 * \fn AssetsList(const std::string name, const std::string path)
 * \brief Constructor of a specific assets list.
 *
 * \return AssetsList&
 */
Asset::AssetsList::AssetsList()
{
}

/**
 * \fn ~AssetsList()
 * \brief Destructor of a specific assetsList
 */
Asset::AssetsList::~AssetsList()
{
}


/**
 * \fn unsigned long getAssetsListSize() const
 * \brief Getter returning list size of a specific assetsList.
 *
 * \return unsigned long
 */
unsigned long Asset::AssetsList::getAssetsListSize() const
{
	return _assetsList.size();
}

/**
 * \fn std::list<Asset::IAsset*> getAssetsList() const
 * \brief Getter returning list of asset of a specific assetsList.
 *
 * \return std::list<Asset::IAsset*>
 */
std::list<Asset::IAsset*> Asset::AssetsList::getAssetsList() const
{
	return _assetsList;
}

/**
 * \fn std::list<std::string> getAssetsListName() const
 * \brief Getter returning list of name of a specific assetsList.
 *
 * \return std::list<std::string>
 */
std::list<std::string> Asset::AssetsList::getAssetsListName() const
{
	return _assetsNameList;
}

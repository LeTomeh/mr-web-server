/**
 * \file Asset.cpp
 * \brief Asset Class.
 * \Author Gilles Geunis
 * \version 0.1
 * \date 01/12/2016
 *
 * Asset class implementation.
 *
 */

#include "Asset/Asset.hh"

/**
 * \fn Asset(const std::string, const std::string)
 * \brief Constructor of a specific asset.
 *
 * \param name, use to get the name of the asset.
 * \param path, use to get the path of the asset.
 * \return Asset&
 */
Asset::Asset::Asset(const std::string name, const std::string path)
{
	_name = name;
	_path = path;
	_asset = NULL;
}

/**
 * \fn ~Asset()
 * \brief Destructor of a specific asset
 */
Asset::Asset::~Asset()
{
}

/**
 * \fn std::string getPath() const
 * \brief Getter returning path of a specific asset.
 *
 * \return std::string
 */
std::string Asset::Asset::getPath() const
{
	return _path;
}

/**
 * \fn std::string getName() const
 * \brief Getter returning name of a specific asset.
 *
 * \return std::string
 */
std::string Asset::Asset::getName() const
{
	return _name;
}

/**
 * \fn std::string getAsset() const
 * \brief Getter returning the class of a specific asset.
 *
 * \return void*
 */
void * Asset::Asset::getAsset() const
{
//	if (_asset == NULL)
//		_asset = read(path);
	return _asset;
}

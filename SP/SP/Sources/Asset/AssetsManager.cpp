/**
* \file AssetsManager.cpp
* \brief AssetsManager Class.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* AssetsManager class implementation.
*
*/

#include "Asset/AssetsManager.hh"

/**
* \fn AssetsManager(const std::string, Logger::ILogger*))
* \brief Constructor of a specific assetsManager.
*
* \param rootPathForAssets, use to get the path of modules.
* \param logger, use to get the logger.
* \return AssetsManager&
*/
Asset::AssetsManager::AssetsManager(const std::string rootPathForAssets, Logger::ILogger * logger)
	: _rootPathForAssets(rootPathForAssets)
{
	_logger = logger;
}

/**
* \fn ~AssetsManager()
* \brief Destructor of a specific assetsManager
*/
Asset::AssetsManager::~AssetsManager()
{
}

/**
* \fn std::string addNewAssets(unsigned long, std::string)
* \brief Function adding asset from a specfic path to the specfic module.
*
* \param moduleID, use to get the id of the module.
* \param path, use to get the path of the specific asset.
* \return void
*/
void Asset::AssetsManager::addNewAssets(unsigned long moduleID, std::string path)
{
}

/**
* \fn unsigned long getModuleAssetsListSize(unsigned long) const
* \brief Getter returning size of a specific assetList for a specific module.
*
* \param moduleID, using to get the id of the module.
* \return unsigned long
*/
unsigned long Asset::AssetsManager::getModuleAssetsListSize(unsigned long moduleID) const
{
	return _moduleAssetsList.size();
}

/**
* \fn Asset::IAssetsList* getModuleAssetsList(unsigned long) const
* \brief Getter returning a pointer on a specific assetList for a specific module.
*
* \param moduleID, using to get the id of the module.
* \return Asset::IAssetsList
*/
Asset::IAssetsList * Asset::AssetsManager::getModuleAssetsList(unsigned long moduleID) const
{
	return (_moduleAssetsList.front() + moduleID);
}

/**
* \fn std::list<std::string> getModuleAssetsListName(unsigned long) const
* \brief Getter returning a list of name on a specific assetList for a specific module.
*
* \param moduleID, using to get the id of the module.
* \return std::list<std::string>
*/
std::list<std::string> Asset::AssetsManager::getModuleAssetsListName(unsigned long moduleID) const
{
	return (_moduleAssetsList.front() + moduleID)->getAssetsListName();
}

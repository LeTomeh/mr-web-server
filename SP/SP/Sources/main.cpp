#include "Core/Server.hh"
#include <iostream>
int main()
{
	try {
		Core::IServer*	server = new Core::Server();
		server->init();
		server->run();
		delete (server);
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
	return 0;
}
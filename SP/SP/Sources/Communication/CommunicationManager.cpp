/**
* \file CommunicationManager.cpp
* \brief CommunicationManager Class.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CommunicationManager class implementation.
*
*/

#include "Communication/CommunicationManager.hh"
/**
* \fn CommunicationManager(Client::IClientManager*, Task::ITaskManager*, Logger::ILogger*)
* \brief Constructor of a specific CommunicationManager.
*
* \param clientManager, use to get the clientManager.
* \param taskManager, use to get the taskManager.
* \param logger, use to get the logger.
* \return CommunicationManager&
*/
Communication::CommunicationManager::CommunicationManager(Client::IClientManager * clientManager, Task::ITaskManager * taskManager, Logger::ILogger * logger, boost::asio::io_service& service, short port)
	: _acceptor(service)
{
	boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::tcp::v4(), port);
	_acceptor.open(endpoint.protocol());
	_acceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
	_logger->newLog(new Logger::Log("Binding server on " + endpoint + ":" + port, "CommunicationManager"));
	_acceptor.bind(endpoint);
	_logger->newLog(new Logger::Log("Server is now listening", "CommunicationManager"));
	_acceptor.listen();
	_port = port;
	_logger = logger;
	_clientManager = clientManager;
	_taskManager = taskManager;
	handleIOSocket();
}

/**
* \fn ~CommunicationManager()
* \brief Destructor of a specific CommunicationManager
*/
Communication::CommunicationManager::~CommunicationManager()
{
}

/**
* \fn void init(short)
* \brief Function to init a specific CommunictionManager.
*
* \param port, use to init the server socket.
* \return void
*/
void Communication::CommunicationManager::init(short port)
{
	if (port != _port)
	{
		_port = port;
		_acceptor.bind(boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port));
	}
}

/**
* \fn void clean()
* \brief Function to clear a specific CommunictionManager.
*
* \return void
*/
void Communication::CommunicationManager::clean()
{
}

/**
* \fn void handleIOSocket()
* \brief Function to handle every Input and Output data for a specific CommunictionManager.
*
* \return void
*/
void Communication::CommunicationManager::handleIOSocket()
{
	Client::IClient* newConnection = new Client::Client(_acceptor.get_io_service(), _taskManager);

	_acceptor.async_accept(newConnection->socket(),
		boost::bind(&Communication::CommunicationManager::addNewClient, this, newConnection,
			boost::asio::placeholders::error));
}

/**
* \fn void sendFileToServer(unsigned long, std::string)
* \brief Function to send a file to a specific server.
*
* \param serverID, use to get server id.
* \param path, use to get the path of the file to send.
* \return void
*/
void Communication::CommunicationManager::sendFileToServer(unsigned long serverID, std::string path)
{
}

/**
* \fn void sendFileToServer(unsigned long, unsigned long)
* \brief Function to receive a file from a specific server.
*
* \param serverID, use to get server id.
* \param moduleID, use to get the module for the file.
* \return void
*/
void Communication::CommunicationManager::recvFileFromServer(unsigned long serverID, unsigned long moduleID)
{
}

/**
* \fn void addNewClient(boost::asio::ip::tcp::socket)
* \brief Function to add a client to the client list.
*
* \param clientSocket, use to get client socket.
* \return void
*/
void Communication::CommunicationManager::addNewClient(Client::IClient* newConnection, const boost::system::error_code& error)
{
	if (!error)
	{
		_clientManager->addClient(newConnection);
		newConnection->doRead();
	}
	else
		_logger->newLog(new Logger::Log("An error occured on client connection : " + error.message(), "CommunicationManager"));
	handleIOSocket();
}


/**
* \fn void removeClient(unsigned long)
* \brief Function to remove a client of the client list.
*
* \param clientID, use to get client id.
* \return void
*/
void Communication::CommunicationManager::removeClient(unsigned long clientID)
{
}

/**
* \fn Client::IClient* createConnexion();
* \brief Function creating connexion between server and module.
*
* \return Client::IClient*
*/
Client::IClient * Communication::CommunicationManager::createConnexion()
{
	Client::IClient* newConnexion = new Client::Client(_acceptor.get_io_service(), _taskManager);
	std::string host_name = "127.0.0.1";
	std::string port = "7730";
	boost::asio::ip::tcp::resolver resolver(_acceptor.get_io_service());
	boost::asio::ip::tcp::resolver::query query(host_name, port);
	boost::asio::ip::tcp::endpoint remote_endpoint = *resolver.resolve(query);

	try {
		newConnexion->socket().connect(remote_endpoint);
		return newConnexion;
	}
	catch (const std::exception& e) {
		std::cerr << e.what() << std::endl;
		return nullptr;
	}
}

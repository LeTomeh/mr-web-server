/**
* \file Logger.cpp
* \brief Logger Class.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* Logger class implementation.
*
*/

#include "Logger/Logger.hh"

/**
* \fn Logger(const unsigned int)
* \brief Constructor of a specific Logger.
*
* \param historicSize, use to set the size of the historic.
* \return Logger&
*/
Logger::Logger::Logger(const unsigned int historicSize) : _historicLogSize(historicSize)
{
}

/**
* \fn ~Logger()
* \brief Destructor of a specific Logger
*/
Logger::Logger::~Logger()
{
}

/**
* \fn void newLog(const ILog*)
* \brief Function for adding new log to the logger.
*
* \param log, use to get the value of the new log.
* \return void
*/
void Logger::Logger::newLog(ILog * log)
{
	if (_lastLog.size() == _historicLogSize)
		_lastLog.pop_front();
	_lastLog.push_back(log);
}

/**
* \fn std::list<Logger::ILog*> getLastLog(size) const
* \brief Getter returning the log history of a specific logger.
*
* \param size, use to get the quantity of log that we are asking.
* \return std::list<Logger::ILog*>
*/
std::list<Logger::ILog*> Logger::Logger::getLastLog() const
{
	return _lastLog;
}

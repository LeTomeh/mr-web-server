/**
* \file Log.cpp
* \brief Log Class.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* Log class implementation.
*
*/

#include "Logger/Log.hh"

/**
* \fn Log(const std::string, const std::string)
* \brief Constructor of a specific Log.
*
* \param log, use to get the value of the log.
* \param logProvenance, use to get the class who create the log.
* \return Log&
*/
Logger::Log::Log(const std::string log, const std::string logProvenance, const bool isError)
	: _log(log), _logProvenance(logProvenance), _isError(isError)
{
}

/**
* \fn ~Log()
* \brief Destructor of a specific Log.
*/
Logger::Log::~Log()
{
}

/**
* \fn std::string getLog() const
* \brief Getter returning the value of a specific log.
*
* \return std::string
*/
std::string Logger::Log::getLog() const
{
	return _log;
}

/**
* \fn std::string getLogProvenance() const
* \brief Getter returning the class who create the log for a specific log.
*
* \return std::string
*/
std::string Logger::Log::getLogProvenance() const
{
	return _logProvenance;
}

/**
* \fn bool isError() const
* \brief Getter to know if the log is an error.
*
* \return bool
*/
bool Logger::Log::isError() const
{
	return _isError;
}

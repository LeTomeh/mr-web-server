/**
* \file TaskManager.cpp
* \brief TaskManager Class.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* TaskManager class implementation.
*
*/

#include "Task/TaskManager.hh"

/**
* \fn TaskManager(Core::IServer*, unsigned int)
* \brief Constructor of a specific TaskManager.
*
* \param server, use to attach server.
* \param worker, use to get the number of workers.
* \return TaskManager&
*/
Task::TaskManager::TaskManager(Core::IServer * server, Logger::ILogger* logger, unsigned int workers)
	: _worker(_service), _logger(logger)
{
	_server = server;

	for (unsigned int i = 0; i < workers; i++)
	{
		_threadpool.create_thread(
			boost::bind(&boost::asio::io_service::run, &_service)
		);
	}
}

/**
* \fn ~TaskManager()
* \brief Destructor of a specific TaskManager
*/
Task::TaskManager::~TaskManager()
{
	_service.stop();
	_threadpool.join_all();
}

/**
* \fn void addTask(ITask*)
* \brief Function adding a task to a specific taskManager.
*
* \param task, use to add the task.
* \return void
*/
void Task::TaskManager::addTask(ITask * task)
{
	Worker* worker = new Worker(dynamic_cast<ITaskManager*>(this));

	if (task)
		_service.post(boost::bind(&Worker::doTask, worker, _server, task));
}

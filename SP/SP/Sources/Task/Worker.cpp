/**
* \file Worker.cpp
* \brief Worker Class.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* Worker class implementation.
*
*/

#include "Task/Worker.hh"

/**
* \fn Worker()
* \brief Constructor of a specific Worker.
*
* \return Worker&
*/
Task::Worker::Worker(ITaskManager* taskManager)
{
	_taskManager = taskManager;
	_task[LAUNCH_MODULE] = &Worker::launchModule;
	_task[STOP_MODULE] = &Worker::stopModule;
	_task[PAUSE_MODULE] = &Worker::pauseModule;
	_task[ADD_CLIENT] = &Worker::addUserToModule;
//	_task[PAUSE_CLIENT] = &Worker::pauseClient;
//	_task[CLOSE_CLIENT] = &Worker::closeClient;
	_task[LIST_MODULE] = &Worker::listModule;
	_task[LIST_MODULE_AR] = &Worker::listModuleAR;
	_task[LIST_MODULE_VR] = &Worker::listModuleVR;
	_task[LIST_MODULE_ON] = &Worker::listModuleON;
	_task[LIST_MODULE_OFF] = &Worker::listModuleOFF;
	_task[LIST_ALLMODULE] = &Worker::listAll;
	_task[SWITCHING_CONNECTION] = &Worker::switchConnectionType;
	_task[HOSTING_CONNECTION] = &Worker::hostingNewConnection;
	_task[UPLOAD_FILE] = &Worker::uploadPackage;
	_task[DOWNLOAD_FILE] = &Worker::downloadPackage;
	_task[RESPONSE] = &Worker::sendResponse;
}

/**
* \fn ~Worker()
* \brief Destructor of a specific Worker
*/
Task::Worker::~Worker()
{
}

/**
* \fn void yield()
* \brief Function to make a worker yield.
*
* \return void
*/
void Task::Worker::yield()
{
	boost::this_thread::yield();
}

/**
* \fn void doTask(Core::IServer* server, ITask* task)
* \brief Function handling a task for a specific worker.
*
* \param server, used to access data.
* \param task, used to acces task informations
* \return void
*/
void Task::Worker::doTask(Core::IServer* server, ITask* task)
{
	(this->*(_task[task->getTaskID()]))(server, task);
}

/**
* \fn bool isBusy() const
* \brief Getter returning a boolean associated to the activity of a specific worker.
*
* \return bool
*/
bool Task::Worker::isBusy() const
{
	return false;
}

/**
 * \fn void listAll(Core::IServer* server, Task::ITask* task)
 * \brief Function listing all modules for a specific client
 *
 * \param server, used to access to the server
 * \param task, used to access to the necessary data
 *
 * \return void
 */
void Task::Worker::listAll(Core::IServer * server, Task::ITask * task)
{
	std::map<std::string, void*> map;
	std::vector<Module::IModule*> moduleList;

	moduleList = server->getModuleManager()->getModuleList();
	if (moduleList.size() > 0)
	{
		map["RESPONSE"] = static_cast<void*>(new std::string("200 " + std::to_string(moduleList.size()) + "\r\n"));
		for (int it = 0; it < moduleList.size(); it++)
		{
			Module::IModule* actualmodule = moduleList.front();
			static_cast<std::string*>(map["RESPONSE"])->append("210 " + moduleList[it]->getName() + "\r\n211 " + std::to_string(moduleList[it]->getID()) + "\r\n");
			static_cast<std::string*>(map["RESPONSE"])->append("212 ");
			if (moduleList[it]->getDescription()->getDescriptionSize())
			{
				static_cast<std::string*>(map["RESPONSE"])->append(moduleList[it]->getDescription()->getDescriptionSize() + " " + moduleList[it]->getDescription()->getDescription());
			}
			static_cast<std::string*>(map["RESPONSE"])->append("\r\n");
			static_cast<std::string*>(map["RESPONSE"])->append((moduleList[it]->allowVR() ? "213 \r\n" : ""));
			static_cast<std::string*>(map["RESPONSE"])->append((moduleList[it]->allowAR() ? "214 \r\n" : ""));
			static_cast<std::string*>(map["RESPONSE"])->append("215 ");
			if (moduleList[it]->getUserList() && moduleList[it]->getUserList()->getUserListSize())
			{
				static_cast<std::string*>(map["RESPONSE"])->append("" + moduleList[it]->getUserList()->getUserListSize());
				for (auto itUser = moduleList[it]->getUserList()->getUserList().begin(); itUser != moduleList[it]->getUserList()->getUserList().end(); itUser++)
					static_cast<std::string*>(map["RESPONSE"])->append(" " + (*itUser));
			}
			else
				static_cast<std::string*>(map["RESPONSE"])->append("0");
			static_cast<std::string*>(map["RESPONSE"])->append("\r\n");
			static_cast<std::string*>(map["RESPONSE"])->append((moduleList[it]->getModuleDisponibility() == Module::CLOSED ? "216 \r\n" : ""));
			static_cast<std::string*>(map["RESPONSE"])->append((moduleList[it]->getModuleDisponibility() == Module::LAUNCH ? "217 \r\n" : ""));
			static_cast<std::string*>(map["RESPONSE"])->append("218 ");
			if (server->getModuleManager()->getModuleAssetsListSize(moduleList[it]->getID()))
			{
				static_cast<std::string*>(map["RESPONSE"])->append("" + server->getModuleManager()->getModuleAssetsListSize(moduleList[it]->getID()));
				for (auto itAsset = server->getModuleManager()->getModuleAssetsListName(moduleList[it]->getID()).begin(); itAsset != server->getModuleManager()->getModuleAssetsListName(moduleList[it]->getID()).end(); itAsset++)
					static_cast<std::string*>(map["RESPONSE"])->append((*itAsset));
			}
			else
				static_cast<std::string*>(map["RESPONSE"])->append("0");
			static_cast<std::string*>(map["RESPONSE"])->append("\r\n");
			static_cast<std::string*>(map["RESPONSE"])->append("260 \r\n");
		}
	}
	else
		map["RESPONSE"] = static_cast<void*>(new std::string("620 \r\n"));
	static_cast<std::string*>(map["RESPONSE"])->append("270 \r\n");
	server->getLogger()->newLog(new Logger::Log("Sending presentation of every module", "Worker"));
	map["RECEIVER"] = task->getDatas("SENDER");
	delete task;
	_taskManager->addTask(new Task(RESPONSE, map));
}

/**
* \fn void listModule(Core::IServer* server, Task::ITask* task)
* \brief Function listing a specific modules for a specific client
*
* \param server, used to access to the server
* \param task, used to access to the necessary data
*
* \return void
*/
void Task::Worker::listModule(Core::IServer * server, Task::ITask * task)
{
	std::map<std::string, void*> map;
	std::string* idModule;
	Module::IModule* module = nullptr;

	idModule = static_cast<std::string*>(task->getDatas("ID_MODULE"));
	module = server->getModuleManager()->getModule(std::strtoul(idModule->c_str(), NULL, 0));
	if (!module)
	{
		server->getLogger()->newLog(new Logger::Log("Can't find module of id [" + *idModule + "]", "Worker", true));
		map["RESPONSE"] = static_cast<void*>(new std::string("625 \r\n"));
	}
	else
	{
		map["RESPONSE"] = static_cast<void*>(new std::string("210 " + module->getName() + "\r\n211 " + idModule->c_str() + "\r\n"));
		static_cast<std::string*>(map["RESPONSE"])->append("212 ");
		if (module->getDescription()->getDescriptionSize())
		{
			static_cast<std::string*>(map["RESPONSE"])->append(module->getDescription()->getDescriptionSize() + " " + module->getDescription()->getDescription());
		}
		static_cast<std::string*>(map["RESPONSE"])->append("\r\n");
		static_cast<std::string*>(map["RESPONSE"])->append((module->allowVR() ? "213 \r\n" : ""));
		static_cast<std::string*>(map["RESPONSE"])->append((module->allowAR() ? "214 \r\n" : ""));
		static_cast<std::string*>(map["RESPONSE"])->append("215 ");
		if (module->getUserList() && module->getUserList()->getUserListSize())
		{
			static_cast<std::string*>(map["RESPONSE"])->append("" + module->getUserList()->getUserListSize());
			for (auto it = module->getUserList()->getUserList().begin(); it != module->getUserList()->getUserList().end(); it++)
				static_cast<std::string*>(map["RESPONSE"])->append(" " + (*it));
		}
		else
			static_cast<std::string*>(map["RESPONSE"])->append("0");
		static_cast<std::string*>(map["RESPONSE"])->append("\r\n");
		static_cast<std::string*>(map["RESPONSE"])->append((module->getModuleDisponibility() == Module::CLOSED ? "216 \r\n" : ""));
		static_cast<std::string*>(map["RESPONSE"])->append((module->getModuleDisponibility() == Module::LAUNCH ? "217 \r\n" : ""));
		static_cast<std::string*>(map["RESPONSE"])->append("218 ");
		if (server->getModuleManager()->getModuleAssetsListSize(std::strtoul(idModule->c_str(), NULL, 0)))
		{
			static_cast<std::string*>(map["RESPONSE"])->append("" + server->getModuleManager()->getModuleAssetsListSize(std::strtoul(idModule->c_str(), NULL, 0)));
			for (auto it = server->getModuleManager()->getModuleAssetsListName(std::strtoul(idModule->c_str(), NULL, 0)).begin(); it != server->getModuleManager()->getModuleAssetsListName(std::strtoul(idModule->c_str(), NULL, 0)).end(); it++)
				static_cast<std::string*>(map["RESPONSE"])->append((*it));
		}
		else
			static_cast<std::string*>(map["RESPONSE"])->append("0");
		static_cast<std::string*>(map["RESPONSE"])->append("\r\n");
		static_cast<std::string*>(map["RESPONSE"])->append("260 \r\n");
		server->getLogger()->newLog(new Logger::Log("Sending presentation of module of id [" + *idModule + "]", "Worker"));
	}
	map["RECEIVER"] = task->getDatas("SENDER");
	delete task->getDatas("ID_MODULE");
	delete task;
	_taskManager->addTask(new Task(RESPONSE, map));
}

/**
* \fn void listModuleON(Core::IServer* server, Task::ITask* task)
* \brief Function listing every modules online for a specific client
*
* \param server, used to access to the server
* \param task, used to access to the necessary data
*
* \return void
*/
void Task::Worker::listModuleON(Core::IServer * server, Task::ITask * task)
{
	std::map<std::string, void*> map;
	std::list<unsigned long> moduleList;

	moduleList = server->getModuleManager()->getModuleLaunched();
	if (moduleList.size() > 0)
	{
		map["RESPONSE"] = static_cast<void*>(new std::string("204 " + std::to_string(moduleList.size()) + "\r\n"));
		for (auto it = moduleList.begin(); it != moduleList.end(); it++)
		{
			Module::IModule* module= server->getModuleManager()->getModule(*it);
			static_cast<std::string*>(map["RESPONSE"])->append("210 " + module->getName() + "\r\n211 " + std::to_string(module->getID()) + "\r\n");
			static_cast<std::string*>(map["RESPONSE"])->append("212 ");
			if (module->getDescription()->getDescriptionSize())
			{
				static_cast<std::string*>(map["RESPONSE"])->append(module->getDescription()->getDescriptionSize() + " " + module->getDescription()->getDescription());
			}
			static_cast<std::string*>(map["RESPONSE"])->append("\r\n");
			static_cast<std::string*>(map["RESPONSE"])->append((module->allowVR() ? "213 \r\n" : ""));
			static_cast<std::string*>(map["RESPONSE"])->append((module->allowAR() ? "214 \r\n" : ""));
			static_cast<std::string*>(map["RESPONSE"])->append("215 ");
			if (module->getUserList() && module->getUserList()->getUserListSize())
			{
				static_cast<std::string*>(map["RESPONSE"])->append("" + module->getUserList()->getUserListSize());
				for (auto itUser = module->getUserList()->getUserList().begin(); itUser != module->getUserList()->getUserList().end(); itUser++)
					static_cast<std::string*>(map["RESPONSE"])->append(" " + (*itUser));
			}
			else
				static_cast<std::string*>(map["RESPONSE"])->append("0");
			static_cast<std::string*>(map["RESPONSE"])->append("\r\n");
			static_cast<std::string*>(map["RESPONSE"])->append((module->getModuleDisponibility() == Module::CLOSED ? "216 \r\n" : ""));
			static_cast<std::string*>(map["RESPONSE"])->append((module->getModuleDisponibility() == Module::LAUNCH ? "217 \r\n" : ""));
			static_cast<std::string*>(map["RESPONSE"])->append("218 ");
			if (server->getModuleManager()->getModuleAssetsListSize(module->getID()))
			{
				static_cast<std::string*>(map["RESPONSE"])->append("" + server->getModuleManager()->getModuleAssetsListSize(module->getID()));
				for (auto itAsset = server->getModuleManager()->getModuleAssetsListName(module->getID()).begin(); itAsset != server->getModuleManager()->getModuleAssetsListName(module->getID()).end(); itAsset++)
					static_cast<std::string*>(map["RESPONSE"])->append((*itAsset));
			}
			else
				static_cast<std::string*>(map["RESPONSE"])->append("0");
			static_cast<std::string*>(map["RESPONSE"])->append("\r\n");
			static_cast<std::string*>(map["RESPONSE"])->append("260 \r\n");
		}
	}
	else
		map["RESPONSE"] = static_cast<void*>(new std::string("623 \r\n"));
	static_cast<std::string*>(map["RESPONSE"])->append("270 \r\n");
	server->getLogger()->newLog(new Logger::Log("Sending presentation of launched module", "Worker"));
	map["RECEIVER"] = task->getDatas("SENDER");
	delete task;
	_taskManager->addTask(new Task(RESPONSE, map));
}

/**
* \fn void listModuleOFF(Core::IServer* server, Task::ITask* task)
* \brief Function listing every modules offline for a specific client
*
* \param server, used to access to the server
* \param task, used to access to the necessary data
*
* \return void
*/
void Task::Worker::listModuleOFF(Core::IServer * server, Task::ITask * task)
{
	std::map<std::string, void*> map;
	std::list<unsigned long> moduleList;

	moduleList = server->getModuleManager()->getModuleClosed();
	if (moduleList.size() > 0)
	{
		map["RESPONSE"] = static_cast<void*>(new std::string("203 " + std::to_string(moduleList.size()) + "\r\n"));
		for (auto it = moduleList.begin(); it != moduleList.end(); it++)
		{
			Module::IModule* module = server->getModuleManager()->getModule(*it);
			static_cast<std::string*>(map["RESPONSE"])->append("210 " + module->getName() + "\r\n211 " + std::to_string(module->getID()) + "\r\n");
			static_cast<std::string*>(map["RESPONSE"])->append("212 ");
			if (module->getDescription()->getDescriptionSize())
			{
				static_cast<std::string*>(map["RESPONSE"])->append(module->getDescription()->getDescriptionSize() + " " + module->getDescription()->getDescription());
			}
			static_cast<std::string*>(map["RESPONSE"])->append("\r\n");
			static_cast<std::string*>(map["RESPONSE"])->append((module->allowVR() ? "213 \r\n" : ""));
			static_cast<std::string*>(map["RESPONSE"])->append((module->allowAR() ? "214 \r\n" : ""));
			static_cast<std::string*>(map["RESPONSE"])->append("215 ");
			if (module->getUserList() && module->getUserList()->getUserListSize())
			{
				static_cast<std::string*>(map["RESPONSE"])->append("" + module->getUserList()->getUserListSize());
				for (auto itUser = module->getUserList()->getUserList().begin(); itUser != module->getUserList()->getUserList().end(); itUser++)
					static_cast<std::string*>(map["RESPONSE"])->append(" " + (*itUser));
			}
			else
				static_cast<std::string*>(map["RESPONSE"])->append("0");
			static_cast<std::string*>(map["RESPONSE"])->append("\r\n");
			static_cast<std::string*>(map["RESPONSE"])->append((module->getModuleDisponibility() == Module::CLOSED ? "216 \r\n" : ""));
			static_cast<std::string*>(map["RESPONSE"])->append((module->getModuleDisponibility() == Module::LAUNCH ? "217 \r\n" : ""));
			static_cast<std::string*>(map["RESPONSE"])->append("218 ");
			if (server->getModuleManager()->getModuleAssetsListSize(module->getID()))
			{
				static_cast<std::string*>(map["RESPONSE"])->append("" + server->getModuleManager()->getModuleAssetsListSize(module->getID()));
				for (auto itAsset = server->getModuleManager()->getModuleAssetsListName(module->getID()).begin(); itAsset != server->getModuleManager()->getModuleAssetsListName(module->getID()).end(); itAsset++)
					static_cast<std::string*>(map["RESPONSE"])->append((*itAsset));
			}
			else
				static_cast<std::string*>(map["RESPONSE"])->append("0");
			static_cast<std::string*>(map["RESPONSE"])->append("\r\n");
			static_cast<std::string*>(map["RESPONSE"])->append("260 \r\n");
		}
	}
	else
		map["RESPONSE"] = static_cast<void*>(new std::string("624\r\n"));
	static_cast<std::string*>(map["RESPONSE"])->append("270 \r\n");
	server->getLogger()->newLog(new Logger::Log("Sending presentation of closed module", "Worker"));
	map["RECEIVER"] = task->getDatas("SENDER");
	delete task;
	_taskManager->addTask(new Task(RESPONSE, map));
}

/**
* \fn void listModuleVR(Core::IServer* server, Task::ITask* task)
* \brief Function listing every modules allowing VR for a specific client
*
* \param server, used to access to the server
* \param task, used to access to the necessary data
*
* \return void
*/
void Task::Worker::listModuleVR(Core::IServer * server, Task::ITask * task)
{
	std::map<std::string, void*> map;
	std::list<unsigned long> moduleList;

	moduleList = server->getModuleManager()->getModuleVRList();
	if (moduleList.size() > 0)
	{
		map["RESPONSE"] = static_cast<void*>(new std::string("201 " + std::to_string(moduleList.size()) + "\r\n"));
		for (auto it = moduleList.begin(); it != moduleList.end(); it++)
		{
			Module::IModule* module = server->getModuleManager()->getModule(*it);
			static_cast<std::string*>(map["RESPONSE"])->append("210 " + module->getName() + "\r\n211 " + std::to_string(module->getID()) + "\r\n");
			static_cast<std::string*>(map["RESPONSE"])->append("212 ");
			if (module->getDescription()->getDescriptionSize())
			{
				static_cast<std::string*>(map["RESPONSE"])->append(module->getDescription()->getDescriptionSize() + " " + module->getDescription()->getDescription());
			}
			static_cast<std::string*>(map["RESPONSE"])->append("\r\n");
			static_cast<std::string*>(map["RESPONSE"])->append((module->allowVR() ? "213 \r\n" : ""));
			static_cast<std::string*>(map["RESPONSE"])->append((module->allowAR() ? "214 \r\n" : ""));
			static_cast<std::string*>(map["RESPONSE"])->append("215 ");
			if (module->getUserList() && module->getUserList()->getUserListSize())
			{
				static_cast<std::string*>(map["RESPONSE"])->append("" + module->getUserList()->getUserListSize());
				for (auto itUser = module->getUserList()->getUserList().begin(); itUser != module->getUserList()->getUserList().end(); itUser++)
					static_cast<std::string*>(map["RESPONSE"])->append(" " + (*itUser));
			}
			else
				static_cast<std::string*>(map["RESPONSE"])->append("0");
			static_cast<std::string*>(map["RESPONSE"])->append("\r\n");
			static_cast<std::string*>(map["RESPONSE"])->append((module->getModuleDisponibility() == Module::CLOSED ? "216 \r\n" : ""));
			static_cast<std::string*>(map["RESPONSE"])->append((module->getModuleDisponibility() == Module::LAUNCH ? "217 \r\n" : ""));
			static_cast<std::string*>(map["RESPONSE"])->append("218 ");
			if (server->getModuleManager()->getModuleAssetsListSize(module->getID()))
			{
				static_cast<std::string*>(map["RESPONSE"])->append("" + server->getModuleManager()->getModuleAssetsListSize(module->getID()));
				for (auto itAsset = server->getModuleManager()->getModuleAssetsListName(module->getID()).begin(); itAsset != server->getModuleManager()->getModuleAssetsListName(module->getID()).end(); itAsset++)
					static_cast<std::string*>(map["RESPONSE"])->append((*itAsset));
			}
			else
				static_cast<std::string*>(map["RESPONSE"])->append("0");
			static_cast<std::string*>(map["RESPONSE"])->append("\r\n");
			static_cast<std::string*>(map["RESPONSE"])->append("260 \r\n");
		}
	}
	else
		map["RESPONSE"] = static_cast<void*>(new std::string("621 \r\n"));
	static_cast<std::string*>(map["RESPONSE"])->append("270 \r\n");
	server->getLogger()->newLog(new Logger::Log("Sending presentation of every module with VR", "Worker"));
	map["RECEIVER"] = task->getDatas("SENDER");
	delete task;
	_taskManager->addTask(new Task(RESPONSE, map));
}

/**
* \fn void listModuleAR(Core::IServer* server, Task::ITask* task)
* \brief Function listing every modules allowing AR for a specific client
*
* \param server, used to access to the server
* \param task, used to access to the necessary data
*
* \return void
*/
void Task::Worker::listModuleAR(Core::IServer * server, Task::ITask * task)
{
	std::map<std::string, void*> map;
	std::list<unsigned long> moduleList;

	moduleList = server->getModuleManager()->getModuleARList();
	if (moduleList.size() > 0)
	{
		map["RESPONSE"] = static_cast<void*>(new std::string("202 " + std::to_string(moduleList.size()) + "\r\n"));
		for (auto it = moduleList.begin(); it != moduleList.end(); it++)
		{
			Module::IModule* module = server->getModuleManager()->getModule(*it);
			static_cast<std::string*>(map["RESPONSE"])->append("210 " + module->getName() + "\r\n211 " + std::to_string(module->getID()) + "\r\n");
			static_cast<std::string*>(map["RESPONSE"])->append("212 ");
			if (module->getDescription()->getDescriptionSize())
			{
				static_cast<std::string*>(map["RESPONSE"])->append(module->getDescription()->getDescriptionSize() + " " + module->getDescription()->getDescription());
			}
			static_cast<std::string*>(map["RESPONSE"])->append("\r\n");
			static_cast<std::string*>(map["RESPONSE"])->append((module->allowVR() ? "213 \r\n" : ""));
			static_cast<std::string*>(map["RESPONSE"])->append((module->allowAR() ? "214 \r\n" : ""));
			static_cast<std::string*>(map["RESPONSE"])->append("215 ");
			if (module->getUserList() && module->getUserList()->getUserListSize())
			{
				static_cast<std::string*>(map["RESPONSE"])->append("" + module->getUserList()->getUserListSize());
				for (auto itUser = module->getUserList()->getUserList().begin(); itUser != module->getUserList()->getUserList().end(); itUser++)
					static_cast<std::string*>(map["RESPONSE"])->append(" " + (*itUser));
			}
			else
				static_cast<std::string*>(map["RESPONSE"])->append("0");
			static_cast<std::string*>(map["RESPONSE"])->append("\r\n");
			static_cast<std::string*>(map["RESPONSE"])->append((module->getModuleDisponibility() == Module::CLOSED ? "216 \r\n" : ""));
			static_cast<std::string*>(map["RESPONSE"])->append((module->getModuleDisponibility() == Module::LAUNCH ? "217 \r\n" : ""));
			static_cast<std::string*>(map["RESPONSE"])->append("218 ");
			if (server->getModuleManager()->getModuleAssetsListSize(module->getID()))
			{
				static_cast<std::string*>(map["RESPONSE"])->append("" + server->getModuleManager()->getModuleAssetsListSize(module->getID()));
				for (auto itAsset = server->getModuleManager()->getModuleAssetsListName(module->getID()).begin(); itAsset != server->getModuleManager()->getModuleAssetsListName(module->getID()).end(); itAsset++)
					static_cast<std::string*>(map["RESPONSE"])->append((*itAsset));
			}
			else
				static_cast<std::string*>(map["RESPONSE"])->append("0");
			static_cast<std::string*>(map["RESPONSE"])->append("\r\n");
			static_cast<std::string*>(map["RESPONSE"])->append("260 \r\n");
		}
	}
	else
		map["RESPONSE"] = static_cast<void*>(new std::string("622 \r\n"));
	static_cast<std::string*>(map["RESPONSE"])->append("270 \r\n");
	server->getLogger()->newLog(new Logger::Log("Sending presentation of every module with AR", "Worker"));
	map["RECEIVER"] = task->getDatas("SENDER");
	delete task;
	_taskManager->addTask(new Task(RESPONSE, map));
}

/**
* \fn void launchModule(Core::IServer* server, Task::ITask* task)
* \brief Function launching a specfic module for a specific client
*
* \param server, used to access to the server
* \param task, used to access to the necessary data
*
* \return void
*/
void Task::Worker::launchModule(Core::IServer * server, Task::ITask * task)
{
	std::string* userId = static_cast<std::string*>(task->getDatas("ID_USER"));
	std::string* moduleId = static_cast<std::string*>(task->getDatas("ID_MODULE"));
	std::map<std::string, void*> map;

	try {
		server->getModuleManager()->launchModule(std::stoul(userId->c_str()), std::stoul(moduleId->c_str()), server);
		server->getLogger()->newLog(new Logger::Log("Module of id [" + *moduleId + "] is launched", "Worker"));
		map["RESPONSE"] = static_cast<void*>(new std::string("301\r\n"));
	}
	catch (const Module::CantLaunchModule& e)
	{
		server->getLogger()->newLog(new Logger::Log("Can't launch module of id [" + *moduleId + "]", "Worker", true));
		map["RESPONSE"] = static_cast<void*>(new std::string("625\r\n"));
	}
	map["RECEIVER"] = task->getDatas("SENDER");
	delete task->getDatas("ID_MODULE");
	delete task;
	_taskManager->addTask(new Task(RESPONSE, map));
}

/**
* \fn void pauseModule(Core::IServer* server, Task::ITask* task)
* \brief Function pausing a specfic module for a specific client
*
* \param server, used to access to the server
* \param task, used to access to the necessary data
*
* \return void
*/
void Task::Worker::pauseModule(Core::IServer * server, Task::ITask * task)
{
	std::string* moduleId = static_cast<std::string*>(task->getDatas("ID_MODULE"));
	std::map<std::string, void*> map;

	try {
		server->getModuleManager()->pauseModule(std::stoul(moduleId->c_str()));
		server->getLogger()->newLog(new Logger::Log("Module of id [" + *moduleId + "] is paused", "Worker"));
		map["RESPONSE"] = static_cast<void*>(new std::string("303\r\n"));
	}
	catch (const Module::CantPauseModule& e)
	{
		server->getLogger()->newLog(new Logger::Log("Can't pause module of id [" + *moduleId + "]", "Worker", true));
		map["RESPONSE"] = static_cast<void*>(new std::string("625\r\n"));
	}
	map["RECEIVER"] = task->getDatas("SENDER");
	delete task->getDatas("ID_MODULE");
	delete task;
	_taskManager->addTask(new Task(RESPONSE, map));
}

/**
* \fn void addUserToModule(Core::IServer* server, Task::ITask* task)
* \brief Function adding a specific client to a specific module
*
* \param server, used to access to the server
* \param task, used to access to the necessary data
*
* \return void
*/
void Task::Worker::addUserToModule(Core::IServer * server, Task::ITask * task)
{
	std::string* moduleId = static_cast<std::string*>(task->getDatas("ID_MODULE"));
	std::string* userID = static_cast<std::string*>(task->getDatas("ID_USER"));
	std::map<std::string, void*> map;

	try {
		server->getModuleManager()->addClientToModule(std::stoul(userID->c_str()), std::stoul(moduleId->c_str()));
		server->getLogger()->newLog(new Logger::Log("User of id [" + *userID + "] added to the module of id ["  + *moduleId + "]", "Worker"));
		map["RESPONSE"] = static_cast<void*>(new std::string("302\r\n"));
	}
	catch (const Module::CantFindModule& e)
	{
		server->getLogger()->newLog(new Logger::Log("Can't add user of id [" + *userID + "] to the module of id [" + *moduleId + "]", "Worker", true));
		map["RESPONSE"] = static_cast<void*>(new std::string("625\r\n"));
	}
	map["RECEIVER"] = task->getDatas("SENDER");
	delete task->getDatas("ID_MODULE");
	delete task->getDatas("ID_USER");
	delete task;
	_taskManager->addTask(new Task(RESPONSE, map));
}

/**
* \fn void logoutUser(Core::IServer* server, Task::ITask* task)
* \brief Function disconnecting a specific client to a specific module
*
* \param server, used to access to the server
* \param task, used to access to the necessary data
*
* \return void
*/
void Task::Worker::logoutUser(Core::IServer * server, Task::ITask * task)
{
	std::string* moduleId = static_cast<std::string*>(task->getDatas("ID_MODULE"));
	std::string* userID = static_cast<std::string*>(task->getDatas("ID_USER"));
	std::map<std::string, void*> map;

	try {
		server->getModuleManager()->removeClientToModule(std::stoul(userID->c_str()), std::stoul(moduleId->c_str()));
		server->getLogger()->newLog(new Logger::Log("User of id [" + *userID + "] removed to the module of id [" + *moduleId + "]", "Worker"));
		map["RESPONSE"] = static_cast<void*>(new std::string("302\r\n"));
	}
	catch (const Module::CantFindModule& e)
	{
		server->getLogger()->newLog(new Logger::Log("Can't remove user of id [" + *userID + "] to the module of id [" + *moduleId + "]", "Worker", true));
		map["RESPONSE"] = static_cast<void*>(new std::string("625\r\n"));
	}
	map["RECEIVER"] = task->getDatas("SENDER");
	delete task->getDatas("ID_MODULE");
	delete task->getDatas("ID_USER");
	delete task;
	_taskManager->addTask(new Task(RESPONSE, map));
}

/**
* \fn void stopModule(Core::IServer* server, Task::ITask* task)
* \brief Function disconnecting a specific module
*
* \param server, used to access to the server
* \param task, used to access to the necessary data
*
* \return void
*/
void Task::Worker::stopModule(Core::IServer * server, Task::ITask * task)
{
	std::string* moduleId = static_cast<std::string*>(task->getDatas("ID_MODULE"));
	std::map<std::string, void*> map;

	try {
		server->getModuleManager()->stopModule(std::stoul(moduleId->c_str()));
		server->getLogger()->newLog(new Logger::Log("Module of id [" + *moduleId + "] is stopped", "Worker"));
		map["RESPONSE"] = static_cast<void*>(new std::string("305\r\n"));
	}
	catch (const Module::CantStopModule& e)
	{
		server->getLogger()->newLog(new Logger::Log("Can't stop module of id [" + *moduleId + "]", "Worker", true));
		map["RESPONSE"] = static_cast<void*>(new std::string("625\r\n"));
	}
	map["RECEIVER"] = task->getDatas("SENDER");
	delete task->getDatas("ID_MODULE");
	delete task;
	_taskManager->addTask(new Task(RESPONSE, map));
}

/**
* \fn void switchConnectionType(Core::IServer* server, Task::ITask* task)
* \brief Function switching connection type for a specific web server
*
* \param server, used to access to the server
* \param task, used to access to the necessary data
*
* \return void
*/
void Task::Worker::switchConnectionType(Core::IServer * server, Task::ITask * task)
{
}

/**
* \fn void hostingNewConnection(Core::IServer* server, Task::ITask* task)
* \brief Function to manage the connection for a specific web server
*
* \param server, used to access to the server
* \param task, used to access to the necessary data
*
* \return void
*/
void Task::Worker::hostingNewConnection(Core::IServer * server, Task::ITask * task)
{
}

/**
* \fn void uploadPackage(Core::IServer* server, Task::ITask* task)
* \brief Function uploading a module for a specific web server
*
* \param server, used to access to the server
* \param task, used to access to the necessary data
*
* \return void
*/
void Task::Worker::uploadPackage(Core::IServer * server, Task::ITask * task)
{
}

/**
* \fn void downloadPackage(Core::IServer* server, Task::ITask* task)
* \brief Function downloading a module for a specific web server
*
* \param server, used to access to the server
* \param task, used to access to the necessary data
*
* \return void
*/
void Task::Worker::downloadPackage(Core::IServer * server, Task::ITask * task)
{
}

/**
* \fn void sendResponse(Core::IServer* server, Task::ITask* task)
* \brief Function sending response for a specific client
*
* \param server, used to access to the server
* \param task, used to access to the necessary data
*
* \return void
*/
void Task::Worker::sendResponse(Core::IServer * server, Task::ITask * task)
{
	std::string* clientID = static_cast<std::string*>(task->getDatas("RECEIVER"));
	std::string* response = static_cast<std::string*>(task->getDatas("RESPONSE"));

	server->getClientManager()->getClient(std::stoul(clientID->c_str()))->addToResponseStream(*response);
	server->getClientManager()->getClient(std::stoul(clientID->c_str()))->doWrite();
	delete task->getDatas("RECEIVER");
	delete task->getDatas("RESPONSE");
	delete task;

}

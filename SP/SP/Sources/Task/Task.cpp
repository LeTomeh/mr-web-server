/**
* \file Task.cpp
* \brief Task Class.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* Task class implementation.
*
*/

#include "Task/Task.hh"

/**
* \fn Task(e_TaskID taskID, std::map<std::string, void*> data)
* \brief Constructor of a specific Task.
*
* \param taskID, use to get the type of the task.
* \param data, use to get the data of the task.
* \return Task&
*/
Task::Task::Task(e_TaskID taskID, std::map<std::string, void*> data)
{
	_taskID = taskID;
	_datas = data;
}

/**
* \fn Task(e_TaskID taskID)
* \brief Constructor of a specific Task.
*
* \param taskID, use to get the type of the task.
* \return Task&
*/
Task::Task::Task(e_TaskID taskID)
{
	_taskID = taskID;
}

/**
* \fn ~Task()
* \brief Destructor of a specific Task
*/
Task::Task::~Task()
{
}

/**
* \fn Task::e_TaskID getTaskID() const
* \brief Getter returning the id of a specific task.
*
* \return Task::e_TaskID
*/
Task::e_TaskID Task::Task::getTaskID() const
{
	return _taskID;
}

/**
* \fn Task::e_TaskID getDatas(std::string) const
* \brief Getter returning the data of a specific task.
*
* \param key, use to get the right data associated to the given key.
* \return Task::e_TaskID
*/
void * Task::Task::getDatas(std::string key) const
{
	if (_datas.find(key) != _datas.end())
		return (_datas.find(key))->second;
	return NULL;
}

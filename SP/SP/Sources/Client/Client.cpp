/**
* \file Client.cpp
* \brief Client Class.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* Client class implementation.
*
*/

#include "Client/Client.hh"

/**
* \fn Client(boost::asio::io_service service, Task::ITaskManager* taskManager, Logger::ILogger* logger)
* \brief Constructor of a specific Client.
*
* \param service , use to get the service.
* \param taskManager, use to get the taskManager.
* \param logger, use to get the logger
* \return Client&
*/
Client::Client::Client(boost::asio::io_service& service, Task::ITaskManager* taskManager, Logger::ILogger* logger)
	: _socket(service), _logger(logger)
{
	static unsigned long id = 0;
	
	_isConnected = true;
	_id = id;
	_taskManager = taskManager;
	_isWebsite = false;
	++id;
}

/**
* \fn ~Client()
* \brief Destructor of a specific Client
*/
Client::Client::~Client()
{
}

/**
* \fn unsigned long getActualModule() const
* \brief Getter returning the actual module id of the current player module.
*
* \return unsigned long
*/
unsigned long Client::Client::getActualModule() const
{
	return _actualModule;
}

/**
* \fn void setActualModule(unsigned long)
* \brief Setter of the actual module id of the current player module.
*
* \param moduleID, use to set the module of the current player
* \return unsigned long
*/
void Client::Client::setActualModule(unsigned long moduleID)
{
	_actualModule = moduleID;
}

/**
* \fn unsigned long getID() const
* \brief Getter returning the id of a specific client.
*
* \return unsigned long
*/
unsigned long Client::Client::getID() const
{
	return _id;
}

/**
* \fn const boost::asio::ip::tcp::socket& getSocket() const
* \brief Getter returning the socket of a specific client.
*
* \return const boost::asio::ip::tcp::socket&
*/
const boost::asio::ip::tcp::socket& Client::Client::getSocket() const
{
	return _socket;
}

/**
* \fn const boost::asio::ip::tcp::socket& getSocket() const
* \brief Getter returning the socket of a specific client.
*
* \return const boost::asio::ip::tcp::socket&
*/
boost::asio::ip::tcp::socket& Client::Client::socket()
{
	return _socket;
}

/**
* \fn void doRead()
* \brief Function handling asynchronous input for a specific client
*
* \return void
*/
void Client::Client::doRead()
{
	boost::asio::async_read_until(_socket, _request, "TATA",
		boost::bind(&Client::Client::handle_read, this, boost::asio::placeholders::error, false));
}

/**
* \fn void doWrite()
* \brief Function handling asynchronous output for a specific client
*
* \return void
*/
void Client::Client::doWrite()
{
	boost::asio::async_write(_socket, _response,
		boost::bind(&Client::Client::handle_write, this, boost::asio::placeholders::error, 0));
}

/**
* \fn void handle_write(const boost::system::error_code & error, size_t bytesTransferred)
* \brief Function handling output for a specific client
*
* \param error, use to get the error if there is an error
* \param bytesTranseferred, use to get the number of bytes transferred
*
* \return void
*/
void Client::Client::handle_write(const boost::system::error_code & error, size_t bytesTransferred)
{
	if (error)
		_logger->newLog(new Logger::Log("Client " + std::to_string(_id) + ": \"" + error.message() + "\"", "Client", true));
}

/**
* \fn void handle_read(const boost::system::error_code & error, bool isRecusive)
* \brief Function handling input for a specific client
*
* \param error, use to get the error if there is an error
* \param isRecursive, use to know if this is the first call of this function
*
* \return void
*/
void Client::Client::handle_read(const boost::system::error_code & error, bool isRecursive)
{
	if (!error)
	{
		std::istream		response_stream(&_request);
		if (!isRecursive)
		{
			std::string			header;
			
			if (std::getline(response_stream, header))
				std::cout << header << std::endl;
			if (!Utils::Connection::checkWebServerHeader(header))
			{
				if (!Utils::Connection::checkGameServerHeader(header))
				{
					_logger->newLog(new Logger::Log("Connection refused due to unknow type of client", "Client", true));
					_isConnected = false;
					_socket.close();
					return;
				}
				_logger->newLog(new Logger::Log("New GameServer connected", "Client"));
			}
			else
			{
				_logger->newLog(new Logger::Log("New WebServer connected", "Client"));
				setWebsite(true);
			}
			addToResponseStream("100\r\n");
			doWrite();
			_isConnected = true;
		}
		if (_request.size())
		{
			std::string			clientRequest;
			while (std::getline(response_stream, clientRequest) && clientRequest != "\r\n")
			{
				_logger->newLog(new Logger::Log("Client " + std::to_string(_id) + " send a new request : \"" + clientRequest + "\"", "Client"));
				_taskManager->addTask(Task::TaskGenerator::generateTask(clientRequest, _id));
			}
		}
		boost::asio::async_read_until(_socket, _request, "\n",
			boost::bind(&Client::Client::handle_read, this, boost::asio::placeholders::error, true));
	}
	else if (error != boost::asio::error::eof)
	{
		_logger->newLog(new Logger::Log("Client " + std::to_string(_id) + ":\"" + error.message() + "\"", "Client", true));
	}
	else
	{
		_logger->newLog(new Logger::Log("Client " + std::to_string(_id) + " is disconnected", "Client", true));
	}
	disconnect();
}

/**
* \fn bool isConnected() const
* \brief Getter to know if a specific client is online
*
* \return bool
*/
bool Client::Client::isConnected() const
{
	return _isConnected;
}

/**
* \fn void addToResponseStream(const std::string response)
* \brief Function adding response to the client response stream
*
* \param response, use to append response to the reponse stream
* \return void
*/
void Client::Client::addToResponseStream(const std::string response)
{
	std::ostream request_stream(&_response);
	request_stream.write(response.c_str(), response.size());
	_logger->newLog(new Logger::Log("[SEND] Client " + std::to_string(_id) + ": \"" + response.c_str() + "\"", "Client", true));
}

/**
* \fn bool isWebsite() const
* \brief Function returning true if client is a website
*
* \return bool
*/
bool Client::Client::isWebsite() const
{
	return _isWebsite;
}

/**
* \fn void setWebsite(bool isWebsite)
* \brief Setter for isWebsite attribute
*
* \param isWebsite, used to set attribute
* \return void
*/
void Client::Client::setWebsite(bool isWebsite)
{
	_isWebsite = isWebsite;
}

/**
* \fn void disconnect()
* \brief Disconnect client from server
*
* \return void
*/
void Client::Client::disconnect()
{
	if (_isConnected)
	{
		_isConnected = false;
		_actualModule = 0;
		_socket.close();
	}
}

/**
* \file ClientManager.cpp
* \brief ClientManager Class.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* ClientManager class implementation.
*
*/

#include "Client/ClientManager.hh"

/**
* \fn ClientManager()
* \brief Constructor of a specific ClientManager.
*
* \return ClientManager&
*/
Client::ClientManager::ClientManager(Logger::ILogger* logger)
	: _logger(logger)
{
}

/**
* \fn ~ClientManager()
* \brief Destructor of a specific ClientManager
*/
Client::ClientManager::~ClientManager()
{
}

/**
* \fn void clean()
* \brief Function to clear client list.
*
* \return void
*/
void Client::ClientManager::clean()
{
	_clientList.clear();
}

/**
* \fn void addClient(IClient*)
* \brief Function to add a client to the client list.
*
* \param client, use to get the instance of the client.
* \return void
*/
void Client::ClientManager::addClient(Client::IClient*  client)
{
	_clientList.push_back(client);
	_logger->newLog(new Logger::Log("Adding new client to server", "ClientManager"));
}

/**
* \fn void addClient(unsigned long)
* \brief Function to remove a client to the client list.
*
* \param clientID, use to get the id of the client.
* \return void
*/
void Client::ClientManager::removeClient(unsigned long clientID)
{
	_clientList[clientID]->disconnect();
	_logger->newLog(new Logger::Log("Removing client from server", "ClientManager"));
}

/**
* \fn Client::IClient* getClient(unsigned long) const
* \brief Getter for a client.
*
* \param clientID, use to get the id of the client.
* \return Client::IClient*
*/
Client::IClient* Client::ClientManager::getClient(unsigned long clientID) const
{
	return _clientList[clientID];
}

/**
* \fn std::vector<Client::IClient*> getClientList() const
* \brief Getter for the client list.
*
* \return std::vector<Client::IClient*>
*/
std::vector<Client::IClient*> Client::ClientManager::getClientList() const
{
	return _clientList;
}

/**
* \fn unsigned long getClientListSize() const
* \brief Getter for the size of the client list.
*
* \return unsigned long
*/
unsigned long Client::ClientManager::getClientListSize() const
{
	return _clientList.size();
}

/**
* \file Log.hh
* \brief Log header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* Log class header.
*
*/

#pragma once

#include		"Logger/ILog.hh"

/**
* \namespace Logger
* \brief Logger implementation
*
* Namespace for Logger and Logger Management
*/
namespace Logger
{
	/**
	* \class Log
	* \brief Class header for Log
	*
	* Class for Log creation
	*/
	class Log
		: public ILog
	{
	public:
		/**
		* \fn Log(const std::string, const std::string, bool)
		* \brief Constructor of a specific Log.
		*
		* \param log, use to get the value of the log.
		* \param logProvenance, use to get the class who create the log.
		* \param isError, use to set if the log is an error.
		* \return Log&
		*/
		Log(const std::string log, const std::string logProvenance, const bool isError = false);

		/**
		* \fn ~Log()
		* \brief Destructor of a specific Log.
		*/
		virtual ~Log();

		/**
		* \fn std::string getLog() const
		* \brief Getter returning the value of a specific log.
		*
		* \return std::string
		*/
		virtual std::string getLog() const override;
		/**
		* \fn std::string getLogProvenance() const
		* \brief Getter returning the class who create the log for a specific log.
		*
		* \return std::string
		*/
		virtual std::string getLogProvenance() const override;

		/**
		* \fn bool isError() const
		* \brief Getter to know if the log is an error.
		*
		* \return bool
		*/
		virtual bool isError() const override;

	private:
		const std::string	_log;
		const std::string	_logProvenance;
		const bool			_isError;
	};
}
/**
* \file Logger.hh
* \brief Logger header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* Logger class header.
*
*/

#pragma once

#include	"Logger/ILogger.hh"
#include	"Logger/ILog.hh"

/**
* \namespace Logger
* \brief Logger implementation
*
* Namespace for Logger and Logger Management
*/
namespace Logger
{
	/**
	* \class Logger
	* \brief Class header for Logger
	*
	* Class for Logger creation
	*/
	class Logger
		: public ILogger
	{
	public:
		/**
		* \fn Logger(const unsigned int)
		* \brief Constructor of a specific Logger.
		*
		* \param historicSize, use to set the size of the historic.
		* \return Logger&
		*/
		Logger(const unsigned int historicSize = 1);

		/**
		* \fn ~Logger()
		* \brief Destructor of a specific Logger
		*/
		virtual ~Logger();

		/**
		* \fn void newLog(ILog*)
		* \brief Function for adding new log to the logger.
		*
		* \param log, use to get the value of the new log.
		* \return void
		*/
		virtual void newLog(ILog * log) override;

		/**
		* \fn std::list<Logger::ILog*> getLastLog() const
		* \brief Getter returning the log history of a specific logger.
		*
		* \return std::list<Logger::ILog*>
		*/
		virtual std::list<ILog*> getLastLog() const override;

	private:

		std::list<ILog*>		_lastLog;
		const unsigned int		_historicLogSize;
	};
}
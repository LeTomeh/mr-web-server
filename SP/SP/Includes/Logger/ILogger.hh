/**
* \file ILogger.hh
* \brief ILogger header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* ILogger class header.
*
*/

#pragma once

#include			<Logger/ILog.hh>

#include			<list>

/**
* \namespace Logger
* \brief Logger implementation
*
* Namespace for Logger and Logger Management
*/
namespace Logger
{
	/**
	* \class ILogger
	* \brief Class header for ILogger
	*
	* Class for ILogger creation
	*/
	class ILogger {
	public:
		/**
		* \fn ~ILogger()
		* \brief Destructor of a specific ILogger
		*/
		virtual ~ILogger() {};

		/**
		* \fn void newLog(ILog*)
		* \brief Function for adding new log to the logger.
		*
		* \param log, use to get the value of the new log.
		* \return void
		*/
		virtual void newLog(ILog* log) = 0;

		/**
		* \fn std::list<Logger::ILog*> getLastLog() const
		* \brief Getter returning the log history of a specific logger.
		*
		* \return std::list<Logger::ILog*>
		*/
		virtual std::list<ILog*> getLastLog() const = 0;
	};
}
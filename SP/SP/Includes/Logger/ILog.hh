/**
* \file ILog.hh
* \brief ILog header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* ILog class header.
*
*/

#pragma once

#include		<string>

/**
* \namespace Logger
* \brief Logger implementation
*
* Namespace for Logger and Logger Management
*/
namespace Logger
{
	/**
	* \class ILog
	* \brief Class header for ILog
	*
	* Class for ILog creation
	*/
	class ILog {
	public:
		/**
		* \fn ~ILog()
		* \brief Destructor of a specific ILog.
		*/
		virtual ~ILog() {};

		/**
		* \fn std::string getLog() const
		* \brief Getter returning the value of a specific log.
		*
		* \return std::string
		*/
		virtual std::string getLog() const = 0;

		/**
		* \fn std::string getLogProvenance() const
		* \brief Getter returning the class who create the log for a specific log.
		*
		* \return std::string
		*/
		virtual std::string getLogProvenance() const = 0;

		/**
		* \fn bool isError() const
		* \brief Getter to know if the log is an error.
		*
		* \return bool
		*/
		virtual bool isError() const = 0;
	};
}
/**
* \file ClientManager.hh
* \brief ClientManager header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* ClientManager class header.
*
*/

#include			"Client/IClientManager.hh"
#include			"boost/bind.hpp"
#include			"Logger/ILogger.hh"
#include			"Logger/Log.hh"

/**
* \namespace Client
* \brief Client implementation
*
* Namespace for Client and Client Management
*/
namespace Client
{
	/**
	* \class ClientManager
	* \brief Class header for ClientManager
	*
	* Class for ClientManager creation
	*/
	class ClientManager
		: public IClientManager
	{
	public:
		/**
		* \fn ClientManager(Logger::ILogger* logger)
		* \brief Constructor of a specific ClientManager.
		*
		* \param logger, used to get access to the logger
		* \return ClientManager&
		*/
		ClientManager(Logger::ILogger* logger);

		/**
		* \fn ~ClientManager()
		* \brief Destructor of a specific ClientManager
		*/
		virtual ~ClientManager();

		/**
		* \fn void clean()
		* \brief Function to clear client list.
		*
		* \return void
		*/
		virtual void clean() override;

		/**
		* \fn void addClient(IClient*)
		* \brief Function to add a client to the client list.
		*
		* \param client, use to get the instance of the client.
		* \return void
		*/
		virtual void addClient(Client::IClient* client) override;

		/**
		* \fn void addClient(unsigned long)
		* \brief Function to remove a client to the client list.
		*
		* \param clientID, use to get the id of the client.
		* \return void
		*/
		virtual void removeClient(unsigned long clientID) override;

		/**
		* \fn Client::IClient* getClient(unsigned long) const
		* \brief Getter for a client.
		*
		* \param clientID, use to get the id of the client.
		* \return Client::IClient*
		*/
		virtual Client::IClient* getClient(unsigned long clientID) const override;

		/**
		* \fn std::vector<Client::IClient*> getClientList() const
		* \brief Getter for the client list.
		*
		* \return std::vector<Client::IClient*>
		*/
		virtual std::vector<Client::IClient*> getClientList() const override;

		/**
		* \fn unsigned long getClientListSize() const
		* \brief Getter for the size of the client list.
		*
		* \return unsigned long
		*/
		virtual unsigned long getClientListSize() const override;

	private:

		Logger::ILogger*					_logger;
		std::vector<Client::IClient*>		_clientList;
	};
}
/**
* \file IClient.hh
* \brief IClient header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* IClient class header.
*
*/

#pragma once

#include	<boost\asio.hpp>
#include	<boost/enable_shared_from_this.hpp>

/**
* \namespace Client
* \brief Client implementation
*
* Namespace for Client and Client Management
*/
namespace Client
{
	/**
	* \class IClient
	* \brief Class header for IClient
	*
	* Class for IClient creation
	*/
	class IClient
	{
	public:
		/**
		* \fn ~IClient()
		* \brief Destructor of a specific IClient
		*/
		virtual ~IClient() {};

		/**
		* \fn unsigned long getActualModule() const
		* \brief Getter returning the actual module id of the current player module.
		*
		* \return unsigned long
		*/
		virtual unsigned long getActualModule() const = 0;

		/**
		* \fn void setActualModule(unsigned long)
		* \brief Setter of the actual module id of the current player module.
		*
		* \param moduleID, use to set the module of the current player
		* \return unsigned long
		*/
		virtual void			setActualModule(unsigned long moduleID) = 0;

		/**
		* \fn unsigned long getID() const
		* \brief Getter returning the id of a specific client.
		*
		* \return unsigned long
		*/
		virtual unsigned long	getID() const = 0;

		/**
		* \fn const boost::asio::ip::tcp::socket& getSocket() const
		* \brief Getter returning the socket of a specific client.
		*
		* \return const boost::asio::ip::tcp::socket&
		*/
		virtual const boost::asio::ip::tcp::socket& getSocket() const = 0;

		/**
		* \fn  boost::asio::ip::tcp::socket& socket() const
		* \brief Getter returning the socket of a specific client.
		*
		* \return  boost::asio::ip::tcp::socket&
		*/
		virtual boost::asio::ip::tcp::socket& socket() = 0;

		/**
		* \fn void doWrite()
		* \brief Function handling asynchronous output for a specific client
		*
		* \return void
		*/
		virtual void			doWrite() = 0;

		/**
		* \fn void doRead()
		* \brief Function handling asynchronous input for a specific client
		*
		* \return void
		*/
		virtual void			doRead() = 0;

		/**
		* \fn bool isConnected() const
		* \brief Getter to know if a specific client is online
		*
		* \return bool
		*/
		virtual bool			isConnected() const = 0;

		/**
		 * \fn void addToResponseStream(const std::string response)
		 * \brief Function adding response to the client response stream
		 *
		 * \param response, use to append response to the reponse stream
		 * \return void
		 */
		virtual void addToResponseStream(const std::string response) = 0;

		/**
		* \fn bool isWebsite() const
		* \brief Function returning true if client is a website
		*
		* \return bool
		*/
		virtual bool isWebsite() const = 0;

		/**
		* \fn void setWebsite(bool isWebsite)
		* \brief Setter for isWebsite attribute
		*
		* \param isWebsite, used to set attribute
		* \return void
		*/
		virtual void setWebsite(bool isWebsite) = 0;

		/**
		* \fn void disconnect()
		* \brief Disconnect client from server
		*
		* \return void
		*/
		virtual void disconnect() = 0;
	};
}
/**
* \file IClientManager.hh
* \brief IClientManager header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* IClientManager class header.
*
*/

#pragma once

#include		<vector>

#include		"Client/IClient.hh"

/**
* \namespace Client
* \brief Client implementation
*
* Namespace for Client and Client Management
*/
namespace Client
{
	/**
	* \class IClientManager
	* \brief Class header for IClientManager
	*
	* Class for IClientManager creation
	*/
	class IClientManager {
	public:
		/**
		* \fn ~IClientManager()
		* \brief Destructor of a specific IClientManager
		*/
		virtual ~IClientManager() { };

		/**
		* \fn void clean()
		* \brief Function to clear client list.
		*
		* \return void
		*/
		virtual void clean() = 0;

		/**
		* \fn void addClient(IClient*)
		* \brief Function to add a client to the client list.
		*
		* \param client, use to get the instance of the client.
		* \return void
		*/
		virtual void addClient(Client::IClient* client) = 0;

		/**
		* \fn void addClient(unsigned long)
		* \brief Function to remove a client to the client list.
		*
		* \param clientID, use to get the id of the client.
		* \return void
		*/
		virtual void removeClient(unsigned long cientID) = 0;

		/**
		* \fn Client::IClient* getClient(unsigned long) const
		* \brief Getter for a client.
		*
		* \param clientID, use to get the id of the client.
		* \return Client::IClient*
		*/
		virtual Client::IClient* getClient(unsigned long clientID) const = 0;

		/**
		* \fn std::vector<Client::IClient*> getClientList() const
		* \brief Getter for the client list.
		*
		* \return std::vector<Client::IClient*>
		*/
		virtual	std::vector<Client::IClient*> getClientList() const = 0;

		/**
		* \fn unsigned long getClientListSize() const
		* \brief Getter for the size of the client list.
		*
		* \return unsigned long
		*/
		virtual unsigned long getClientListSize() const = 0;
	};
}
/**
* \file Client.hh
* \brief Client header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* Client class header.
*
*/

#pragma once

#include "Client/IClient.hh"

#include "Utils/Connection.hh"
#include "Utils/TaskGenerator.hh"

#include "Task/ITaskManager.hh"

#include "Logger/ILogger.hh"
#include "Logger/Log.hh"

#include <boost\asio.hpp>
#include <boost/bind.hpp>
#include <iostream>

/**
* \namespace Client
* \brief Client implementation
*
* Namespace for Client and Client Management
*/
namespace Client
{
	/**
	* \class Client
	* \brief Class header for Client
	*
	* Class for Client creation
	*/
	class Client
		: public IClient
	{
	public:
		
		/**
		* \fn Client(boost::asio::io_service service, Task::ITaskManager* taskManager, Logger::ILogger* logger)
		* \brief Constructor of a specific Client.
		*
		* \param service , use to get the service.
		* \param taskManager, use to get the taskManager.
		* \param logger, use to get the logger
		* \return Client&
		*/
		Client(boost::asio::io_service& service, Task::ITaskManager* taskManager, Logger::ILogger* logger);

		/**
		* \fn ~Client()
		* \brief Destructor of a specific Client
		*/
		virtual ~Client();

		/**
		* \fn unsigned long getActualModule() const
		* \brief Getter returning the actual module id of the current player module.
		*
		* \return unsigned long
		*/
		virtual unsigned long getActualModule() const override;

		/**
		* \fn void setActualModule(unsigned long)
		* \brief Setter of the actual module id of the current player module.
		*
		* \param moduleID, use to set the module of the current player
		* \return unsigned long
		*/
		virtual void setActualModule(unsigned long moduleID) override;

		/**
		* \fn unsigned long getID() const
		* \brief Getter returning the id of a specific client.
		*
		* \return unsigned long
		*/
		virtual unsigned long getID() const override;

		/**
		* \fn const boost::asio::ip::tcp::socket& getSocket() const
		* \brief Getter returning the socket of a specific client.
		*
		* \return const boost::asio::ip::tcp::socket&
		*/
		virtual const boost::asio::ip::tcp::socket& getSocket() const override;

		/**
		* \fn  boost::asio::ip::tcp::socket& getSocket()
		* \brief Getter returning the socket of a specific client.
		*
		* \return boost::asio::ip::tcp::socket&
		*/
		virtual boost::asio::ip::tcp::socket& socket() override;

		/**
		* \fn void doRead()
		* \brief Function handling asynchronous input for a specific client
		*
		* \return void
		*/
		virtual void doRead() override;

		/**
		* \fn void doWrite()
		* \brief Function handling asynchronous output for a specific client
		*
		* \return void
		*/
		virtual void doWrite() override;

		/**
		* \fn bool isConnected() const
		* \brief Getter to know if a specific client is online
		*
		* \return bool
		*/
		virtual bool isConnected() const override;

		/**
		* \fn void addToResponseStream(const std::string response)
		* \brief Function adding response to the client response stream
		*
		* \param response, use to append response to the reponse stream
		* \return void
		*/
		virtual void addToResponseStream(const std::string response) override;


		/**
		* \fn bool isWebsite() const
		* \brief Function returning true if client is a website
		*
		* \return bool
		*/
		virtual bool isWebsite() const override;


		/**
		* \fn void setWebsite(bool isWebsite)
		* \brief Setter for isWebsite attribute
		*
		* \param isWebsite, used to set attribute
		* \return void
		*/
		virtual void setWebsite(bool isWebsite) override;

		/**
		* \fn void disconnect()
		* \brief Disconnect client from server
		*
		* \return void
		*/
		virtual void disconnect() override;

	private:

		/**
		* \fn void handle_write(const boost::system::error_code & error, size_t bytesTransferred)
		* \brief Function handling output for a specific client
		*
		* \param error, use to get the error if there is an error
		* \param bytesTranseferred, use to get the number of bytes transferred
		*
		* \return void
		*/
		void handle_write(const boost::system::error_code& error, size_t bytesTransferred);

		/**
		* \fn void handle_read(const boost::system::error_code & error, bool isRecusive)
		* \brief Function handling input for a specific client
		*
		* \param error, use to get the error if there is an error
		* \param isRecursive, use to know if this is the first call of this function
		*
		* \return void
		*/
		void handle_read(const boost::system::error_code& error, bool isRecursive);

		boost::asio::ip::tcp::socket		_socket;
		boost::asio::streambuf				_response;
		boost::asio::streambuf				_request;

		unsigned long						_id;
		unsigned long						_actualModule;
		bool								_isWebsite;
		bool								_isConnected;

		Task::ITaskManager*					_taskManager;
		Logger::ILogger*					_logger;
	};
}
/**
* \file MRinsideError.hh
* \brief MRinsideError header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* MRinsideError class header.
*
*/

#pragma once

#include						<exception>

/**
* \class MRinsideError
* \brief Class header for MRinsideError
*
* Class for MRinsideError error
*/
class							MRinsideError : public std::exception
{
public:
	/**
	* \fn ~MRinsideError()
	* \brief Destructor of a specific MRinsideError
	*/
	virtual ~MRinsideError() {};

	/**
	* \fn const char* what() const
	* \brief Getter returning message of a specific error.
	*
	* \return const char*
	*/
	virtual const char*			what() const = 0;
};
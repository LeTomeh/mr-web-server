/**
* \file ListenError.hh
* \brief ListenError header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* ListenError class header.
*
*/

#pragma once

#include			"Errors/Communication/CommunicationError.hh"

#include			<string>

/**
* \namespace Communication
* \brief Communication implementation
*
* Namespace for Communication and Communication Management
*/
namespace Communication
{
	/**
	* \class ListenError
	* \brief Class header for ListenError
	*
	* Class for ListenError error
	*/
	class				ListenError : public CommunicationError
	{
	private:
		const std::string&		_message;

	public:
		/**
		* \fn ListenError(const std::string& message)
		* \brief Constructor of a specific ListenError.
		*
		* \param message, use to get the message of the error.
		* \return ListenError&
		*/
		ListenError(const std::string& message);
	
		/**
		* \fn ~ListenError()
		* \brief Destructor of a specific ListenError
		*/
		virtual ~ListenError();

		/**
		* \fn const char* what() const
		* \brief Getter returning message of a specific error.
		*
		* \return const char*
		*/
		const char*		what() const;
	};
}
/**
* \file BindError.hh
* \brief BindError header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* BindError class header.
*
*/

#pragma once

#include			"Errors/Communication/CommunicationError.hh"

#include			<string>

/**
* \namespace Communication
* \brief Communication implementation
*
* Namespace for Communication and Communication Management
*/
namespace Communication
{
	/**
	* \class BindError
	* \brief Class header for BindError
	*
	* Class for BindError error
	*/
	class				BindError : public CommunicationError
	{
	private:
		const std::string&		_message;

	public:
		/**
		* \fn BindError(const std::string& message)
		* \brief Constructor of a specific BindError.
		*
		* \param message, use to get the message of the error.
		* \return BindError&
		*/
		BindError(const std::string& message);
		/**
		* \fn ~BindError()
		* \brief Destructor of a specific BindError
		*/
		virtual ~BindError();

		/**
		* \fn const char* what() const
		* \brief Getter returning message of a specific error.
		*
		* \return const char*
		*/
		const char*		what() const;
	};
}
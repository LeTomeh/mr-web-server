/**
* \file CommunicationError.hh
* \brief CommunicationError header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CommunicationError class header.
*
*/

#pragma once

#include		"Errors/MRinsideError.hh"

/**
* \namespace Communication
* \brief Communication implementation
*
* Namespace for Communication and Communication Management
*/
namespace Communication
{
	/**
	* \class CommunicationError
	* \brief Class header for CommunicationError
	*
	* Class for CommunicationError error
	*/
	class			CommunicationError : public MRinsideError
	{
	public:
		/**
		* \fn ~CommunicationError()
		* \brief Destructor of a specific error
		*/
		virtual ~CommunicationError() {};

		/**
		* \fn const char* what() const
		* \brief Getter returning message of a specific error.
		*
		* \return const char*
		*/
		virtual const char*			what() const = 0;
	};
}
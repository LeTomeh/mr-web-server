/**
* \file CantCreateWorker.hh
* \brief CantCreateWorker header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CantCreateWorker class header.
*
*/

#pragma once

#include			"Errors/Task/TaskError.hh"

#include			<string>

/**
* \namespace Task
* \brief Task implementation
*
* Namespace for Task and Task Management
*/
namespace Task
{
	/**
	* \class CantCreateWorker
	* \brief Class header for CantCreateWorker
	*
	* Class for CantCreateWorker error
	*/
	class				CantCreateWorker : public TaskError
	{
	private:
		const std::string&		_message;

	public:
		/**
		* \fn CantCreateWorker(const std::string& message)
		* \brief Constructor of a specific CantCreateWorker.
		*
		* \param message, use to get the message of the error.
		* \return CantCreateWorker&
		*/
		CantCreateWorker(const std::string& message);

		/**
		* \fn ~CantCreateWorker()
		* \brief Destructor of a specific CantCreateWorker
		*/
		virtual ~CantCreateWorker();

		/**
		* \fn const char* what() const
		* \brief Getter returning message of a specific error.
		*
		* \return const char*
		*/
		const char*		what() const;
	};
}
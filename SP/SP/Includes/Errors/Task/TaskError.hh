/**
* \file TaskError.hh
* \brief TaskError header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* TaskError class header.
*
*/

#pragma once

#include		"Errors/MRinsideError.hh"

/**
* \namespace Task
* \brief Module implementation
*
* Namespace for Task and Task Management
*/
namespace Task
{
	/**
	* \class TaskError
	* \brief Class header for TaskError
	*
	* Class for TaskError error
	*/
	class			TaskError : public MRinsideError
	{
	public:
		/**
		* \fn ~TaskError()
		* \brief Destructor of a specific error
		*/
		virtual ~TaskError() {};

		/**
		* \fn const char* what() const
		* \brief Getter returning message of a specific error.
		*
		* \return const char*
		*/
		virtual const char*			what() const = 0;
	};
}
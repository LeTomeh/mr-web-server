/**
* \file CantLoadModule.hh
* \brief CantLoadModule header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CantLoadModule class header.
*
*/

#pragma once

#include			"Errors/Module/ModuleError.hh"

#include			<string>

/**
* \namespace Module
* \brief Module implementation
*
* Namespace for Module and Module Management
*/
namespace Module
{
	/**
	* \class CantLoadModule
	* \brief Class header for CantLoadModule
	*
	* Class for CantLoadModule error
	*/
	class				CantLoadModule : public ModuleError
	{
	private:
		const std::string&		_message;

	public:
		/**
		* \fn CantLoadModule(const std::string& message)
		* \brief Constructor of a specific CantLoadModule.
		*
		* \param message, use to get the message of the error.
		* \return CantLoadModule&
		*/
		CantLoadModule(const std::string& message);

		/**
		* \fn ~CantLoadModule()
		* \brief Destructor of a specific CantLoadModule
		*/
		virtual ~CantLoadModule();

		/**
		* \fn const char* what() const
		* \brief Getter returning message of a specific error.
		*
		* \return const char*
		*/
		const char*		what() const;
	};
}
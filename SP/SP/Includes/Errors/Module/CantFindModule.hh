/**
* \file CantFindModule.hh
* \brief CantFindModule header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CantFindModule class header.
*
*/

#pragma once

#include			"Errors/Module/ModuleError.hh"

#include			<string>

/**
* \namespace Module
* \brief Module implementation
*
* Namespace for Module and Module Management
*/
namespace Module
{
	/**
	* \class CantFindModule
	* \brief Class header for CantFindModule
	*
	* Class for CantFindModule error
	*/
	class				CantFindModule : public ModuleError
	{
	private:
		const std::string&		_message;

	public:
		/**
		* \fn CantFindModule(const std::string& message)
		* \brief Constructor of a specific CantFindModule.
		*
		* \param message, use to get the message of the error.
		* \return CantFindModule&
		*/
		CantFindModule(const std::string& message);

		/**
		* \fn ~CantFindModule()
		* \brief Destructor of a specific CantFindModule
		*/
		virtual ~CantFindModule();

		/**
		* \fn const char* what() const
		* \brief Getter returning message of a specific error.
		*
		* \return const char*
		*/
		const char*		what() const;
	};
}
/**
* \file CantStopModule.hh
* \brief CantStopModule header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CantStopModule class header.
*
*/

#pragma once

#include			"Errors/Module/ModuleError.hh"

#include			<string>

/**
* \namespace Module
* \brief Module implementation
*
* Namespace for Module and Module Management
*/
namespace Module
{
	/**
	* \class CantStopModule
	* \brief Class header for CantStopModule
	*
	* Class for CantStopModule error
	*/
	class				CantStopModule : public ModuleError
	{
	private:
		const std::string&		_message;

	public:
		/**
		* \fn CantStopModule(const std::string& message)
		* \brief Constructor of a specific CantStopModule.
		*
		* \param message, use to get the message of the error.
		* \return CantStopModule&
		*/
		CantStopModule(const std::string& message);

		/**
		* \fn ~CantStopModule()
		* \brief Destructor of a specific CantStopModule
		*/
		virtual ~CantStopModule();

		/**
		* \fn const char* what() const
		* \brief Getter returning message of a specific error.
		*
		* \return const char*
		*/
		const char*		what() const;
	};
}
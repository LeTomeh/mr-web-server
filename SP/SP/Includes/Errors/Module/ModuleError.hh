/**
* \file ModuleError.hh
* \brief ModuleError header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* ModuleError class header.
*
*/

#pragma once

#include		"Errors/MRinsideError.hh"

/**
* \namespace Module
* \brief Module implementation
*
* Namespace for Module and Module Management
*/
namespace Module
{
	/**
	* \class ModuleError
	* \brief Class header for ModuleError
	*
	* Class for ModuleError error
	*/
	class			ModuleError : public MRinsideError
	{
	public:
		/**
		* \fn ~ModuleError()
		* \brief Destructor of a specific 
		*/
		virtual ~ModuleError() {};

		/**
		* \fn const char* what() const
		* \brief Getter returning message of a specific error.
		*
		* \return const char*
		*/
		virtual const char*			what() const = 0;
	};
}
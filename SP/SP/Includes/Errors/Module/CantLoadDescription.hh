/**
* \file CantLoadDescription.hh
* \brief CantLoadDescription header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CantLoadDescription class header.
*
*/

#pragma once

#include			"Errors/Module/ModuleError.hh"

#include			<string>

/**
* \namespace Module
* \brief Module implementation
*
* Namespace for Module and Module Management
*/
namespace Module
{
	/**
	* \class CantLoadDescription
	* \brief Class header for CantLoadDescription
	*
	* Class for CantLoadDescription error
	*/
	class				CantLoadDescription : public ModuleError
	{
	private:
		const std::string&		_message;

	public:
		/**
		* \fn CantLoadDescription(const std::string& message)
		* \brief Constructor of a specific CantLoadDescription.
		*
		* \param message, use to get the message of the error.
		* \return CantLoadDescription&
		*/
		CantLoadDescription(const std::string& message);
		/**
		* \fn ~CantLoadDescription()
		* \brief Destructor of a specific CantLoadDescription
		*/
		virtual ~CantLoadDescription();

		/**
		* \fn const char* what() const
		* \brief Getter returning message of a specific error.
		*
		* \return const char*
		*/
		const char*		what() const;
	};
}
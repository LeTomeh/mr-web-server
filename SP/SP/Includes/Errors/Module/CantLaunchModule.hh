/**
* \file CantLaunchModule.hh
* \brief CantLaunchModule header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CantLaunchModule class header.
*
*/

#pragma once

#include			"Errors/Module/ModuleError.hh"

#include			<string>

/**
* \namespace Module
* \brief Module implementation
*
* Namespace for Module and Module Management
*/
namespace Module
{
	/**
	* \class CantLaunchModule
	* \brief Class header for CantLaunchModule
	*
	* Class for CantLaunchModule error
	*/
	class				CantLaunchModule : public ModuleError
	{
	private:
		const std::string&		_message;

	public:
		/**
		* \fn CantLaunchModule(const std::string& message)
		* \brief Constructor of a specific CantLaunchModule.
		*
		* \param message, use to get the message of the error.
		* \return CantLaunchModule&
		*/
		CantLaunchModule(const std::string& message);

		/**
		* \fn ~CantLaunchModule()
		* \brief Destructor of a specific CantLaunchModule
		*/
		virtual ~CantLaunchModule();

		/**
		* \fn const char* what() const
		* \brief Getter returning message of a specific error.
		*
		* \return const char*
		*/
		const char*		what() const;
	};
}
/**
* \file CantAddClientToModule.hh
* \brief CantAddClientToModule header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CantAddClientToModule class header.
*
*/

#pragma once

#include			"Errors/Module/ModuleError.hh"

#include			<string>

/**
* \namespace Module
* \brief Module implementation
*
* Namespace for Module and Module Management
*/
namespace Module
{
	/**
	* \class CantAddClientToModule
	* \brief Class header for CantAddClientToModule
	*
	* Class for CantAddClientToModule error
	*/
	class				CantAddClientToModule : public ModuleError
	{
	private:
		const std::string&		_message;

	public:
		/**
		* \fn CantAddClientToModule(const std::string& message)
		* \brief Constructor of a specific CantAddClientToModule.
		*
		* \param message, use to get the message of the error.
		* \return CantAddClientToModule&
		*/
		CantAddClientToModule(const std::string& message);

		/**
		* \fn ~CantAddClientToModule()
		* \brief Destructor of a specific CantAddClientToModule
		*/
		virtual ~CantAddClientToModule();

		/**
		* \fn const char* what() const
		* \brief Getter returning message of a specific error.
		*
		* \return const char*
		*/
		const char*		what() const;
	};
}
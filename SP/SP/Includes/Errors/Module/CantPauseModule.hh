/**
* \file CantPauseModule.hh
* \brief CantPauseModule header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CantPauseModule class header.
*
*/

#pragma once

#include			"Errors/Module/ModuleError.hh"

#include			<string>

/**
* \namespace Module
* \brief Module implementation
*
* Namespace for Module and Module Management
*/
namespace Module
{
	/**
	* \class CantPauseModule
	* \brief Class header for CantPauseModule
	*
	* Class for CantPauseModule error
	*/
	class				CantPauseModule : public ModuleError
	{
	private:
		const std::string&		_message;

	public:
		/**
		* \fn CantPauseModule(const std::string& message)
		* \brief Constructor of a specific CantPauseModule.
		*
		* \param message, use to get the message of the error.
		* \return CantPauseModule&
		*/
		CantPauseModule(const std::string& message);

		/**
		* \fn ~CantPauseModule()
		* \brief Destructor of a specific CantPauseModule
		*/
		virtual ~CantPauseModule();

		/**
		* \fn const char* what() const
		* \brief Getter returning message of a specific error.
		*
		* \return const char*
		*/
		const char*		what() const;
	};
}
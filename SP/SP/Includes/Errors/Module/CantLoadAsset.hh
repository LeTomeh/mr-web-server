/**
* \file CantLoadAsset.hh
* \brief CantLoadAsset header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CantLoadAsset class header.
*
*/

#pragma once

#include			"Errors/Module/ModuleError.hh"

#include			<string>

/**
* \namespace Module
* \brief Module implementation
*
* Namespace for Module and Module Management
*/
namespace Module
{
	/**
	* \class CantLoadAsset
	* \brief Class header for CantLoadAsset
	*
	* Class for CantLoadAsset error
	*/
	class				CantLoadAsset : public ModuleError
	{
	private:
		const std::string&		_message;

	public:
		/**
		* \fn CantLoadAsset(const std::string& message)
		* \brief Constructor of a specific CantLoadAsset.
		*
		* \param message, use to get the message of the error.
		* \return CantLoadAsset&
		*/
		CantLoadAsset(const std::string& message);

		/**
		* \fn ~CantLoadAsset()
		* \brief Destructor of a specific CantLoadAsset
		*/
		virtual ~CantLoadAsset();

		/**
		* \fn const char* what() const
		* \brief Getter returning message of a specific error.
		*
		* \return const char*
		*/
		const char*		what() const;
	};
}
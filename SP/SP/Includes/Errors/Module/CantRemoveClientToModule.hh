/**
* \file CantRemoveClientToModule.hh
* \brief CantRemoveClientToModule header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CantRemoveClientToModule class header.
*
*/

#pragma once

#include			"Errors/Module/ModuleError.hh"

#include			<string>

/**
* \namespace Module
* \brief Module implementation
*
* Namespace for Module and Module Management
*/
namespace Module
{
	/**
	* \class CantRemoveClientToModule
	* \brief Class header for CantRemoveClientToModule
	*
	* Class for CantRemoveClientToModule error
	*/
	class				CantRemoveClientToModule : public ModuleError
	{
	private:
		const std::string&		_message;

	public:
		/**
		* \fn CantRemoveClientToModule(const std::string& message)
		* \brief Constructor of a specific CantRemoveClientToModule.
		*
		* \param message, use to get the message of the error.
		* \return CantRemoveClientToModule&
		*/
		CantRemoveClientToModule(const std::string& message);

		/**
		* \fn ~CantRemoveClientToModule()
		* \brief Destructor of a specific CaCantRemoveClientToModulentLaunchModule
		*/
		virtual ~CantRemoveClientToModule();

		/**
		* \fn const char* what() const
		* \brief Getter returning message of a specific error.
		*
		* \return const char*
		*/
		const char*		what() const;
	};
}
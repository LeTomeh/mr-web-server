/**
* \file IServer.hh
* \brief IServer header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* IServer class header.
*
*/

#pragma once

namespace Core
{
	class IServer;
}
#include			"Module/IModuleManager.hh"
#include			"Client/IClientManager.hh"
#include			"Communication/ICommunicationManager.hh"
#include			"Logger/ILogger.hh"
/**
* \namespace Core
* \brief Core implementation
*
* Namespace for Core
*/
namespace Core
{
	/**
	* \class IServer
	* \brief Class header for IServer
	*
	* Class for IServer creation
	*/
	class				IServer {
	public:
		/**
		* \fn ~IServer()
		* \brief Destructor of a specific IServer
		*/
		virtual ~IServer() {};

		/**
		* \fn void run()
		* \brief Function for handling the main loop of the server.
		*
		* \return void
		*/
		virtual void	run() = 0;

		/**
		* \fn void close()
		* \brief Function for handling the shutdown of the server.
		*
		* \return void
		*/
		virtual void	close() = 0;

		/**
		* \fn void init(short)
		* \brief Function for handling the initialisation of the server.
		*
		* \param port, use to setup the communication of the server.
		* \return void
		*/
		virtual void	init(short port = 4242) = 0;

		/**
		* \fn Module::IModuleManager* getModuleManager() const
		* \brief Getter for the moduleManager of the server.
		*
		* \return Module::IModuleManager*
		*/
		virtual Module::IModuleManager* getModuleManager() const = 0;

		/**
		* \fn Client::IClientManager* getClientManager() const
		* \brief Getter for the clientManager of the server.
		*
		* \return Client::IClientManager*
		*/
		virtual Client::IClientManager* getClientManager() const = 0;

		/**
		* \fn Communication::ICommunicationManager* getCommunicationManager() const
		* \brief Getter for the communicationManager of the server.
		*
		* \return Communication::ICommunicationManager*
		*/
		virtual Communication::ICommunicationManager* getCommunicationManager() const = 0;

		/**
		* \fn Logger::ILogger* getLogger() const
		* \brief Getter for the logger of the server.
		*
		* \return Logger::ILogger*
		*/
		virtual Logger::ILogger * getLogger() const = 0;
	};
}
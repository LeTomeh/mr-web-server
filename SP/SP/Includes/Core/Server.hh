/**
* \file Server.hh
* \brief Server header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* Server class header.
*
*/

#pragma once

#include "Core/IServer.hh"

#include "Errors/Errors.hh"

#include "Logger/Logger.hh"
#include "Logger/Log.hh"

#include "Client/ClientManager.hh"

#include "Module/ModuleManager.hh"

#include "Communication/CommunicationManager.hh"

#include "Task/TaskManager.hh"

/**
* \namespace Core
* \brief Core implementation
*
* Namespace for Core
*/
namespace Core
{
	/**
	* \class Server
	* \brief Class header for Server
	*
	* Class for Server creation
	*/
	class			Server
		: public IServer {

	public:
		/**
		* \fn Server(short)
		* \brief Constructor of a specific Server.
		*
		* \param port, use to setup the communication.
		* \return Server&
		*/
		Server(short port = 4242);

		/**
		* \fn ~Server()
		* \brief Destructor of a specific Server
		*/
		virtual ~Server();

		/**
		* \fn void run()
		* \brief Function for handling the main loop of the server.
		*
		* \return void
		*/
		virtual void run() override;

		/**
		* \fn void close()
		* \brief Function for handling the shutdown of the server.
		*
		* \return void
		*/
		virtual void close() override;

		/**
		* \fn void init(short)
		* \brief Function for handling the initialisation of the server.
		*
		* \param port, use to setup the communication of the server.
		* \return void
		*/
		virtual void init(short port = 4242) override;

		/**
		* \fn Module::IModuleManager* getModuleManager() const
		* \brief Getter for the moduleManager of the server.
		*
		* \return Module::IModuleManager*
		*/
		virtual Module::IModuleManager * getModuleManager() const override;

		/**
		* \fn Client::IClientManager* getClientManager() const
		* \brief Getter for the clientManager of the server.
		*
		* \return Client::IClientManager*
		*/
		virtual Client::IClientManager * getClientManager() const override;

		/**
		* \fn Communication::ICommunicationManager* getCommunicationManager() const
		* \brief Getter for the communicationManager of the server.
		*
		* \return Communication::ICommunicationManager*
		*/
		virtual Communication::ICommunicationManager * getCommunicationManager() const override;

		/**
		* \fn Logger::ILogger* getLogger() const
		* \brief Getter for the logger of the server.
		*
		* \return Logger::ILogger*
		*/
		virtual Logger::ILogger * getLogger() const override;

	private:
		short									_port;
		Module::IModuleManager*					_moduleManager;
		Client::IClientManager*					_clientManager;
		Communication::ICommunicationManager*	_communicationManager;
		Task::ITaskManager*						_taskManager;
		Logger::ILogger*						_logger;
		bool									_isRunning;

		boost::asio::io_service*					_service;

	};
}
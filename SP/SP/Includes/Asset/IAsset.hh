/**
* \file IAsset.hh
* \brief IAsset header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* IAsset class header.
*
*/

#pragma once

#include <string>


/**
* \namespace Asset
* \brief Asset implementation
*
* Namespace for Asset and asset Management
*/
namespace Asset
{
	/**
	* \class IAsset
	* \brief Class header for IAsset
	*
	* Class for IAsset creation
	*/
	class						IAsset {

	public:
		/**
		* \fn ~IAsset()
		* \brief Destructor of a specific IAsset
		*/
		virtual					~IAsset() {};

		/**
		* \fn std::string getPath() const
		* \brief Getter returning path of a specific asset.
		*
		* \return std::string
		*/
		virtual std::string		getPath() const = 0;

		/**
		* \fn std::string getName() const
		* \brief Getter returning name of a specific asset.
		*
		* \return std::string
		*/
		virtual std::string		getName() const = 0;

		/**
		* \fn std::string getAsset() const
		* \brief Getter returning the class of a specific asset.
		*
		* \return void*
		*/
		virtual void*			getAsset() const = 0;
	};
}
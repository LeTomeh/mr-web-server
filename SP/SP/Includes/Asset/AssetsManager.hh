/**
* \file AssetsManager.hh
* \brief AssetsManager header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* AssetsManager class header.
*
*/

#pragma once

#include		"Asset/IAssetsManager.hh"
#include		"Logger/ILogger.hh"

#include		<string>

/**
* \namespace Asset
* \brief Asset implementation
*
* Namespace for Asset and asset Management
*/
namespace Asset
{
	/**
	* \class AssetsManager
	* \brief Class header for AssetsManager
	*
	* Class for AssetsManager creation
	*/
	class AssetsManager
		: public IAssetsManager {
		
	public:
		/**
		* \fn AssetsManager(const std::string, Logger::ILogger*))
		* \brief Constructor of a specific assetsManager.
		*
		* \param rootPathForAssets, use to get the path of modules.
		* \param logger, use to get the logger.
		* \return AssetsManager&
		*/
		AssetsManager(const std::string rootPathForAssets, Logger::ILogger* logger);

		/**
		* \fn ~AssetsManager()
		* \brief Destructor of a specific assetsManager
		*/
		virtual ~AssetsManager();

		/**
		* \fn std::string addNewAssets(unsigned long, std::string)
		* \brief Function adding asset from a specfic path to the specfic module.
		*
		* \param moduleID, use to get the id of the module.
		* \param path, use to get the path of the specific asset.
		* \return void
		*/
		virtual void addNewAssets(unsigned long moduleID, std::string path) override;

		/**
		* \fn unsigned long getModuleAssetsListSize(unsigned long) const
		* \brief Getter returning size of a specific assetList for a specific module.
		*
		* \param moduleID, using to get the id of the module.
		* \return unsigned long
		*/
		virtual unsigned long getModuleAssetsListSize(unsigned long moduleID) const override;

		/**
		* \fn Asset::IAssetsList* getModuleAssetsList(unsigned long) const
		* \brief Getter returning a pointer on a specific assetList for a specific module.
		*
		* \param moduleID, using to get the id of the module.
		* \return Asset::IAssetsList
		*/
		virtual IAssetsList * getModuleAssetsList(unsigned long moduleID) const override;

		/**
		* \fn std::list<std::string> getModuleAssetsListName(unsigned long) const
		* \brief Getter returning a list of name on a specific assetList for a specific module.
		*
		* \param moduleID, using to get the id of the module.
		* \return std::list<std::string>
		*/
		virtual std::list<std::string> getModuleAssetsListName(unsigned long moduleID) const override;

	private:
		const std::string		_rootPathForAssets;
		std::list<IAssetsList*>	_moduleAssetsList;
		std::list<std::string>	_moduleAssetsListName;
		Logger::ILogger*		_logger;
	};
}

/**
* \file Asset.hh
* \brief Asset header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* Asset class header.
*
*/

#include "Asset/IAsset.hh"

#include <string>

/**
 * \namespace Asset
 * \brief Asset implementation
 * 
 * Namespace for Asset and asset Management
 */
namespace Asset
{
	/**
	 * \class Asset
	 * \brief Class header for Asset
	 *
	 * Class for Asset creation
	 */
	class Asset
		: public IAsset
	{
	public:
		/**
		* \fn Asset(const std::string, const std::string)
		* \brief Constructor of a specific asset.
		*
		* \param name, use to get the name of the asset.
		* \param path, use to get the path of the asset.
		* \return Asset&
		*/
		Asset(const std::string name, const std::string path);

		/**
		* \fn ~Asset()
		* \brief Destructor of a specific asset
		*/
		virtual ~Asset();

		/**
		* \fn std::string getPath() const
		* \brief Getter returning path of a specific asset.
		*
		* \return std::string
		*/
		virtual std::string getPath() const override;

		/**
		* \fn std::string getName() const
		* \brief Getter returning name of a specific asset.
		*
		* \return std::string
		*/
		virtual std::string getName() const override;

		/**
		* \fn std::string getAsset() const
		* \brief Getter returning the class of a specific asset.
		*
		* \return void*
		*/
		virtual void * getAsset() const override;
	
	private:

		std::string			_path;
		std::string			_name;
		void*				_asset;
	};
}
/**
* \file IAssetsList.hh
* \brief AssetsList header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* IAssetsList class header.
*
*/

#pragma once

#include							<string>
#include							<list>
#include							"Asset/IAsset.hh"

/**
* \namespace Asset
* \brief Asset implementation
*
* Namespace for Asset and asset Management
*/
namespace Asset
{
	/**
	* \class IAssetsList
	* \brief Class header for IAssetsList
	*
	* Class for IAssetsList creation
	*/
	class								IAssetsList {
	public:
		/**
		* \fn ~IAssetsList()
		* \brief Destructor of a specific IAssetsList
		*/
		virtual							~IAssetsList() {};

		/**
		* \fn unsigned long getAssetsListSize() const
		* \brief Getter returning list size of a specific assetsList.
		*
		* \return unsigned long
		*/
		virtual unsigned long			getAssetsListSize() const = 0;

		/**
		* \fn std::list<Asset::IAsset*> getAssetsList() const
		* \brief Getter returning list of asset of a specific assetsList.
		*
		* \return std::list<Asset::IAsset*>
		*/
		virtual std::list<IAsset*>		getAssetsList() const = 0;

		/**
		* \fn std::list<std::string> getAssetsListName() const
		* \brief Getter returning list of name of a specific assetsList.
		*
		* \return std::list<std::string>
		*/
		virtual std::list<std::string>	getAssetsListName() const = 0;
	};
}
/**
* \file AssetsList.hh
* \brief AssetsList header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* AssetsList class header.
*
*/

#pragma once

#include "Asset/IAssetsList.hh"

/**
* \namespace Asset
* \brief Asset implementation
*
* Namespace for Asset and asset Management
*/
namespace Asset
{
	/**
	* \class AssetsList
	* \brief Class header for AssetsList
	*
	* Class for AssetsList creation
	*/
	class AssetsList
		: public IAssetsList {
	public:

		/**
		* \fn AssetsList(const std::string name, const std::string path)
		* \brief Constructor of a specific assets list.
		*
		* \return AssetsList&
		*/
		AssetsList();

		/**
		* \fn ~AssetsList()
		* \brief Destructor of a specific assetsList
		*/
		virtual ~AssetsList();

		/**
		* \fn unsigned long getAssetsListSize() const
		* \brief Getter returning list size of a specific assetsList.
		*
		* \return unsigned long
		*/
		virtual unsigned long getAssetsListSize() const override;

		/**
		* \fn std::list<Asset::IAsset*> getAssetsList() const
		* \brief Getter returning list of asset of a specific assetsList.
		*
		* \return std::list<Asset::IAsset*>
		*/
		virtual std::list<IAsset*> getAssetsList() const override;

		/**
		* \fn std::list<std::string> getAssetsListName() const
		* \brief Getter returning list of name of a specific assetsList.
		*
		* \return std::list<std::string>
		*/
		virtual std::list<std::string> getAssetsListName() const override;
	
	private:
		std::list<IAsset*>			_assetsList;
		std::list<std::string>		_assetsNameList;
	};
}
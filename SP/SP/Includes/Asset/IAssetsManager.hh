/**
* \file IAssetsManager.hh
* \brief IAssetsManager header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* IAssetsManager class header.
*
*/

#pragma once

#include							<string>
#include							<list>
#include							"Asset/IAssetsList.hh"

/**
* \namespace Asset
* \brief Asset implementation
*
* Namespace for Asset and asset Management
*/
namespace Asset
{
	/**
	* \class IAssetsManager
	* \brief Class header for IAssetsManager
	*
	* Class for IAssetsManager creation
	*/
	class								IAssetsManager {

	public:
		/**
		* \fn ~IAssetsManager()
		* \brief Destructor of a specific IAssetsManager
		*/
		virtual							~IAssetsManager() {};

		/**
		* \fn std::string addNewAssets(unsigned long, std::string)
		* \brief Function adding asset from a specfic path to the specfic module.
		*
		* \param moduleID, use to get the id of the module.
		* \param path, use to get the path of the specific asset.
		* \return void
		*/
		virtual void					addNewAssets(unsigned long moduleID, std::string path) = 0;

		/**
		* \fn unsigned long getModuleAssetsListSize(unsigned long) const
		* \brief Getter returning size of a specific assetList for a specific module.
		*
		* \param moduleID, using to get the id of the module.
		* \return unsigned long
		*/
		virtual unsigned long			getModuleAssetsListSize(unsigned long moduleID) const = 0;
		
		/**
		* \fn Asset::IAssetsList* getModuleAssetsList(unsigned long) const
		* \brief Getter returning a pointer on a specific assetList for a specific module.
		*
		* \param moduleID, using to get the id of the module.
		* \return Asset::IAssetsList
		*/
		virtual IAssetsList*			getModuleAssetsList(unsigned long moduleID) const = 0;
		
		/**
		* \fn std::list<std::string> getModuleAssetsListName(unsigned long) const
		* \brief Getter returning a list of name on a specific assetList for a specific module.
		*
		* \param moduleID, using to get the id of the module.
		* \return std::list<std::string>
		*/
		virtual std::list<std::string>	getModuleAssetsListName(unsigned long moduleID) const = 0;

	};
}
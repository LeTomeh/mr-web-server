#pragma once

#include "Task/Task.hh"
#include "Task/ETaskID.hh"

#include <string>
#include <map>

namespace Task
{
	class TaskGenerator {
	public : 
		static ITask* generateTask(e_TaskID taskID, std::map<std::string, void*> data);
	};
}
/**
* \file Worker.hh
* \brief Worker header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* Worker class header.
*
*/

#pragma once

#include	"Task/IWorker.hh"
#include	"Task/Task.hh"
#include	"Task/ITaskManager.hh"

#include	"Module/IModule.hh"

#include	"Errors/Module/CantStopModule.hh"
#include	"Errors/Module/CantLaunchModule.hh"
#include	"Errors/Module/CantPauseModule.hh"
#include	"Errors/Module/CantFindModule.hh"

#include	"Logger/Log.hh"

#include	<boost/thread.hpp>

/**
* \namespace Task
* \brief Task implementation
*
* Namespace for Task and Task Management
*/	
namespace Task
{
	/**
	* \class Worker
	* \brief Class header Worker
	*
	* Class for Worker creation
	*/
	class Worker
		: public IWorker
	{
	private:
		typedef void (Worker::*fWorkerPtr)(Core::IServer* server, Task::ITask* task);



		/**
		* \fn void listAll(Core::IServer* server, Task::ITask* task)
		* \brief Function listing all modules for a specific client
		*
		* \param server, used to access to the server
		* \param task, used to access to the necessary data
		*
		* \return void
		*/
		void	listAll(Core::IServer* server, Task::ITask* task);

		/**
		* \fn void listModule(Core::IServer* server, Task::ITask* task)
		* \brief Function listing a specific modules for a specific client
		*
		* \param server, used to access to the server
		* \param task, used to access to the necessary data
		*
		* \return void
		*/
		void	listModule(Core::IServer* server, Task::ITask* task);

		/**
		* \fn void listModuleON(Core::IServer* server, Task::ITask* task)
		* \brief Function listing every modules online for a specific client
		*
		* \param server, used to access to the server
		* \param task, used to access to the necessary data
		*
		* \return void
		*/
		void	listModuleON(Core::IServer* server, Task::ITask* task);

		/**
		* \fn void listModuleOFF(Core::IServer* server, Task::ITask* task)
		* \brief Function listing every modules offline for a specific client
		*
		* \param server, used to access to the server
		* \param task, used to access to the necessary data
		*
		* \return void
		*/
		void	listModuleOFF(Core::IServer* server, Task::ITask* task);

		/**
		* \fn void listModuleVR(Core::IServer* server, Task::ITask* task)
		* \brief Function listing every modules allowing VR for a specific client
		*
		* \param server, used to access to the server
		* \param task, used to access to the necessary data
		*
		* \return void
		*/
		void	listModuleVR(Core::IServer* server, Task::ITask* task);
		
		/**
		* \fn void listModuleAR(Core::IServer* server, Task::ITask* task)
		* \brief Function listing every modules allowing AR for a specific client
		*
		* \param server, used to access to the server
		* \param task, used to access to the necessary data
		*
		* \return void
		*/
		void	listModuleAR(Core::IServer* server, Task::ITask* task);

		/**
		* \fn void launchModule(Core::IServer* server, Task::ITask* task)
		* \brief Function launching a specfic module for a specific client
		*
		* \param server, used to access to the server
		* \param task, used to access to the necessary data
		*
		* \return void
		*/
		void	launchModule(Core::IServer* server, Task::ITask* task);

		/**
		* \fn void pauseModule(Core::IServer* server, Task::ITask* task)
		* \brief Function pausing a specfic module for a specific client
		*
		* \param server, used to access to the server
		* \param task, used to access to the necessary data
		*
		* \return void
		*/
		void	pauseModule(Core::IServer* server, Task::ITask* task);

		/**
		* \fn void addUserToModule(Core::IServer* server, Task::ITask* task)
		* \brief Function adding a specific client to a specific module
		*
		* \param server, used to access to the server
		* \param task, used to access to the necessary data
		*
		* \return void
		*/
		void	addUserToModule(Core::IServer* server, Task::ITask* task);
		
		/**
		* \fn void logoutUser(Core::IServer* server, Task::ITask* task)
		* \brief Function disconnecting a specific client to a specific module
		*
		* \param server, used to access to the server
		* \param task, used to access to the necessary data
		*
		* \return void
		*/
		void	logoutUser(Core::IServer* server, Task::ITask* task);
		
		/**
		* \fn void stopModule(Core::IServer* server, Task::ITask* task)
		* \brief Function disconnecting a specific module
		*
		* \param server, used to access to the server
		* \param task, used to access to the necessary data
		*
		* \return void
		*/
		void	stopModule(Core::IServer* server, Task::ITask* task);
		
		/**
		* \fn void switchConnectionType(Core::IServer* server, Task::ITask* task)
		* \brief Function switching connection type for a specific web server
		*
		* \param server, used to access to the server
		* \param task, used to access to the necessary data
		*
		* \return void
		*/
		void	switchConnectionType(Core::IServer* server, Task::ITask* task);
		
		/**
		* \fn void hostingNewConnection(Core::IServer* server, Task::ITask* task)
		* \brief Function to manage the connection for a specific web server
		*
		* \param server, used to access to the server
		* \param task, used to access to the necessary data
		*
		* \return void
		*/
		void	hostingNewConnection(Core::IServer* server, Task::ITask* task);

		/**
		* \fn void uploadPackage(Core::IServer* server, Task::ITask* task)
		* \brief Function uploading a module for a specific web server
		*
		* \param server, used to access to the server
		* \param task, used to access to the necessary data
		*
		* \return void
		*/
		void	uploadPackage(Core::IServer* server, Task::ITask* task);

		/**
		* \fn void downloadPackage(Core::IServer* server, Task::ITask* task)
		* \brief Function downloading a module for a specific web server
		*
		* \param server, used to access to the server
		* \param task, used to access to the necessary data
		*
		* \return void
		*/
		void	downloadPackage(Core::IServer* server, Task::ITask* task);

		/**
		* \fn void sendResponse(Core::IServer* server, Task::ITask* task)
		* \brief Function sending response for a specific client
		*
		* \param server, used to access to the server
		* \param task, used to access to the necessary data
		*
		* \return void
		*/
		void	sendResponse(Core::IServer* server, Task::ITask* task);

	public:
		/**
		* \fn Worker()
		* \brief Constructor of a specific Worker.
		*
		* \return Worker&
		*/
		Worker(ITaskManager* taskManager);

		/**
		* \fn ~Worker()
		* \brief Destructor of a specific Worker
		*/
		virtual ~Worker();

		/**
		* \fn void yield()
		* \brief Function to make a worker yield.
		*
		* \return void
		*/
		virtual void yield() override;

		/**
		* \fn void doTask(Core::IServer* server, ITask* task)
		* \brief Function handling a task for a specific worker.
		*
		* \param server, used to access data.
		* \param task, used to acces task informations
		* \return void
		*/
		virtual void doTask(Core::IServer* server, Task::ITask* task) override;

		/**
		* \fn boolc isBusy() const
		* \brief Getter returning a boolean associated to the activity of a specific worker.
		*
		* \return bool
		*/
		virtual bool isBusy() const override;

	private:
		ITaskManager*				_taskManager;
		std::map<e_TaskID, fWorkerPtr>	_task;
	};
}
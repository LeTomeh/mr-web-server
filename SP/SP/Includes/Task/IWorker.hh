/**
* \file IWorker.hh
* \brief IWorker header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* IWorker class header.
*
*/

#pragma once

#include "Task/ITask.hh"
#include "Core/IServer.hh"

/**
* \namespace Task
* \brief Task implementation
*
* Namespace for Task and Task Management
*/
namespace Task
{
	/**
	* \class IWorker
	* \brief Class header IWorker
	*
	* Class for IWorker creation
	*/
	class				IWorker {
	public:

		/**
		* \fn ~IWorker()
		* \brief Destructor of a specific IWorker
		*/
		virtual			~IWorker() {};

		/**
		* \fn void yield()
		* \brief Function to make a worker yield.
		*
		* \return void
		*/
		virtual void	yield() = 0;

		/**
		* \fn void doTask(Core::IServer* server, ITask* task)
		* \brief Function handling a task for a specific worker.
		*
		* \param server, used to access data.
		* \param task, used to acces task informations
		* \return void
		*/
		virtual void	doTask(Core::IServer* server, Task::ITask* task) = 0;

		/**
		* \fn boolc isBusy() const
		* \brief Getter returning a boolean associated to the activity of a specific worker.
		*
		* \return bool
		*/
		virtual bool	isBusy() const = 0;
	};
}
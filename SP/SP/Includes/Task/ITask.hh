/**
* \file Task.hh
* \brief Task header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* Task class header.
*
*/

#pragma once

#include				"Task/ETaskID.hh"
#include				<string>

/**
* \namespace Task
* \brief Task implementation
*
* Namespace for Task and Task Management
*/
namespace Task
{
	/**
	* \class ITask
	* \brief Class header ITask
	*
	* Class for ITask creation
	*/
	class					ITask
	{
	public:

		/**
		* \fn ~ITask()
		* \brief Destructor of a specific ITask
		*/
		virtual				~ITask() {};

		/**
		* \fn Task::e_TaskID getTaskID() const
		* \brief Getter returning the id of a specific task.
		*
		* \return Task::e_TaskID
		*/
		virtual e_TaskID	getTaskID() const = 0;

		/**
		* \fn Task::e_TaskID getDatas(std::string) const
		* \brief Getter returning the data of a specific task.
		*
		* \param key, use to get the right data associated to the given key.
		* \return Task::e_TaskID
		*/
		virtual void*		getDatas(std::string key) const = 0;
	};
}
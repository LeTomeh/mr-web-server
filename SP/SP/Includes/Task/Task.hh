/**
* \file Task.hh
* \brief Task header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* Task class header.
*
*/

#pragma once

#include	"Task/ITask.hh"

#include	<map>

/**
* \namespace Task
* \brief Task implementation
*
* Namespace for Task and Task Management
*/
namespace Task
{
	/**
	* \class Task
	* \brief Class header Task
	*
	* Class for Task creation
	*/
	class Task
		: public ITask
	{
	public:
		/**
		* \fn Task(e_TaskID, void*)
		* \brief Constructor of a specific Task.
		*
		* \param taskID, use to get the type of the task.
		* \param data, use to get the data of the task.
		* \return Task&
		*/
		Task(e_TaskID taskID, std::map<std::string, void*> data);

		/**
		* \fn Task(e_TaskID taskID)
		* \brief Constructor of a specific Task.
		*
		* \param taskID, use to get the type of the task.
		* \return Task&
		*/
		Task(e_TaskID taskID);

		/**
		* \fn ~Task()
		* \brief Destructor of a specific Task
		*/
		virtual ~Task();

		/**
		* \fn Task::e_TaskID getTaskID() const
		* \brief Getter returning the id of a specific task.
		*
		* \return Task::e_TaskID
		*/
		virtual e_TaskID getTaskID() const override;

		/**
		* \fn Task::e_TaskID getDatas(std::string) const
		* \brief Getter returning the data of a specific task.
		*
		* \param key, use to get the right data associated to the given key.
		* \return Task::e_TaskID
		*/
		virtual void * getDatas(std::string key) const override;

	private:
		e_TaskID						_taskID;
		std::map<std::string, void*>	_datas;
	};
}
/**
* \file ITaskManager.hh
* \brief ITaskManager header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* ITaskManager class header.
*
*/

#pragma once

#include				"Task/ITask.hh"

/**
* \namespace Task
* \brief Task implementation
*
* Namespace for Task and Task Management
*/
namespace Task
{
	/**
	* \class ITaskManager
	* \brief Class header ITaskManager
	*
	* Class for ITaskManager creation
	*/
	class					ITaskManager {
	public:

		/**
		* \fn ~ITaskManager()
		* \brief Destructor of a specific ITaskManager
		*/
		virtual				~ITaskManager() {};

		/**
		* \fn void addTask(ITask*)
		* \brief Function adding a task to a specific taskManager.
		*
		* \param task, use to add the task.
		* \return void
		*/
		virtual void		addTask(ITask*) = 0;
		

	};
}
/**
* \file ETaskID.hh
* \brief e_TaskID header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* e_TaskID enum header.
*
*/

#pragma once

/**
* \namespace Task
* \brief Task implementation
*
* Namespace for Task and Task Management
*/
namespace Task
{
	/**
	* \enum e_TaskID
	* \brief e_TaskID header
	*
	* Enum for task id
	*/
	enum e_TaskID
	{
		LAUNCH_MODULE,
		STOP_MODULE,
		PAUSE_MODULE,
		ADD_CLIENT,
		PAUSE_CLIENT,
		CLOSE_CLIENT,
		LIST_MODULE,
		LIST_MODULE_AR,
		LIST_MODULE_VR,
		LIST_MODULE_ON,
		LIST_MODULE_OFF,
		LIST_ALLMODULE,
		SWITCHING_CONNECTION,
		HOSTING_CONNECTION,
		UPLOAD_FILE,
		DOWNLOAD_FILE,
		RESPONSE
	};
}
/**
* \file TaskManager.hh
* \brief TaskManager header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* TaskManager class header.
*
*/

#pragma once

#include	"Task/ITaskManager.hh"
#include	"Task/Worker.hh"
#include	"Core/IServer.hh"
#include	"Logger/ILogger.hh"

/**
* \namespace Task
* \brief Task implementation
*
* Namespace for Task and Task Management
*/
namespace Task
{
	/**
	* \class TaskManager
	* \brief Class header TaskManager
	*
	* Class for TaskManager creation
	*/
	class TaskManager
		: public ITaskManager
	{
	public:
		/**
		* \fn TaskManager(Core::IServer*, Logger::ILogger* logger, unsigned int)
		* \brief Constructor of a specific TaskManager.
		*
		* \param server, use to attach server.
		* \param logger, used to get access to the logger.
		* \param worker, use to get the number of workers.
		* \return TaskManager&
		*/
		TaskManager(Core::IServer* server, Logger::ILogger* logger, unsigned int workers = 2);

		/**
		* \fn ~TaskManager()
		* \brief Destructor of a specific TaskManager
		*/
		virtual ~TaskManager();


		/**
		* \fn void addTask(ITask*)
		* \brief Function adding a task to a specific taskManager.
		*
		* \param task, use to add the task.
		* \return void
		*/
		virtual void addTask(ITask * task) override;


	private:
		boost::asio::io_service			_service;
		boost::asio::io_service::work	_worker;
		boost::thread_group				_threadpool;

		Logger::ILogger*				_logger;
		Core::IServer*					_server;
	};
}
/**
* \file ICommunicationManager.hh
* \brief ICommunicationManager header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* ICommunicationManager class header.
*
*/

#pragma once

#include			<string>
#include			<boost\asio.hpp>
#include			<boost/bind.hpp>
#include			"Client/IClient.hh"

/**
* \namespace Communication
* \brief Communication implementation
*
* Namespace for Communication and Communication Management
*/
namespace Communication
{
	/**
	* \class ICommunicationManager
	* \brief Class header for ICommunicationManager
	*
	* Class for ICommunicationManager creation
	*/
	class ICommunicationManager {
	public:
		/**
		* \fn ~ICommunicationManager()
		* \brief Destructor of a specific ICommunicationManager
		*/
		virtual ~ICommunicationManager() {};

		/**
		* \fn void init(short)
		* \brief Function to init a specific CommunictionManager.
		*
		* \param port, use to init the server socket.
		* \return void
		*/
		virtual void init(short port) = 0;

		/**
		* \fn void clean()
		* \brief Function to clear a specific CommunictionManager.
		*
		* \return void
		*/
		virtual void clean() = 0;

		/**
		* \fn void handleIOSocket()
		* \brief Function to handle every Input and Output data for a specific CommunictionManager.
		*
		* \return void
		*/
		virtual void handleIOSocket() = 0;

		/**
		* \fn void sendFileToServer(unsigned long, std::string)
		* \brief Function to send a file to a specific server.
		*
		* \param serverID, use to get server id.
		* \param path, use to get the path of the file to send.
		* \return void
		*/
		virtual void sendFileToServer(unsigned long serverID, std::string path) = 0;

		/**
		* \fn void sendFileToServer(unsigned long, unsigned long)
		* \brief Function to receive a file from a specific server.
		*
		* \param serverID, use to get server id.
		* \param moduleID, use to get the module for the file.
		* \return void
		*/
		virtual void recvFileFromServer(unsigned long serverID, unsigned long moduleID) = 0;
		
		/**
		* \fn Client::IClient* createConnexion();
		* \brief Function creating connexion between server and module.
		*
		* \return Client::IClient*
		*/
		virtual Client::IClient* createConnexion() = 0;

	protected:
		/**
		* \fn void addNewClient(boost::asio::ip::tcp::socket)
		* \brief Function to add a client to the client list.
		*
		* \param clientSocket, use to get client socket.
		* \return void
		*/
		virtual void addNewClient(Client::IClient* newConnection, const boost::system::error_code& error) = 0;

		/**
		* \fn void removeClient(unsigned long)
		* \brief Function to remove a client of the client list.
		*
		* \param clientID, use to get client id.
		* \return void
		*/
		virtual void removeClient(unsigned long clientID) = 0;
	};
}
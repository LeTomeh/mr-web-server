/**
* \file CommunicationManager.hh
* \brief CommunicationManager header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* CommunicationManager class header.
*
*/

#pragma once

#include "Communication/ICommunicationManager.hh"

#include "Client/IClientManager.hh"
#include "Client/Client.hh"

#include "Task/ITaskManager.hh"

#include "Logger/ILogger.hh"
#include "Logger/Log.hh"

#include <boost/bind.hpp>

/**
* \namespace Communication
* \brief Communication implementation
*
* Namespace for Communication and Communication Management
*/
namespace Communication
{
	/**
	* \class CommunicationManager
	* \brief Class header for CommunicationManager
	*
	* Class for CommunicationManager creation
	*/
	class CommunicationManager
		: public ICommunicationManager
	{
	public:
		/**
		* \fn CommunicationManager(Client::IClientManager*, Task::ITaskManager*, Logger::ILogger*, boost::asio::io_service& service, short port)
		* \brief Constructor of a specific CommunicationManager.
		*
		* \param clientManager, use to get the clientManager.
		* \param taskManager, use to get the taskManager.
		* \param logger, use to get the logger.
		* \param service, use to accept and handle connections.
		* \param port, use to set the port for the server.
		* \return CommunicationManager&
		*/
		CommunicationManager(Client::IClientManager* clientManager, Task::ITaskManager* taskManager, Logger::ILogger* logger, boost::asio::io_service& service, short port);

		/**
		* \fn ~CommunicationManager()
		* \brief Destructor of a specific CommunicationManager
		*/
		virtual ~CommunicationManager();

		/**
		* \fn void init(short)
		* \brief Function to init a specific CommunictionManager.
		*
		* \param port, use to init the server socket.
		* \return void
		*/
		virtual void init(short port) override;

		/**
		* \fn void clean()
		* \brief Function to clear a specific CommunictionManager.
		*
		* \return void
		*/
		virtual void clean() override;

		/**
		* \fn void handleIOSocket()
		* \brief Function to handle every Input and Output data for a specific CommunictionManager.
		*
		* \return void
		*/
		virtual void handleIOSocket() override;

		/**
		* \fn void sendFileToServer(unsigned long, std::string)
		* \brief Function to send a file to a specific server.
		*
		* \param serverID, use to get server id.
		* \param path, use to get the path of the file to send.
		* \return void
		*/
		virtual void sendFileToServer(unsigned long serverID, std::string path) override;

		/**
		* \fn void sendFileToServer(unsigned long, unsigned long)
		* \brief Function to receive a file from a specific server.
		*
		* \param serverID, use to get server id.
		* \param moduleID, use to get the module for the file.
		* \return void
		*/
		virtual void recvFileFromServer(unsigned long serverID, unsigned long moduleID) override;

		/**
		* \fn void addNewClient(boost::asio::ip::tcp::socket)
		* \brief Function to add a client to the client list.
		*
		* \param clientSocket, use to get client socket.
		* \return void
		*/
		virtual void addNewClient(Client::IClient* newConnection, const boost::system::error_code& error) override;

		/**
		* \fn void removeClient(unsigned long)
		* \brief Function to remove a client of the client list.
		*
		* \param clientID, use to get client id.
		* \return void
		*/
		virtual void removeClient(unsigned long clientID) override;

		/**
		* \fn Client::IClient* createConnexion();
		* \brief Function creating connexion between server and module.
		*
		* \return Client::IClient*
		*/
		virtual Client::IClient* createConnexion() override;

	private:
		Client::IClientManager*			_clientManager;
		Task::ITaskManager*				_taskManager;
		Logger::ILogger*				_logger;
		
		short							_port;
		boost::asio::ip::tcp::acceptor	_acceptor;
		boost::asio::streambuf			_response;
	};
}
#pragma once

#include "Communication/ICommunicationManager.hh"
#include "Client/IClientManager.hh"
#include "Task/ITaskManager.hh"
#include "Logger/ILogger.hh"

namespace Communication
{
	class CommunicationManager
		: public ICommunicationManager
	{
	public:
		
		CommunicationManager(Client::IClientManager* clientManager, Task::ITaskManager* taskManager, Logger::ILogger* logger);
		virtual ~CommunicationManager();

		// H�rit� via ICommunicationManager
		virtual void init(short port) override;
		virtual void clean() override;
		virtual void handleIOSocket() override;
		virtual void sendFileToServer(unsigned long serverID, std::string path) override;
		virtual void recvFileFromServer(unsigned long serverID, unsigned long moduleID) override;
		virtual void addNewClient(boost::asio::ip::tcp::socket clientSocket) override;
		virtual void removeClient(unsigned long clientID) override;

	private:
		Client::IClientManager*			_clientManager;
		Task::ITaskManager*				_taskManager;
		Logger::ILogger*				_logger;
	};
}
/**
* \file ModuleDescription.hh
* \brief ModuleDescription header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* ModuleDescription class header.
*
*/

#pragma once

#include "Module/IModuleDescription.hh"

/**
* \namespace Module
* \brief Module implementation
*
* Namespace for Module and Module Management
*/
namespace Module
{
	/**
	* \class ModuleDescription
	* \brief Class header for ModuleDescription
	*
	* Class for ModuleDescription creation
	*/
	class ModuleDescription
		: public IModuleDescription
	{
	public:
		/**
		* \fn AssModuleDescriptionet(const std::string)
		* \brief Constructor of a specific ModuleDescription.
		*
		* \param description, use to get the description of the ModuleDescription.
		* \return ModuleDescription&
		*/
		ModuleDescription(const std::string description);

		/**
		* \fn ~ModuleDescription()
		* \brief Destructor of a specific ModuleDescription
		*/
		virtual ~ModuleDescription();

		/**
		* \fn unsigned long getDescriptionSize() const
		* \brief Getter returning size of the description of a specific ModuleDescription.
		*
		* \return unsigned long
		*/
		virtual unsigned long getDescriptionSize() const override;

		/**
		* \fn std::string getDescription() const
		* \brief Getter returning the description of a specific ModuleDescription.
		*
		* \return std::string
		*/
		virtual std::string getDescription() const override;
	private:
		const std::string			_description;
	};
}
#pragma once

/**
* \namespace Module
* \brief Module implementation
*
* Namespace for Module and Module Management
*/
namespace Module 
{
	/**
	* \enum e_ModuleDisponibility
	* \brief e_ModuleDisponibility header
	*
	* enum for module disponibility
	*/
	enum e_ModuleDisponibility
	{
		LAUNCH,
		CLOSED,
		PAUSED
	};
}
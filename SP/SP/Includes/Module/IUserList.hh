/**
* \file IUserList.hh
* \brief IUserList header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* IUserList class header.
*
*/

#pragma once

#include								<list>

/**
* \namespace Module
* \brief Module implementation
*
* Namespace for Module and Module Management
*/
namespace Module
{
	/**
	* \class IUserList
	* \brief Class header for IUserList
	*
	* Class for IUserList creation
	*/
	class									IUserList {
	public:
		/**
		* \fn ~IUserList()
		* \brief Destructor of a specific IUserList
		*/
		virtual								~IUserList() {};


		/**
		* \fn unsigned long getUserListSize() const
		* \brief Getter returning the size of a specific UserList.
		*
		* \return unsigned long
		*/
		virtual unsigned long				getUserListSize() const = 0;

		/**
		* \fn std::list<unsigned long> getUserList() const
		* \brief Getter returning the list of id from a specific UserList.
		*
		* \return std::list<unsigned long>
		*/
		virtual std::list<unsigned long>	getUserList() const = 0;

		/**
		* \fn void addUser(unsigned long)
		* \brief Function adding a new user to the user list.
		*
		* \return void
		*/
		virtual void						addUser(unsigned long userId) = 0;

		/**
		* \fn removeUser(unsigned long clientId)
		* \brief Function removing a user to the user list
		*
		* \param clientId, used to remove id of the user from the userlist
		* \return void
		*/
		virtual void removeUser(unsigned long userId) = 0;
	};
}
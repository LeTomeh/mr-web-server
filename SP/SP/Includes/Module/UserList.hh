/**
* \file UserList.hh
* \brief UserList header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* UserList class header.
*
*/

#pragma once

#include "Module/IUserList.hh"

/**
* \namespace Module
* \brief Module implementation
*
* Namespace for Module and Module Management
*/
namespace Module
{
	/**
	* \class UserList
	* \brief Class header UserList
	*
	* Class for UserList creation
	*/
	class UserList
		: public IUserList
	{
	public :
		/**
		* \fn UserList()
		* \brief Constructor of a specific UserList.
		*
		* \return UserList&
		*/
		UserList();

		/**
		* \fn ~UserList()
		* \brief Destructor of a specific UserList
		*/
		virtual ~UserList();



		/**
		* \fn unsigned long getUserListSize() const
		* \brief Getter returning the size of a specific UserList.
		*
		* \return unsigned long
		*/
		virtual unsigned long getUserListSize() const override;

		/**
		* \fn std::list<unsigned long> getUserList() const
		* \brief Getter returning the list of id from a specific UserList.
		*
		* \return std::list<unsigned long>
		*/
		virtual std::list<unsigned long> getUserList() const override;

		/**
		* \fn void addUser(unsigned long clientId)
		* \brief Function adding a new user to the user list.
		*
		* \param clientId, used to add id of the user to the userlist
		* \return void
		*/
		virtual void addUser(unsigned long userId) override;

		/**
		* \fn removeUser(unsigned long clientId)
		* \brief Function removing a user to the user list
		*
		* \param clientId, used to remove id of the user from the userlist
		* \return void
		*/
		virtual void removeUser(unsigned long userId) override;

	private:
		std::list<unsigned long>		_userList;
	};
}
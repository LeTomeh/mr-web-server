/**
* \file IModule.hh
* \brief IModule header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* IModule class header.
*
*/

#pragma once

namespace Module
{
	class IModule;
}

#include							<string>
#include							"Core/IServer.hh"
#include							"Module/IModuleDescription.hh"
#include							"Module/IUserList.hh"
#include							"Module/EModuleDisponibility.hh"

/**
* \namespace Module
* \brief Module implementation
*
* Namespace for Module and Module Management
*/
namespace Module
{
	/**
	* \class IModule
	* \brief Class header for IModule
	*
	* Class for IModule creation
	*/
	class								IModule
	{
	public:
		/**
		* \fn ~IModule()
		* \brief Destructor of a specific IModule
		*/
		virtual							~IModule() {};


		/**
		* \fn bool isLaunched() const
		* \brief Function use to know if the module is launched.
		*
		* \return bool
		*/
		virtual bool					isLaunched() const = 0;

		/**
		* \fn unsigned long getID() const
		* \brief Getter returning the id of a specific module.
		*
		* \return unsigned long
		*/
		virtual unsigned long			getID() const = 0;

		/**
		* \fn std::string getPath() const
		* \brief Getter returning path of a specific module.
		*
		* \return std::string
		*/
		virtual std::string				getPath() const = 0;

		/**
		* \fn Module::e_ModuleDisponibility getModuleDisponibility() const
		* \brief Getter returning the disponibility of a specific module.
		*
		* \return Module::e_ModuleDisponibility
		*/
		virtual e_ModuleDisponibility	getModuleDisponibility() const = 0;


		/**
		* \fn std::string getName() const
		* \brief Getter returning name of a specific module.
		*
		* \return std::string
		*/
		virtual std::string				getName() const = 0;

		/**
		* \fn const Module::IModuleDescription* getDescription() const
		* \brief Getter returning description of a specific module.
		*
		* \return const Module::IModuleDescription*
		*/
		virtual const IModuleDescription*		getDescription() const = 0;

		/**
		* \fn bool allowAR() const
		* \brief Function returning the AR compatibility of a specific module.
		*
		* \return bool
		*/
		virtual bool					allowAR() const = 0;

		/**
		* \fn bool allowVR() const
		* \brief Function returning the VR compatibility of a specific module.
		*
		* \return bool
		*/
		virtual bool					allowVR() const = 0;

		/**
		* \fn Module::IUserList* getUserList() const
		* \brief Getter returning the uset list of a specific module.
		*
		* \return Module::IUserList*
		*/
		virtual IUserList*				getUserList() const = 0;

		/**
		* \fn void connectNewClient(unsigned long)
		* \brief Function adding a client to the client list of a specific module.
		*
		* \param clientID, use to get the client id.
		* \return void
		*/
		virtual void					connectNewClient(unsigned long clientID) = 0;

		/**
		* \fn void pauseClient(unsigned long)
		* \brief Function pausing a specific client from a specific module.
		*
		* \param clientID, use to get the client id.
		* \return void
		*/
		virtual void					pauseClient(unsigned long clientID) = 0;

		/**
		* \fn void unpauseModule()
		* \brief Function unpausing a specific module.
		*
		* \return void
		*/
		virtual void					unpauseModule() = 0;

		/**
		* \fn void disconnectClient(unsigned long)
		* \brief Function removing a specific client from a specific module.
		*
		* \param clientID, use to get the client id.
		* \return void
		*/
		virtual void					disconnectClient(unsigned long clientID) = 0;

		/**
		* \fn void launch(unsigned long clientID, Core::IServer* server)
		* \brief Function starting a specific module.
		*
		* \param clientID, use to get the id of the client who launch the module.
		* \param server, use to get the server.
		* \return void
		*/
		virtual void					launch(unsigned long clientID, Core::IServer* server) = 0;

		/**
		* \fn void pauseModule()
		* \brief Function pausing a specific module.
		*
		* \return void
		*/
		virtual void					pauseModule() = 0;

		/**
		* \fn void close()
		* \brief Function closing a specific module.
		*
		* \return void
		*/
		virtual void					close() = 0;

		/**
		* \fn bool isAvailable() const
		* \brief Getter to know if module is available.
		*
		* \return bool
		*/
		virtual bool					isAvailable() const = 0;

		/**
		* \fn void setAvailable(bool available)
		* \brief Setter to know if module is available.
		*
		* \param available, use to set the availability of the module.
		* \return void
		*/
		virtual void					setAvailable(bool available) = 0;
	};
}
/**
* \file ModuleManager.hh
* \brief ModuleManager header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* ModuleManager class header.
*
*/

#pragma once

#include "Module/IModuleManager.hh"
#include "Module/IModule.hh"
#include "Module/ModuleFactory.hh"

#include "Client/IClientManager.hh"

#include "Logger/ILogger.hh"

#include "Task/ITaskManager.hh"

#include "Asset/AssetsManager.hh"

#include "Errors/Module/CantFindModule.hh"
#include "Errors/Module/CantLaunchModule.hh"
#include "Errors/Module/CantStopModule.hh"

#include <vector>

/**
* \namespace Module
* \brief Module implementation
*
* Namespace for Module and Module Management
*/
namespace Module
{
	/**
	* \class ModuleManager
	* \brief Class header ModuleManager
	*
	* Class for ModuleManager creation
	*/
	class ModuleManager
		: public IModuleManager
	{
	public:
		/**
		* \fn ModuleManager(const std::string, const std::string)
		* \brief Constructor of a specific ModuleManager.
		*
		* \param name, use to get the name of the asset.
		* \param path, use to get the path of the asset.
		* \return ModuleManager&
		*/
		ModuleManager(Client::IClientManager* clientManager, Logger::ILogger* logger);

		/**
		* \fn ~ModuleManager()
		* \brief Destructor of a specific ModuleManager
		*/
		virtual ~ModuleManager();

		/**
		* \fn void init()
		* \brief Function handling the initialisation of a specific ModuleManager.
		*
		* \return void
		*/
		virtual void init() override;

		/**
		* \fn void update()
		* \brief Function handling the updating of every module for a specific ModuleManager.
		*
		* \return void
		*/
		virtual void update() override;

		/**
		* \fn void clean()
		* \brief Function cleaning a specific ModuleManager.
		*
		* \return void
		*/
		virtual void clean() override;

		/**
		* \fn void createNewModule()
		* \brief Function handling the creation of a new module for specific ModuleManager.
		*
		* \return void
		*/
		virtual void createNewModule() override;

		/**
		* \fn void addClientToModule(unsigned long, unsigned long)
		* \brief Function adding a specific client to a specific module.
		*
		* \param clientID, use to get the client id.
		* \param moduleID, use to get the module id.
		* \return void
		*/
		virtual void addClientToModule(unsigned long clientID, unsigned long moduleID) override;

		/**
		* \fn void pauseClientToModule(unsigned long, unsigned long)
		* \brief Function pausing a specific client to a specific module.
		*
		* \param clientID, use to get the client id.
		* \param moduleID, use to get the module id.
		* \return void
		*/
		virtual void pauseClientToModule(unsigned long clientID, unsigned long moduleID) override;

		/**
		* \fn void removeClientToModule(unsigned long, unsigned long)
		* \brief Function removing a specific client to a specific module.
		*
		* \param clientID, use to get the client id.
		* \param moduleID, use to get the module id.
		* \return void
		*/
		virtual void removeClientToModule(unsigned long clientID, unsigned long moduleID) override;

		/**
		* \fn void launchModule(unsigned long, unsigned long, Core::IServer*)
		* \brief Function launching a specific module.
		*
		* \param clientID, use to get the client id.
		* \param moduleID, use to get the module id.
		* \param server, used to get the server.
		* \return void
		*/
		virtual void launchModule(unsigned long clientID, unsigned long moduleID, Core::IServer* server) override;

		/**
		* \fn void pauseModule(unsigned long)
		* \brief Function pausing a specific module.
		*
		* \param moduleID, use to get the module id.
		* \return void
		*/
		virtual void pauseModule(unsigned long moduleID) override;

		/**
		* \fn void stopModule(unsigned long)
		* \brief Function stoping a specific module.
		*
		* \param moduleID, use to get the module id.
		* \return void
		*/
		virtual void stopModule(unsigned long moduleID) override;

		/**
		* \fn IModule* getModule(unsigned long) const
		* \brief Function returning a specific module.
		*
		* \param moduleID, use to get the module id.
		* \return IModule*
		*/
		virtual IModule* getModule(unsigned long moduleID) const override;

		/**
		* \fn std::vector<IModule*> getModuleList() const
		* \brief Function returning a specific module.
		*
		* \param moduleID, use to get the module id.
		* \return std::vector<IModule*>
		*/
		virtual std::vector<IModule*> getModuleList() const override;

		/**
		* \fn unsigned long getModuleListSize() const
		* \brief Getter returning the size of the module list.
		*
		* \return unsigned long
		*/
		virtual unsigned long getModuleListSize() const override;

		/**
		* \fn std::list<unsigned long> getModuleVRList() const
		* \brief Getter returning the VR module list.
		*
		* \return std::list<unsigned long>
		*/
		virtual std::list<unsigned long> getModuleVRList() const override;

		/**
		* \fn std::list<unsigned long> getModuleARList() const
		* \brief Getter returning the AR module list.
		*
		* \return std::list<unsigned long>
		*/
		virtual std::list<unsigned long> getModuleARList() const override;

		/**
		* \fn std::list<unsigned long> getModuleLaunched() const
		* \brief Getter returning the launched module list.
		*
		* \return std::list<unsigned long>
		*/
		virtual std::list<unsigned long> getModuleLaunched() const override;

		/**
		* \fn std::list<unsigned long> getModuleClosed() const
		* \brief Getter returning the closed module list.
		*
		* \return std::list<unsigned long>
		*/
		virtual std::list<unsigned long> getModuleClosed() const override;

		/**
		* \fn void addNewAssets(unsigned long, std::string)
		* \brief Function adding a new asset to a specific module.
		*
		* \param moduleID, use to get the id of the module.
		* \param path, use to get the path of the asset.
		* \return void
		*/
		virtual void addNewAssets(unsigned long moduleID, std::string path) override;

		/**
		* \fn unsigned long getModuleAssetsListSize(unsigned long) const
		* \brief Getter returning the size of the asset list for a specific module.
		*
		* \param moduleID, use to get the id of the module.
		* \return unsigned long
		*/
		virtual unsigned long getModuleAssetsListSize(unsigned long moduleID) const override;

		/**
		* \fn Asset::IAssetsList* getModuleAssetsList(unsigned long) const
		* \brief Getter returning the asset list for a specific module.
		*
		* \param moduleID, use to get the id of the module.
		* \return Asset::IAssetsList*
		*/
		virtual Asset::IAssetsList* getModuleAssetsList(unsigned long moduleID) const override;

		/**
		* \fn std::list<std::string> getModuleAssetsListName(unsigned long) const
		* \brief Getter returning the list of name of the asset list for a specific module.
		*
		* \param moduleID, use to get the id of the module.
		* \return std::list<std::string>
		*/
		virtual std::list<std::string> getModuleAssetsListName(unsigned long moduleID) const override;

	private:

		Task::ITaskManager*				_taskManager;
		Client::IClientManager*			_clientManager;
		Logger::ILogger*				_logger;
		Asset::IAssetsManager*			_assetManager;
		ModuleFactory*					_moduleFactory;


		std::vector<IModule*>			_moduleList;
		std::list<unsigned long>		_moduleVRList;
		std::list<unsigned long>		_moduleARList;
		std::list<unsigned long>		_moduleONList;
		std::list<unsigned long>		_moduleOFFList;
		std::list<unsigned long>		_modulePausedList;
	};
}
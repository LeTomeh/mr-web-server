/**
* \file IModuleDescription.hh
* \brief IModuleDescription header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* IModuleDescription class header.
*
*/

#pragma once

#include						<string>

/**
* \namespace Module
* \brief Module implementation
*
* Namespace for Module and Module Management
*/
namespace Module
{
	/**
	* \class IModuleDescription
	* \brief Class header for IModuleDescription
	*
	* Class for IModuleDescription creation
	*/
	class							IModuleDescription {
	public:
		/**
		* \fn ~IModuleDescription()
		* \brief Destructor of a specific IModuleDescription
		*/
		virtual						~IModuleDescription() {};


		/**
		* \fn unsigned long getDescriptionSize() const
		* \brief Getter returning size of the description of a specific ModuleDescription.
		*
		* \return unsigned long
		*/
		virtual unsigned long		getDescriptionSize() const = 0;

		/**
		* \fn std::string getDescription() const
		* \brief Getter returning the description of a specific ModuleDescription.
		*
		* \return std::string
		*/
		virtual std::string			getDescription() const = 0;
	};
}
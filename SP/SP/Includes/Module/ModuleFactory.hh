/**
* \file ModuleFactory.hh
* \brief ModuleFactory header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* ModuleFactory class header
*
*/

#pragma once

#include	"Module/IModuleFactory.hh"
#include	"Module/ModuleDescription.hh"
#include	"Module/Module.hh"

#include	<libxml\tree.h>
#include	<libxml\parser.h>
#include	<boost\filesystem.hpp>

/**
* \namespace Module
* \brief Module implementation
*
* Namespace for Module and Module Management
*/
namespace Module
{
	/**
	* \class ModuleFactory
	* \brief Class header for ModuleFactory
	*
	* Class for ModuleFactory creation
	*/
	class ModuleFactory
		: public IModuleFactory
	{
	public:
		/**
		* \fn ModuleFactory(std::string)
		* \brief Constructor of a specific ModuleFactory.
		*
		* \param rootModulePath, use to get the root path of every modules.
		* \return ModuleFactory&
		*/
		ModuleFactory(std::string rootModulePath);

		/**
		* \fn ~ModuleFactory()
		* \brief Destructor of a specific ModuleFactory
		*/
		virtual ~ModuleFactory();

		/**
		* \fn Module::IModule* createModule(const unsigned long) const
		* \brief Function returning a new module based on the module id.
		*
		* \param id, use to get the id of the module.
		* \return Module::IModule*
		*/
		virtual IModule * createModule(const unsigned long id) const override;

	private:
		const std::string		_rootModulePath;
	};
}
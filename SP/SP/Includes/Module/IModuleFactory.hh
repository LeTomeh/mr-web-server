/**
* \file IModuleFactory.hh
* \brief IModuleFactory header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* IModuleFactory class header
*
*/

#pragma once

#include				"Module/IModule.hh"

/**
* \namespace Module
* \brief Module implementation
*
* Namespace for Module and Module Management
*/
namespace Module
{
	/**
	* \class IModuleFactory
	* \brief Class header for IModuleFactory
	*
	* Class for IModuleFactory creation
	*/
	class					IModuleFactory {
	public:
		/**
		* \fn ~IModuleFactory()
		* \brief Destructor of a specific IModuleFactory
		*/
		virtual				~IModuleFactory() {};

		/**
		* \fn Module::IModule* createModule(const unsigned long) const
		* \brief Function returning a new module based on the module id.
		*
		* \param id, use to get the id of the module.
		* \return Module::IModule*
		*/
		virtual IModule*	createModule(const unsigned long id) const = 0;
	};
}
/**
* \file IModuleManager.hh
* \brief IModuleManager header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* IModuleManager class header.
*
*/

#pragma once

#include	<list>
#include	<vector>

namespace Module
{
	class IModuleManager;
}

#include	"Module/IModule.hh"

#include	"Asset/IAssetsList.hh"

/**
* \namespace Module
* \brief Module implementation
*
* Namespace for Module and Module Management
*/
namespace Module
{
	/**
	* \class IModuleManager
	* \brief Class header for IModuleManager
	*
	* Class for IModuleManager creation
	*/
	class									IModuleManager {
	public:
		/**
		* \fn ~IModuleManager()
		* \brief Destructor of a specific IModuleManager
		*/
		virtual								~IModuleManager() {};

		/**
		* \fn void init()
		* \brief Function handling the initialisation of a specific ModuleManager.
		*
		* \return void
		*/
		virtual void						init() = 0;

		/**
		* \fn void update()
		* \brief Function handling the updating of every module for a specific ModuleManager.
		*
		* \return void
		*/
		virtual void						update() = 0;

		/**
		* \fn void clean()
		* \brief Function cleaning a specific ModuleManager.
		*
		* \return void
		*/
		virtual void						clean() = 0;

		/**
		* \fn void createNewModule()
		* \brief Function handling the creation of a new module for specific ModuleManager.
		*
		* \return void
		*/
		virtual void						createNewModule() = 0;

		/**
		* \fn void addClientToModule(unsigned long, unsigned long)
		* \brief Function adding a specific client to a specific module.
		*
		* \param clientID, use to get the client id.
		* \param moduleID, use to get the module id.
		* \return void
		*/
		virtual void						addClientToModule(unsigned long clientID, unsigned long moduleID) = 0;

		/**
		* \fn void pauseClientToModule(unsigned long, unsigned long)
		* \brief Function pausing a specific client to a specific module.
		*
		* \param clientID, use to get the client id.
		* \param moduleID, use to get the module id.
		* \return void
		*/
		virtual void						pauseClientToModule(unsigned long clientID, unsigned long moduleID) = 0;

		/**
		* \fn void removeClientToModule(unsigned long, unsigned long)
		* \brief Function removing a specific client to a specific module.
		*
		* \param clientID, use to get the client id.
		* \param moduleID, use to get the module id.
		* \return void
		*/
		virtual void						removeClientToModule(unsigned long clientID, unsigned long moduleID) = 0;

		/**
		* \fn void launchModule(unsigned long, unsigned long, Core::IServer* server)
		* \brief Function launching a specific module.
		*
		* \param clientID, use to get the client id.
		* \param moduleID, use to get the module id.
		* \param server, use to get the server.
		* \return void
		*/
		virtual void						launchModule(unsigned long clientID, unsigned long moduleID, Core::IServer* server) = 0;

		/**
		* \fn void pauseModule(unsigned long)
		* \brief Function pausing a specific module.
		*
		* \param moduleID, use to get the module id.
		* \return void
		*/
		virtual void						pauseModule(unsigned long moduleID) = 0;

		/**
		* \fn void stopModule(unsigned long)
		* \brief Function stoping a specific module.
		*
		* \param moduleID, use to get the module id.
		* \return void
		*/
		virtual void						stopModule(unsigned long moduleID) = 0;

		/**
		* \fn IModule* getModule(unsigned long)
		* \brief Function returning a specific module.
		*
		* \param moduleID, use to get the module id.
		* \return IModule*
		*/
		virtual IModule* getModule(unsigned long moduleID) const = 0;

		/**
		* \fn std::vector<IModule*> getModuleList() const
		* \brief Function returning a specific module.
		*
		* \param moduleID, use to get the module id.
		* \return std::vector<IModule*>
		*/
		virtual std::vector<IModule*> getModuleList() const = 0;

		/**
		* \fn unsigned long getModuleListSize() const
		* \brief Getter returning the size of the module list.
		*
		* \return unsigned long
		*/
		virtual unsigned long				getModuleListSize() const = 0;

		/**
		* \fn std::list<unsigned long> getModuleVRList() const
		* \brief Getter returning the VR module list.
		*
		* \return std::list<unsigned long>
		*/
		virtual std::list<unsigned long>	getModuleVRList() const = 0;

		/**
		* \fn std::list<unsigned long> getModuleARList() const
		* \brief Getter returning the AR module list.
		*
		* \return std::list<unsigned long>
		*/
		virtual std::list<unsigned long>	getModuleARList() const = 0;

		/**
		* \fn std::list<unsigned long> getModuleLaunched() const
		* \brief Getter returning the launched module list.
		*
		* \return std::list<unsigned long>
		*/
		virtual std::list<unsigned long>	getModuleLaunched() const = 0;

		/**
		* \fn std::list<unsigned long> getModuleClosed() const
		* \brief Getter returning the closed module list.
		*
		* \return std::list<unsigned long>
		*/
		virtual std::list<unsigned long>	getModuleClosed() const = 0;

		/**
		* \fn void addNewAssets(unsigned long, std::string)
		* \brief Function adding a new asset to a specific module.
		*
		* \param moduleID, use to get the id of the module.
		* \param path, use to get the path of the asset.
		* \return void
		*/
		virtual void						addNewAssets(unsigned long moduleID, std::string path) = 0;
		
		/**
		* \fn unsigned long getModuleAssetsListSize(unsigned long) const
		* \brief Getter returning the size of the asset list for a specific module.
		*
		* \param moduleID, use to get the id of the module.
		* \return unsigned long
		*/
		virtual unsigned long				getModuleAssetsListSize(unsigned long moduleID) const = 0;

		/**
		* \fn std::list<Asset::IAsset*> getModuleAssetsList(unsigned long) const
		* \brief Getter returning the asset list for a specific module.
		*
		* \param moduleID, use to get the id of the module.
		* \return std::list<Asset::IAsset*>
		*/
		virtual Asset::IAssetsList*	getModuleAssetsList(unsigned long moduleID) const = 0;

		/**
		* \fn std::list<std::string> getModuleAssetsListName(unsigned long) const
		* \brief Getter returning the list of name of the asset list for a specific module.
		*
		* \param moduleID, use to get the id of the module.
		* \return std::list<std::string>
		*/
		virtual std::list<std::string>		getModuleAssetsListName(unsigned long moduleID) const = 0;
	};
}
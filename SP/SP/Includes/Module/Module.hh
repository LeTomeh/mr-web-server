/**
* \file Module.hh
* \brief Module header.
* \Author Gilles Geunis
* \version 0.1
* \date 01/12/2016
*
* Module class header.
*
*/

#pragma once

#include "Module/IModule.hh"
#include "Client/Client.hh"

#include <Windows.h>
#include <boost\filesystem.hpp>

/**
* \namespace Module
* \brief Module implementation
*
* Namespace for Module and Module Management
*/
namespace Module
{
	/**
	* \class Module
	* \brief Class header for Module
	*
	* Class for Module creation
	*/
	class Module
		: public IModule
	{
	public:
		/**
		* \fn Module(const unsigned long, const std::string, const std::string, const IModuleDescription*, const bool, const bool)
		* \brief Constructor of a specific Module.
		*
		* \param id, use to set the id of the module.
		* \param path, use to set the path of the module.
		* \param name, use to set the name of the module.
		* \param description, use to set the description of the module.
		* \param allowAR, use to set the AR compatibiilty of the module.
		* \param allowVR, use to set the VR compatibiilty of the module.
		* \return Module&
		*/
		Module(const unsigned long id, const std::string path, const std::string name, const IModuleDescription* description, const bool allowAR, const bool allowVR);

		/**
		* \fn ~Module()
		* \brief Destructor of a specific Module
		*/	
		virtual ~Module();

		/**
		* \fn bool isLaunched() const
		* \brief Function use to know if the module is launched.
		*
		* \return bool
		*/
		virtual bool isLaunched() const override;

		/**
		* \fn unsigned long getID() const
		* \brief Getter returning the id of a specific module.
		*
		* \return unsigned long
		*/
		virtual unsigned long getID() const override;

		/**
		* \fn std::string getPath() const
		* \brief Getter returning path of a specific module.
		*
		* \return std::string
		*/
		virtual std::string getPath() const override;

		/**
		* \fn Module::e_ModuleDisponibility getModuleDisponibility() const
		* \brief Getter returning the disponibility of a specific module.
		*
		* \return Module::e_ModuleDisponibility
		*/
		virtual e_ModuleDisponibility getModuleDisponibility() const override;

		/**
		* \fn std::string getName() const
		* \brief Getter returning name of a specific module.
		*
		* \return std::string
		*/
		virtual std::string getName() const override;

		/**
		* \fn const Module::IModuleDescription* getDescription() const
		* \brief Getter returning description of a specific module.
		*
		* \return const Module::IModuleDescription*
		*/
		virtual const IModuleDescription * getDescription() const override;

		/**
		* \fn bool allowAR() const
		* \brief Function returning the AR compatibility of a specific module.
		*
		* \return bool
		*/
		virtual bool allowAR() const override;

		/**
		* \fn bool allowVR() const
		* \brief Function returning the VR compatibility of a specific module.
		*
		* \return bool
		*/
		virtual bool allowVR() const override;

		/**
		* \fn Module::IUserList* getUserList() const
		* \brief Getter returning the uset list of a specific module.
		*
		* \return Module::IUserList*
		*/
		virtual IUserList * getUserList() const override;

		/**
		* \fn void connectNewClient(unsigned long)
		* \brief Function adding a client to the client list of a specific module.
		*
		* \param clientID, use to get the client id.
		* \return void
		*/
		virtual void connectNewClient(unsigned long clientID) override;

		/**
		* \fn void pauseClient(unsigned long)
		* \brief Function pausing a specific client from a specific module.
		*
		* \param clientID, use to get the client id.
		* \return void
		*/
		virtual void pauseClient(unsigned long clientID) override;

		/**
		* \fn void disconnectClient(unsigned long)
		* \brief Function removing a specific client from a specific module.
		*
		* \param clientID, use to get the client id.
		* \return void
		*/
		virtual void disconnectClient(unsigned long clientID) override;

		/**
		* \fn void launch(unsigned long clientID, Core::IServer* server)
		* \brief Function starting a specific module.
		*
		* \param clientID, use to get the id of the client who launch the module.
		* \param server, use to get the server.
		* \return void
		*/
		virtual void launch(unsigned long clientID, Core::IServer* server) override;

		/**
		* \fn void pauseModule()
		* \brief Function pausing a specific module.
		*
		* \return void
		*/
		virtual void pauseModule() override;

		/**
		* \fn void unpauseModule()
		* \brief Function unpausing a specific module.
		*
		* \return void
		*/
		virtual void unpauseModule() override;

		/**
		* \fn void close()
		* \brief Function closing a specific module.
		*
		* \return void
		*/
		virtual void close() override;

		/**
		* \fn bool isAvailable() const
		* \brief Getter to know if module is available.
		*
		* \return bool
		*/
		virtual bool					isAvailable() const override;

		/**
		* \fn void setAvailable(bool available)
		* \brief Setter to know if module is available.
		* 
		* \param available, use to set the availability of the module.
		* \return void
		*/
		virtual void					setAvailable(bool available) override;

	private:
		LPCSTR						_executablePath;
		const unsigned long			_id;
		bool						_isLaunched;
		const std::string			_path;
		const std::string			_name;
		e_ModuleDisponibility		_disponibility;
		const IModuleDescription*	_description;
		const bool					_allowAR;
		const bool					_allowVR;
		IUserList*					_userList;
		unsigned long				_launchedBy;
		bool						_isAvailable;

		Client::IClient*			_serverConnexion;
	};
}
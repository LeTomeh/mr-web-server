#pragma once

#include	<string>
#include	"Utils/xmlNode.hh"

/**
* \namespace Utils
* \brief Utils implementation
*
* Namespace for snippets
*/
namespace Utils
{
	/**
	* \namespace Parser
	* \brief Parser implementation
	*
	* Namespace for Parser
	*/
	namespace	Parser
	{
		/**
		* \class xmlParser
		* \brief xmlParser implementation
		*
		* Parser for xml file
		*/
		class		xmlParser
		{
		public:
			xmlParser(std::string file);
			~xmlParser();

			xmlNode*	getRootNode() const;
		private:
			xmlNode*	_rootNode;
		};
	}
}
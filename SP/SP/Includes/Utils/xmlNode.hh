#pragma once

#include <string>
#include <vector>

/**
* \namespace Utils
* \brief Utils implementation
*
* Namespace for snippets
*/
namespace Utils
{
	/**
	* \namespace Parser
	* \brief Parser implementation
	*
	* Namespace for Parser
	*/
	namespace	Parser
	{
		/**
		* \class xmlParser
		* \brief xmlParser implementation
		*
		* Parser for xml file
		*/
		class		xmlNode
		{
		public:

			/**
			* \fn xmlNode(std::string name, std::string value)
			* \brief Constructor of a specific xmlNode.
			*
			* \param name , use to get the name of the node.
			* \param value, use to get the value of the node.
			* \return xmlNode&
			*/
			xmlNode(std::string name, std::string value);

			/**
			* \fn ~xmlNode()
			* \brief Destructor of a specific xmlNode
			*/
			~xmlNode();

			/**
			* \fn xmlNode* getNextChild() const
			* \brief Getter returning the next child xmlNode.
			*
			* \return xmlNode*
			*/
			xmlNode*				getNextChild() const;

			/**
			* \fn void addChild(xmlNode* node)
			* \brief Function adding a new child to the current childs list.
			*
			* \param node, use to add the new node.
			* \return void
			*/
			void					addChild(xmlNode* node);

		private:
			std::string				_name;
			std::string				_value;
			std::vector<xmlNode*>	_childrens;
		};
	}
}

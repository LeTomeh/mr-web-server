#pragma once

#include "Task/Task.hh"
#include "Task/ETaskID.hh"

#include <string>
#include <map>
#include <sstream>
#include <iostream>

/**
* \namespace Task
* \brief Task implementation
*
* Namespace for Task and Task Management
*/
namespace Task
{
	/**
	 * \class TaskGenerator
	 * \brief TaskGenerator implementation
	 *
	 * Task factory
	 */
	class TaskGenerator {
	private:
		typedef ITask* (TaskGenerator::*tGeneratorPtr)(std::string clientRequest, unsigned long id);

		/**
		 * \fn ITask* list(std::string clientRequest, unsigned long id);
		 * \brief Function to handle list task
		 *
		 * \param clientRequest, use to get argument from the client request
		 * \param id, use to get the id of the client
		 * \return ITask*
		 */
		ITask* list(std::string clientRequest, unsigned long id);

		/**
		* \fn ITask* listAllModule(std::string clientRequest, unsigned long id);
		* \brief Function to handle list all request
		*
		* \param clientRequest, use to get argument from the client request
		* \param id, use to get the id of the client
		* \return ITask*
		*/
		ITask* listAllModule(std::string clientRequest, unsigned long id);

		/**
		* \fn ITask* listModule(std::string clientRequest, unsigned long id);
		* \brief Function to handle list module request
		*
		* \param clientRequest, use to get argument from the client request
		* \param id, use to get the id of the client
		* \return ITask*
		*/
		ITask* listModule(std::string clientRequest, unsigned long id);

		/**
		* \fn ITask* listModuleOn(std::string clientRequest, unsigned long id);
		* \brief Function to handle list module on request
		*
		* \param clientRequest, use to get argument from the client request
		* \param id, use to get the id of the client
		* \return ITask*
		*/
		ITask* listModuleOn(std::string clientRequest, unsigned long id);

		/**
		* \fn ITask* listModuleOff(std::string clientRequest, unsigned long id);
		* \brief Function to handle list module off request
		*
		* \param clientRequest, use to get argument from the client request
		* \param id, use to get the id of the client
		* \return ITask*
		*/
		ITask* listModuleOff(std::string clientRequest, unsigned long id);

		/**
		* \fn ITask* listModuleVR(std::string clientRequest, unsigned long id);
		* \brief Function to handle list module vr request
		*
		* \param clientRequest, use to get argument from the client request
		* \param id, use to get the id of the client
		* \return ITask*
		*/
		ITask* listModuleVR(std::string clientRequest, unsigned long id);

		/**
		* \fn ITask* listModuleAR(std::string clientRequest, unsigned long id);
		* \brief Function to handle list module ar request
		*
		* \param clientRequest, use to get argument from the client request
		* \param id, use to get the id of the client
		* \return ITask*
		*/
		ITask* listModuleAR(std::string clientRequest, unsigned long id);

		/**
		* \fn ITask* launchModule(std::string clientRequest, unsigned long id);
		* \brief Function to handle launch module request
		*
		* \param clientRequest, use to get argument from the client request
		* \param id, use to get the id of the client
		* \return ITask*
		*/
		ITask* launchModule(std::string clientRequest, unsigned long id);

		/**
		* \fn ITask* pauseModule(std::string clientRequest, unsigned long id);
		* \brief Function to handle pause module request
		*
		* \param clientRequest, use to get argument from the client request
		* \param id, use to get the id of the client
		* \return ITask*
		*/
		ITask* pauseModule(std::string clientRequest, unsigned long id);

		/**
		* \fn ITask* addUserToModule(std::string clientRequest, unsigned long id);
		* \brief Function to handle user request
		*
		* \param clientRequest, use to get argument from the client request
		* \param id, use to get the id of the client
		* \return ITask*
		*/
		ITask* addUserToModule(std::string clientRequest, unsigned long id);

		/**
		* \fn ITask* logoutUser(std::string clientRequest, unsigned long id);
		* \brief Function to handle logout request
		*
		* \param clientRequest, use to get argument from the client request
		* \param id, use to get the id of the client
		* \return ITask*
		*/
		ITask* logoutUser(std::string clientRequest, unsigned long id);

		/**
		* \fn ITask* stopModule(std::string clientRequest, unsigned long id);
		* \brief Function to handle stop request
		*
		* \param clientRequest, use to get argument from the client request
		* \param id, use to get the id of the client
		* \return ITask*
		*/
		ITask* stopModule(std::string clientRequest, unsigned long id);

		/**
		* \fn ITask* loadModule(std::string clientRequest, unsigned long id);
		* \brief Function to handle load request
		*
		* \param clientRequest, use to get argument from the client request
		* \param id, use to get the id of the client
		* \return ITask*
		*/
		ITask* loadModule(std::string clientRequest, unsigned long id);

		/**
		* \fn ITask* switchConnectionType(std::string clientRequest, unsigned long id);
		* \brief Function to handle switch connection request
		*
		* \param clientRequest, use to get argument from the client request
		* \param id, use to get the id of the client
		* \return ITask*
		*/
		ITask* switchConnectionType(std::string clientRequest, unsigned long id);

		/**
		* \fn ITask* hostingNewConnection(std::string clientRequest, unsigned long id);
		* \brief Function to handle hosting new request
		*
		* \param clientRequest, use to get argument from the client request
		* \param id, use to get the id of the client
		* \return ITask*
		*/
		ITask* hostingNewConnection(std::string clientRequest, unsigned long id);
		
		/**
		* \fn ITask* uploadPackage(std::string clientRequest, unsigned long id);
		* \brief Function to handle upload package request
		*
		* \param clientRequest, use to get argument from the client request
		* \param id, use to get the id of the client
		* \return ITask*
		*/
		ITask* uploadPackage(std::string clientRequest, unsigned long id);
		
		/**
		* \fn ITask* downloadPackage(std::string clientRequest, unsigned long id);
		* \brief Function to handle download package request
		*
		* \param clientRequest, use to get argument from the client request
		* \param id, use to get the id of the client
		* \return ITask*
		*/
		ITask* downloadPackage(std::string clientRequest, unsigned long id);

	public :

		/**
		* \fn ITask* generateTask(std::string clientRequest, unsigned long id);
		* \brief Function to handle task creation
		*
		* \param clientRequest, use to get argument from the client request
		* \param id, use to get the id of the client
		* \return ITask*
		*/
		static ITask* generateTask(std::string clientRequest, unsigned long id);
	
	private:
		static std::map < std::string, tGeneratorPtr> createTaskCreator();
		static std::map<std::string, tGeneratorPtr>	_taskCreator;
		static std::map < std::string, tGeneratorPtr> createListCreator();
		static std::map<std::string, tGeneratorPtr> _listCreator;

	};
}
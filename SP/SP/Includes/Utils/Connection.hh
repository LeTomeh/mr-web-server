#pragma once

#include <string>
#include <sstream>
#include <boost/asio.hpp>

/**
* \namespace Utils
* \brief Utils implementation
*
* Namespace for snippets
*/
namespace Utils
{
	/**
	* \namespace Connection
	* \brief Connection implementation
	*
	* Namespace for Connection snippets
	*/
	namespace Connection
	{

#define WEBSERVER_TOKEN		"SW"
#define GAMESERVER_TOKEN	"SG"

		/**
		* \fn bool checkWebServerHeader(std::string& header)
		* \brief Function checking if the header is a web server header
		*
		* \param header, use to get the client header
		* \return bool
		*/
		bool			checkWebServerHeader(std::string& header);

		/**
		* \fn bool checkGameServerHeader(std::string& header)
		* \brief Function checking if the header is a game server header
		*
		* \param header, use to get the client header
		* \return bool
		*/
		bool			checkGameServerHeader(std::string& header);
	}
}